﻿using UnityEngine;
using System.Collections;

public class LODCheck : MonoBehaviour {

    public GameObject[] Objects;
    public float Distance;

    private GameObject _player;

    private bool _isActive = true;
	// Use this for initialization
	void Start ()
    {
        _player = GameObject.FindGameObjectWithTag("Player");
	}
	void FixedUpdate ()
    {
        if(Vector3.Distance(_player.transform.position, transform.position) >= Distance)
        {
            if (_isActive)
            {
                foreach (GameObject obj in Objects)
                {
                    obj.SetActive(false);
                    _isActive = false;
                }
            }
        }
        else
        {
            if(!_isActive)
            {
                foreach (GameObject obj in Objects)
                {
                    obj.SetActive(true);
                    _isActive = true;
                }
            }
        }
	}
}
