﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System;

public class TransformRotation : EditorWindow
{

    [MenuItem("Tools/Euler rotation")]
    public static void SetRotation()
    {
        EditorWindow.GetWindow(typeof(TransformRotation));
    }


    private bool _isStart = true;
    private Vector3 _euler;
    private Vector3 _baseEuler;
    private Quaternion _baseRotation;
    public void OnGUI()
    {
        if(_isStart)
        {
            _baseRotation = Selection.gameObjects[0].transform.rotation;
            _baseEuler = Selection.gameObjects[0].transform.eulerAngles;
            _euler = _baseEuler;
            _isStart = false;
        }

        if (Selection.gameObjects.Length > 0)
        {
            EditorGUILayout.LabelField(String.Format("{0}", Selection.gameObjects[0].name), EditorStyles.miniLabel);
            _euler = EditorGUILayout.Vector3Field("Rotation", _euler);

            _euler.x = _euler.x % 360;
            _euler.y = _euler.y % 360;
            _euler.z = _euler.z % 360;


            Selection.gameObjects[0].transform.eulerAngles = _euler;

            if (GUILayout.Button("Reset", EditorStyles.miniButton))
            {
                Selection.gameObjects[0].transform.rotation = _baseRotation;
                _euler = _baseEuler;
            }
        }
        else
            _isStart = false;
    }
}
#endif