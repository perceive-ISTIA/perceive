﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System;
using System.Collections.Generic;
using System.IO;

public class Prefab : EditorWindow
{
    [MenuItem("Tools/Revert to Prefab %r")]
    static void Revert()
    {
        var selection = Selection.gameObjects;

        if (selection.Length > 0)
        {
            foreach (GameObject obj in selection)
            {
                PrefabUtility.ResetToPrefabState(obj);
            }
        }
        else
        {
            Debug.Log("Cannot revert to prefab - nothing selected");
        }
    }
}

public class TagSearcher : EditorWindow
{
    [MenuItem("Tools/Tags")]
    static void Tags()
    {
        EditorWindow.GetWindow(typeof(TagSearcher));
    }

    private List<GameObject> _tags;
    private string _selectedTag;
    private Vector2 _scroll;
    void OnGUI()
    {
        EditorGUILayout.LabelField("Tag: ");
        _selectedTag = EditorGUILayout.TextField(_selectedTag);

        if (GUILayout.Button("Search"))
        {
            _tags = new List<GameObject>();
            FindObjectsWithTagInactive(_selectedTag);
        }
        if (_tags != null)
        {
            EditorGUILayout.LabelField(string.Format("Number of objects: {0}", _tags.Count));
            _scroll = EditorGUILayout.BeginScrollView(_scroll, GUILayout.Height(100));
            foreach (GameObject go in _tags)
            {
                EditorGUILayout.LabelField(go.name, EditorStyles.miniBoldLabel);
            }
            EditorGUILayout.EndScrollView();

            EditorGUILayout.BeginHorizontal();

            if (GUILayout.Button("Disable"))
            {
                foreach (GameObject obj in _tags)
                    obj.SetActive(false);
            }
            if (GUILayout.Button("Enable"))
            {
                foreach (GameObject obj in _tags)
                    obj.SetActive(true);
            }

            EditorGUILayout.EndHorizontal();
        }
    }

    private void FindObjectsWithTagInactive(string tag)
    {
        GameObject downotown = GameObject.Find("Downtown");

        _tags = BrowseGameObjects(downotown, tag);
    }

    private List<GameObject> BrowseGameObjects(GameObject parent, string tag)
    {
        List<GameObject> list = new List<GameObject>();
        if (parent.tag == tag)
            list.Add(parent);
        else
        {
            foreach (Transform tr in parent.transform)
            {
                if (tr.tag == tag)
                    list.Add(tr.gameObject);
                else
                {
                    if (tr.childCount > 0)
                    {
                        foreach (GameObject o in BrowseGameObjects(tr.gameObject, tag))
                        {
                            list.Add(o);
                        }
                    }
                }
            }
        }
        return list;
    }
}
public class LODEditor : EditorWindow
{
    [MenuItem("Tools/Changer LOD")]
    public static void SetLOD()
    {
        EditorWindow.GetWindow(typeof(LODEditor));
    }
    private string[] _text;
    private bool _initError = false;
    private int _lodCount = 0;

    public void OnEnable()
    {
        InitializeGUI();
    }
    public void OnGUI()
    {
        for (int cnt = 0; cnt < _lodCount; cnt++)
        {
            EditorGUILayout.LabelField(String.Format("LOD #{0}", cnt), EditorStyles.miniLabel);
            _text[cnt] = EditorGUILayout.TextField(_text[cnt], EditorStyles.miniTextField);
        }
        if (!_initError)
        {
            if (GUILayout.Button("Apply", EditorStyles.miniButton))
            {
                bool isOK = true;
                for (int cnt = _lodCount - 1; cnt > 0; cnt--)
                {
                    try
                    {
                        if (float.Parse(_text[cnt]) >= float.Parse(_text[cnt - 1]))
                        {
                            isOK = false;
                            Debug.LogError("=>Attempting to set LOD where the screen relative is greater then or equal to a higher detail LOD level");
                        }
                    }
                    catch (Exception)
                    {
                        isOK = false;
                        Debug.LogError(String.Format("=>Wrong float format in the text field n°{0} or the n°{1}", cnt - 1, cnt));
                    }
                }
                if (isOK)
                {
                    LOD[] lods = new LOD[_lodCount];
                    var selection = Selection.gameObjects;
                    foreach (GameObject obj in selection)
                    {
                        if (obj.GetComponent<LODGroup>() != null)
                        {
                            int cnt = 0;
                            List<Renderer> tmpRnd = GetChildsRenderers(obj.transform);
                            Renderer[] rnd = new Renderer[tmpRnd.Count];
                            foreach (Renderer r in tmpRnd)
                            {
                                rnd[cnt] = r;
                                cnt++;
                            }
                            lods[0] = new LOD(float.Parse(_text[0]) / 100F, rnd);

                            obj.GetComponent<LODGroup>().SetLODS(lods);
                            obj.GetComponent<LODGroup>().RecalculateBounds();
                            Debug.Log("Success!");
                        }
                        else
                            Debug.Log("Nope");
                    }
                }
            }
        }
        else
        {
            EditorGUILayout.LabelField("Error, multiple objects with different or none LOD levels detected !", EditorStyles.largeLabel);
        }
        if (GUILayout.Button("Refresh", EditorStyles.miniButton))
        {
            Refresh();
        }
    }
    private void InitializeGUI()
    {
        int cnt = 0;
        foreach (GameObject obj in Selection.gameObjects)
        {
            if (obj.GetComponent<LODGroup>() != null && (cnt != 0 && _lodCount != obj.GetComponent<LODGroup>().lodCount))
            {
                    _initError = true;
            }
            else
            {
                    _initError = false;
            }
            if (obj.GetComponent<LODGroup>() != null)
                _lodCount = obj.GetComponent<LODGroup>().lodCount;
            cnt++;
        }
        _text = new string[_lodCount];
    }
    private void Refresh()
    {
        InitializeGUI();
    }

    private List<Renderer> GetChildsRenderers(Transform baseTr)
    {
        List<Renderer> tmp = new List<Renderer>();
        if (baseTr.GetComponent<Renderer>() != null)
        {
            tmp.Add(baseTr.GetComponent<Renderer>());
        }
        foreach (Transform tr in baseTr)
        {
            if (tr.GetComponent<Renderer>() != null)
            {
                tmp.Add(tr.GetComponent<Renderer>());
            }
            if (tr.childCount > 0)
            {
                List<Renderer> childs = GetChildsRenderers(tr);
                foreach (Renderer rnd in childs)
                {
                    tmp.Add(rnd);
                }
            }
        }
        return tmp;
    }
}
#endif