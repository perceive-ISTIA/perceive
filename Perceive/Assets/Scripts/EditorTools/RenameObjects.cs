﻿using UnityEditor;
using UnityEngine;

internal class RenameObjects : ScriptableWizard
{
    public GameObject parent;
    public string prefix = "_waypoint";

    [MenuItem("Tools/Rename")]
    public static void CreateWizard()
    {
        string buttonText = "Rename";
        DisplayWizard<RenameObjects>("Rename Objects", buttonText);
    }

    private void OnWizardCreate()
    {
        if(parent == null) {
            return;
        }
        for (int i = 0; i < parent.transform.childCount; i++) {
            GameObject subchild = parent.transform.GetChild(i).gameObject;
            subchild.name = prefix + (i + 1).ToString();
        }
    }
}