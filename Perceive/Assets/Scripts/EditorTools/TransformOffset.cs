﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System;
using UnityEditor.AnimatedValues;
using System.Collections.Generic;

public class TransformOffset : EditorWindow
{
    public float toto;
    private AnimBool _menu1;
    private AnimBool _menu2;

    private GameObject _otherObject;
    private bool _useOtherModel = false;
    private Vector3 _otherSize;
    private Vector3 _otherCenter;
    private bool _isOtherObjectSelection = false;

    private bool _isOriginSelection = false;
    private GameObject _origin;
    private Vector3 _originCenter;

    private bool _isGroupOfObjects = false;
    #region Menu1
    private Vector3 _size;
    private int _number = 1;

    private bool _isX = true;
    private bool _isY = false;
    private bool _isZ = false;

    private bool _alignCenterX = false;
    private bool _alignCenterY = false;
    private bool _alignCenterZ = false;

    private bool _isMinusX = false;
    private bool _isMinusY = false;
    private bool _isMinusZ = false;

    private Vector3 _newLocalPosition;
    private Vector3 _newPosition;
    private Vector3 _baseVector;
    private int _baseX;
    private int _baseY;
    private int _baseZ;
    private GUILayoutOption[] _options = { GUILayout.MaxWidth(200.0f), GUILayout.MinWidth(10.0f), GUILayout.MinWidth(100.0f) };
    #endregion

    #region Menu2
    private Vector3 _size2;
    private int _number2 = 1;

    private bool _isX2 = true;
    private bool _isY2 = false;
    private bool _isZ2 = false;

    private bool _isMinusX2 = false;
    private bool _isMinusY2 = false;
    private bool _isMinusZ2 = false;

    private Vector3 _baseVector2;
    private int _baseX2;
    private int _baseY2;
    private int _baseZ2;


    private GUILayoutOption[] _options2 = { GUILayout.MaxWidth(200.0f), GUILayout.MinWidth(10.0f), GUILayout.MinWidth(100.0f) };
    #endregion

    private Vector2 _scroll;
    [MenuItem("Tools/Offset")]
    public static void SetTransformOffset()
    {
        EditorWindow.GetWindow(typeof(TransformOffset));
    }


    void OnEnable()
    {
        _menu1 = new AnimBool(true);
        _menu1.valueChanged.AddListener(Repaint);
        _menu2 = new AnimBool(true);
        _menu2.valueChanged.AddListener(Repaint);
    }


    void OnGUI()
    {
        _scroll = EditorGUILayout.BeginScrollView(_scroll);
        if (_origin != null)
            EditorGUILayout.LabelField(String.Format("{0} {1}", _origin.name, _origin.transform.position));
        switch (_isOriginSelection)
        {
            case true:
                if (GUILayout.Button("Done"))
                {
                    _isOriginSelection = false;
                    if (Selection.gameObjects.Length > 0)
                    {
                        _origin = Selection.gameObjects[0];

                        if (_origin.GetComponent<Renderer>() == null)
                        {
                            foreach(Transform tr in _origin.transform)
                            {
                                if(tr.GetComponent<Renderer>() != null)
                                {
                                    _size = tr.GetComponent<Renderer>().bounds.size;
                                    _size2 = tr.GetComponent<Renderer>().bounds.size;
                                    //_originCenter = tr.GetComponent<Renderer>().bounds.center;
                                    break;
                                }
                            }
                        }
                        else
                        {
                            _size = _origin.GetComponent<Renderer>().bounds.size;
                            _size2 = _origin.GetComponent<Renderer>().bounds.size;
                            //_originCenter = _origin.GetComponent<Renderer>().bounds.center;
                        }
                        _originCenter = GetCenter(_origin);
                    }
                }
                break;
            case false:
                if (GUILayout.Button("Select the origin object"))
                {
                    _isOriginSelection = true;
                }
                break;
        }

        #region Other object selection
        switch (_isOtherObjectSelection)
        {
            case true:
                if (GUILayout.Button("Done"))
                {
                    _isOtherObjectSelection = false;
                    if (Selection.gameObjects.Length > 0)
                    {
                        _otherObject = Selection.gameObjects[0];
                        if (_otherObject.GetComponent<Renderer>() == null)
                        {
                            foreach (Transform tr in _otherObject.transform)
                            {
                                if (tr.GetComponent<Renderer>() != null)
                                {
                                    _otherSize = tr.GetComponent<Renderer>().bounds.size;
                                    //_otherCenter = tr.GetComponent<Renderer>().bounds.center;
                                    break;
                                }
                            }
                        }
                        else
                        {
                            _otherSize = _otherObject.GetComponent<Renderer>().bounds.size;
                            //_otherCenter = _otherObject.GetComponent<Renderer>().bounds.center;
                        }
                        _otherCenter = GetCenter(_otherObject);
                    }
                }
                break;
            case false:
                if (GUILayout.Button("Select another 3D model to instantiate"))
                {
                    _isOtherObjectSelection = true;
                }
                break;
        }
        #endregion

        if (_origin != null)
        {
            #region MultipleObjects
            /*_isGroupOfObjects = EditorGUILayout.Toggle("Selection is a group of objects", _isGroupOfObjects, EditorStyles.toggle);
            if(_isGroupOfObjects && _otherObject != null)
            {
                _otherCenter = GetCenter(Selection.gameObjects);
            }*/
            #endregion
            #region Menu1
            _menu1.target = EditorGUILayout.ToggleLeft("Set position with offset (one object)", _menu1.target);
            if (EditorGUILayout.BeginFadeGroup(_menu1.faded))
            {
                if(_otherObject != null)
                    _useOtherModel = EditorGUILayout.Toggle("Use other model", _useOtherModel, EditorStyles.toggle);
                EditorGUILayout.LabelField("Move the selected object with the specified offset/id:", EditorStyles.miniLabel);

                EditorGUILayout.BeginHorizontal();

                EditorGUILayout.BeginVertical();
                _isX = EditorGUILayout.Toggle("X", _isX, EditorStyles.toggle);
                if(_isX)
                    _isMinusX = EditorGUILayout.Toggle("-X", _isMinusX, EditorStyles.toggle);
                else
                    _isMinusX = false;
                EditorGUILayout.EndVertical();
                EditorGUILayout.BeginVertical();
                _isY = EditorGUILayout.Toggle("Y", _isY, EditorStyles.toggle);
                if(_isY)
                    _isMinusY = EditorGUILayout.Toggle("-Y", _isMinusY, EditorStyles.toggle);
                else
                    _isMinusY = false;
                EditorGUILayout.EndVertical();
                EditorGUILayout.BeginVertical();
                _isZ = EditorGUILayout.Toggle("Z", _isZ, EditorStyles.toggle);
                if(_isZ)
                    _isMinusZ = EditorGUILayout.Toggle("-Z", _isMinusZ, EditorStyles.toggle);
                else
                    _isMinusZ = false;
                EditorGUILayout.EndVertical();


                EditorGUILayout.EndHorizontal();

                _size = EditorGUILayout.Vector3Field("Size of the model: ", _size, _options);
                _number = EditorGUILayout.IntField("Position number", _number, _options);
                if (_number <= 0)
                    _number = 1;
                //Prediction :
                switch (_isX)
                {
                    case true:
                        if (_isMinusX)
                            _baseX = -1;
                        else
                            _baseX = 1;
                        break;
                    case false:
                        _baseX = 0;
                        break;
                }
                switch (_isY)
                {
                    case true:
                        if (_isMinusY)
                            _baseY = -1;
                        else
                            _baseY = 1;
                        break;
                    case false:
                        _baseY = 0;
                        break;
                }
                switch (_isZ)
                {
                    case true:
                        if (_isMinusZ)
                            _baseZ = -1;
                        else
                            _baseZ = 1;
                        break;
                    case false:
                        _baseZ = 0;
                        break;
                }
                if (!_useOtherModel)
                {
                    _baseVector = new Vector3(_baseX * _size.x, _baseY * _size.y, _baseZ * _size.z);
                }
                else
                {
                    #region Origin X
                    float x;
                    if (_isMinusX)
                    {
                        if (_origin.transform.position.x > _originCenter.x)
                        {
                            x = _size.x - ((_size.x / 2) - (_origin.transform.position.x - _originCenter.x));
                        }
                        else
                        {
                            x = (_size.x / 2) - (_originCenter.x - _origin.transform.position.x);
                        }
                    }
                    else
                    {
                        if (_origin.transform.position.x > _originCenter.x)
                        {
                            x = (_size.x / 2) - (_origin.transform.position.x - _originCenter.x);
                        }
                        else
                            x = _size.x - ((_size.x / 2) - (_originCenter.x - _origin.transform.position.x));
                    }
                    #endregion
                    #region Origin Y
                    float y;
                    if (_isMinusY)
                    {
                        if (_origin.transform.position.y > _originCenter.y)
                        {
                            y = _size.y - ((_size.y / 2) - (_origin.transform.position.y - _originCenter.y));
                        }
                        else
                        {
                            y = (_size.y / 2) - (_originCenter.y - _origin.transform.position.y);
                        }
                    }
                    else
                    {
                        if (_origin.transform.position.y > _originCenter.y)
                        {
                            y = (_size.y / 2) - (_origin.transform.position.y - _originCenter.y);
                        }
                        else
                            y = _size.y - ((_size.y / 2) - (_originCenter.y - _origin.transform.position.y));
                    }
                    #endregion
                    #region Origin Z
                    float z;
                    if (_isMinusZ)
                    {
                        if (_origin.transform.position.z > _originCenter.z)
                        {
                            z = _size.z - ((_size.z / 2) - (_origin.transform.position.z - _originCenter.z));
                        }
                        else
                        {
                            z = (_size.z / 2) - (_originCenter.z - _origin.transform.position.z);
                        }
                    }
                    else
                    {
                        if (_origin.transform.position.z > _originCenter.z)
                        {
                            z = (_size.z / 2) - (_origin.transform.position.z - _originCenter.z);
                        }
                        else
                            z = _size.z - ((_size.z / 2) - (_originCenter.z - _origin.transform.position.z));
                    }
                    #endregion

                    if (_otherObject != null)
                    {
                        #region Other X
                        float ox;
                        if (_isMinusX)
                        {
                            if (_otherObject.transform.position.x > _otherCenter.x)
                                ox = (_otherSize.x / 2) - (_otherObject.transform.position.x - _otherCenter.x);
                            else
                                ox = _otherSize.x - ((_otherSize.x / 2) - (_otherCenter.x - _otherObject.transform.position.x));
                        }
                        else
                        {
                            if (_otherObject.transform.position.x > _otherCenter.x)
                                ox = _otherSize.x - ((_otherSize.x / 2) - (_otherObject.transform.position.x - _otherCenter.x));
                            else
                                ox = (_otherSize.x / 2) - (_otherCenter.x - _otherObject.transform.position.x);
                        }
                        #endregion
                        #region Other Y
                        float oy;
                        if (_isMinusY)
                        {
                            if (_otherObject.transform.position.y > _otherCenter.y)
                                oy = (_otherSize.y / 2) - (_otherObject.transform.position.y - _otherCenter.y);
                            else
                                oy = _otherSize.y - ((_otherSize.y / 2) - (_otherCenter.y - _otherObject.transform.position.y));
                        }
                        else
                        {
                            if (_otherObject.transform.position.y > _otherCenter.y)
                                oy = _otherSize.y - ((_otherSize.y / 2) - (_otherObject.transform.position.y - _otherCenter.y));
                            else
                                oy = (_otherSize.y / 2) - (_otherCenter.y - _otherObject.transform.position.y);
                        }
                        #endregion
                        #region Other Z
                        float oz;
                        if (_isMinusZ)
                        {
                            if (_otherObject.transform.position.z > _otherCenter.z)
                                oz = (_otherSize.z / 2) - (_otherObject.transform.position.z - _otherCenter.z);
                            else
                                oz = _otherSize.z - ((_otherSize.z / 2) - (_otherCenter.z - _otherObject.transform.position.z));
                        }
                        else
                        {
                            if (_otherObject.transform.position.z > _otherCenter.z)
                                oz = _otherSize.z - ((_otherSize.z / 2) - (_otherObject.transform.position.z - _otherCenter.z));
                            else
                                oz = (_otherSize.z / 2) - (_otherCenter.z - _otherObject.transform.position.z);
                        }
                        #endregion
                        _baseVector = new Vector3(_baseX * (x + ox), _baseY * (y + oy), _baseZ * (z + oz));
                    }
                }

                #region Align center
                if (_otherObject != null)
                {
                    _alignCenterX = EditorGUILayout.Toggle("Align center X", _alignCenterX, EditorStyles.toggle);
                    _alignCenterY = EditorGUILayout.Toggle("Align center Y", _alignCenterY, EditorStyles.toggle);
                    _alignCenterZ = EditorGUILayout.Toggle("Align center Z", _alignCenterZ, EditorStyles.toggle);
                }
                #endregion


                _newLocalPosition = _origin.transform.localPosition + (_number * _baseVector);
                EditorGUILayout.LabelField(String.Format("Predicted local position: {0}", _newLocalPosition), EditorStyles.miniLabel);
                _newPosition = _origin.transform.position + (_number * _baseVector);
                EditorGUILayout.LabelField(String.Format("Predicted global position: {0}", _newPosition), EditorStyles.miniLabel);

                if (GUILayout.Button("Clone at the selected position"))
                {
                    GameObject obj;
                    if (!_useOtherModel)
                    {
                        obj = (GameObject)GameObject.Instantiate(_origin);
                        obj.name = _origin.name;
                        obj.transform.parent = _origin.transform.parent;
                        obj.transform.position = _origin.transform.position + _number * _baseVector;//_origin.transform.position + _number * _size * new Vector3(1, 0, 0);//_origin.transform.localPosition + _number * _size * _baseVector;
                        obj.transform.rotation = _origin.transform.rotation;
                    }
                    else
                    {
                        obj = (GameObject)GameObject.Instantiate(_otherObject);
                        obj.name = _otherObject.name;
                        obj.transform.parent = _otherObject.transform.parent;
                        obj.transform.position = _origin.transform.position + _number * _baseVector;//_origin.transform.position + _number * _size * new Vector3(1, 0, 0);//_origin.transform.localPosition + _number * _size * _baseVector;
                        obj.transform.rotation = _otherObject.transform.rotation;
                        //Align center :
                        Vector3 center = GetCenter(obj);
                        float x = 0;
                        float y = 0;
                        float z = 0;
                        if(_alignCenterX)
                        {
                            if (center.x > _originCenter.x)
                                x = -(center.x - _originCenter.x);
                            else
                                x = _originCenter.x - center.x;
                        }
                        if (_alignCenterY)
                        {
                            if (center.y > _originCenter.y)
                                y = -(center.y - _originCenter.y);
                            else
                                y = _originCenter.y - center.y;
                        }
                        if (_alignCenterZ)
                        {
                            if (center.z > _originCenter.z)
                                z = -(center.z - _originCenter.z);
                            else
                                z = _originCenter.z - center.z;
                        }
                        obj.transform.position += new Vector3(x, y, z);
                    }
                }
            }

            EditorGUILayout.EndFadeGroup();
            #endregion

            #region Menu2
            _menu2.target = EditorGUILayout.ToggleLeft("Set position with offset (multiple objects)", _menu2.target);
            if (EditorGUILayout.BeginFadeGroup(_menu2.faded))
            {
                EditorGUILayout.LabelField("Add objects with the specified offset/id:", EditorStyles.miniLabel);

                EditorGUILayout.BeginHorizontal();

                EditorGUILayout.BeginVertical();
                _isX2 = EditorGUILayout.Toggle("X", _isX2, EditorStyles.toggle);
                if(_isX2)
                    _isMinusX2 = EditorGUILayout.Toggle("-X", _isMinusX2, EditorStyles.toggle);
                else
                    _isMinusX2 = false;
                EditorGUILayout.EndVertical();
                EditorGUILayout.BeginVertical();
                _isY2 = EditorGUILayout.Toggle("Y", _isY2, EditorStyles.toggle);
                if(_isY2)
                    _isMinusY2 = EditorGUILayout.Toggle("-Y", _isMinusY2, EditorStyles.toggle);
                else
                    _isMinusY2 = false;
                EditorGUILayout.EndVertical();
                EditorGUILayout.BeginVertical();
                _isZ2 = EditorGUILayout.Toggle("Z", _isZ2, EditorStyles.toggle);
                if(_isZ2)
                    _isMinusZ2 = EditorGUILayout.Toggle("-Z", _isMinusZ2, EditorStyles.toggle);
                else
                    _isMinusZ2  = false;
                EditorGUILayout.EndVertical();


                EditorGUILayout.EndHorizontal();

                _size2 = EditorGUILayout.Vector3Field("Size of the model: ", _size2, _options2);
                _number2 = EditorGUILayout.IntField("Desired amount of objects: ", _number2, _options2);
                if (_number2 <= 0)
                    _number2 = 1;
                //Prediction :
                switch (_isX2)
                {
                    case true:
                        if (_isMinusX2)
                            _baseX2 = -1;
                        else
                            _baseX2 = 1;
                        break;
                    case false:
                        _baseX2 = 0;
                        break;
                }
                switch (_isY2)
                {
                    case true:
                        if (_isMinusY2)
                            _baseY2 = -1;
                        else
                            _baseY2 = 1;
                        break;
                    case false:
                        _baseY2 = 0;
                        break;
                }
                switch (_isZ2)
                {
                    case true:
                        if (_isMinusZ2)
                            _baseZ2 = -1;
                        else
                            _baseZ2 = 1;
                        break;
                    case false:
                        _baseZ2 = 0;
                        break;
                }
                if (GUILayout.Button("Add objects"))
                {
                    _baseVector2 = new Vector3(_baseX2 * _size2.x, _baseY2 * _size2.y, _baseZ2 * _size2.z);
                    GameObject previous = _origin;
                    for (int cnt = 1; cnt <= _number2; cnt++)
                    {
                        GameObject obj = (GameObject)GameObject.Instantiate(_origin);
                        obj.name = _origin.name;
                        obj.transform.parent = _origin.transform.parent;
                        obj.transform.position = previous.transform.position + cnt * _baseVector2;//_origin.transform.position + _number * _size * new Vector3(1, 0, 0);//_origin.transform.localPosition + _number * _size * _baseVector;
                        obj.transform.rotation = _origin.transform.rotation;
                    }
                }
            }

            EditorGUILayout.EndFadeGroup();
            #endregion
        }
        EditorGUILayout.EndScrollView();
    }


    private Vector3 GetCenter(GameObject obj)
    {
        List<Renderer> tmp;
        tmp = GetChildsRenderersCenters(obj.transform);
        Bounds bounds = tmp[0].bounds;
        foreach(Renderer rnd in tmp)
        {
            bounds.Encapsulate(rnd.bounds);
        }

        return new Vector3(bounds.center.x, bounds.center.y, bounds.center.z);
    }
    private Vector3 GetCenter(GameObject[] objs)
    {
        List<Renderer> rendererList = new List<Renderer>();
        foreach(GameObject obj in objs)
        {
            foreach(Renderer rnd in GetChildsRenderersCenters(obj.transform))
            {
                rendererList.Add(rnd);
            }
        }
        Bounds bounds = rendererList[0].bounds;
        foreach (Renderer rnd in rendererList)
        {
            bounds.Encapsulate(rnd.bounds);
        }

        return new Vector3(bounds.center.x, bounds.center.y, bounds.center.z);
    }

    private List<Renderer> GetChildsRenderersCenters(Transform baseTr)
    {
        List<Renderer> rnd = new List<Renderer>();
        if (baseTr.GetComponent<Renderer>() != null)
        {
            if (!rnd.Contains(baseTr.GetComponent<Renderer>()))
                rnd.Add(baseTr.GetComponent<Renderer>());
        }
        foreach (Transform tr in baseTr)
        {
            if (tr.GetComponent<Renderer>() != null)
            {
                if (!rnd.Contains(tr.GetComponent<Renderer>()))
                    rnd.Add(tr.GetComponent<Renderer>());
            }
            if (tr.childCount > 0)
            {
                List<Renderer> childs = GetChildsRenderersCenters(tr);
                foreach (Renderer v3 in childs)
                {
                    if (!rnd.Contains(v3))
                        rnd.Add(v3);
                }
            }
        }
        return rnd;
    }
}

public class AlignObjects : EditorWindow
{
    [MenuItem("Tools/Align objects")]
    public static void Align()
    {
        EditorWindow.GetWindow(typeof(AlignObjects));
    }


    public void OnGUI()
    {

    }
}
#endif