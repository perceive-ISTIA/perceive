﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TrafficLightController : MonoBehaviour
{

    public Light[] RedLight;
    public Light[] YellowLight;
    public Light[] GreenLight;
    public float LightDistance = 200f;



    private Transform _player;
    private Transform _thisTransform;
    private Enums.LightState _state;
    private Flare _flare;
    private bool _isTooFar = false;
    private int _lastFlareIndex = -1;
    private float _dist;


    private ObjectManager _manager;
    private int _flareCount;
    private float _step = 0f;

    void Awake()
    {
        _thisTransform = transform;
        _manager = GameObject.FindGameObjectWithTag("ObjectManager").GetComponent<ObjectManager>();
        _flareCount = _manager.TrafficLightFlares.Length;
    }

	// Use this for initialization
	void Start ()
    {
        //Get Player
        _player = GameObject.FindGameObjectWithTag("Player").transform;
        _flare = RedLight[0].flare;
        _step = LightDistance / (float)_flareCount;
	}

    void FixedUpdate()
    {
        _dist = Vector3.Distance(_player.position, _thisTransform.position);
        if(_dist >= LightDistance)
        {
            if (!_isTooFar)
            {
                foreach (Light light in RedLight)
                    light.flare = null;
                foreach (Light light in YellowLight)
                    light.flare = null;
                foreach (Light light in GreenLight)
                    light.flare = null;
                _isTooFar = true;
            }
        }
        else
        {
            int index = 0;
            for(int i = 0; i < _manager.TrafficLightFlares.Length; i++)
            {
                if (_dist <= (i + 1) * _step)
                {
                    index = i;
                    break;
                }
            }
            if(_isTooFar || _lastFlareIndex != index)
            {
                _flare = _manager.TrafficLightFlares[index];
                foreach (Light light in RedLight)
                    light.flare = _flare;
                foreach (Light light in YellowLight)
                    light.flare = _flare;
                foreach (Light light in GreenLight)
                    light.flare = _flare;
                _isTooFar = false;
                _lastFlareIndex = index;
            }
        }
    }

    public void SetState(Enums.LightState state)
    {
        _state = state;
        switch(state)
        {
            case Enums.LightState.All:
                foreach(Light light in RedLight)
                    light.enabled = true;
                foreach (Light light in YellowLight)
                    light.enabled = true;
                foreach (Light light in GreenLight)
                    light.enabled = true;
                break;
            case Enums.LightState.None:
                foreach (Light light in RedLight)
                    light.enabled = false;
                foreach (Light light in YellowLight)
                    light.enabled = false;
                foreach (Light light in GreenLight)
                    light.enabled = false;
                break;
            case Enums.LightState.Red:
                foreach (Light light in RedLight)
                    light.enabled = true;
                foreach (Light light in YellowLight)
                    light.enabled = false;
                foreach (Light light in GreenLight)
                    light.enabled = false;
                break;
            case Enums.LightState.Yellow:
                foreach (Light light in RedLight)
                    light.enabled = false;
                foreach (Light light in YellowLight)
                    light.enabled = true;
                foreach (Light light in GreenLight)
                    light.enabled = false;
                break;
            case Enums.LightState.Green:
                foreach (Light light in RedLight)
                    light.enabled = false;
                foreach (Light light in YellowLight)
                    light.enabled = false;
                foreach (Light light in GreenLight)
                    light.enabled = true;
                break;
        }
    }

    public Enums.LightState GetState()
    {
        return _state;
    }
}
