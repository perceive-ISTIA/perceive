﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class StreetLight : MonoBehaviour
{
    private float Distance = 150f;

    private List<Light> _lights;
    private List<Flare> _flares = new List<Flare>();
    private GameObject _player;
    private bool _isTooFar = false;

    private Transform _thisTransform;
    private Transform _playerTr;
    public bool Enabled { get; set; }

    void Awake()
    {
        _thisTransform = transform;
        _player = GameObject.FindGameObjectWithTag("Player");
        _playerTr = _player.transform;
    }

	// Use this for initialization
	void Start ()
    {
	    //Looking for the light(s) -- flare(s)
        _lights = Tools.GetComponentsInChildren<Light>(_thisTransform);
        foreach(Light light in _lights)
        {
            _flares.Add(light.flare);
        }
	}
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        if (Enabled)
        {
            if (Vector3.Distance(_thisTransform.position, _playerTr.position) >= Distance)
            {
                if (!_isTooFar)
                {
                    _isTooFar = true;
                    foreach (Light light in _lights)
                    {
                        light.flare = null;
                        light.enabled = false;
                    }
                }
            }
            else
            {
                if (_isTooFar)
                {
                    _isTooFar = false;
                    int cnt = 0;
                    foreach (Light light in _lights)
                    {
                        light.enabled = true;
                        light.flare = _flares[cnt];
                        cnt++;
                    }
                }
            }
        }
	}
}
