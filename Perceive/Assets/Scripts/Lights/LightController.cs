﻿using UnityEngine;
using System.Collections;

public class LightController : MonoBehaviour
{
    public Tools.EnableStatement State;
    private GameObject[] _streetLights;
	// Use this for initialization
	void Start ()
    {
        _streetLights = GameObject.FindGameObjectsWithTag("StreetLights");
        switch(State)
        {
            case Tools.EnableStatement.Enable:
                EnableStreetLights();
                break;
            case Tools.EnableStatement.Disable:
                DisableStreetLights();
                break;
        }
	}

    public void DisableStreetLights()
    {
        foreach(GameObject obj in _streetLights)
        {
            obj.GetComponent<StreetLight>().Enabled = false;
            foreach(Light light in Tools.GetComponentsInChildren<Light>(obj.transform))
            {
                light.enabled = false;
            }
        }
    }

    public void EnableStreetLights()
    {
        foreach (GameObject obj in _streetLights)
        {
            foreach (Light light in Tools.GetComponentsInChildren<Light>(obj.transform))
            {
                light.enabled = true;
            }
            obj.GetComponent<StreetLight>().Enabled = true;
        }
    }
}
