﻿using UnityEngine;
using System.Collections;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.IO;

public class TrafficLightGroupController : MonoBehaviour
{
    public TrafficLightController[] Lights;
    public float Delay = 10;
    public float YellowDelay = 1.5f;
    public float RedToGreenDelay = 2.0f;
    public Enums.IntersectionLightType Type;
    public string DefaultGreen;
    public bool isRandom { get; set; }
    private int _type = 0;

    private bool _start = false;

    private float _tempTime = -1f;
    private float _targetTime = -1f;
    private int _prevSide = 0;
    private bool _isPause = false;

    void Awake()
    {
        isRandom = true;
        DefaultGreen = "01-03";
    }

    void Update()
    {
        if(_start)
        {
            if (Time.time >= _targetTime && !_isPause)
            {
                switch (Type)
                {
                    case Enums.IntersectionLightType.Carrefour:
                        SwitchStateCarrefour();
                        break;
                    case Enums.IntersectionLightType.IntersectionT:
                        SwitchStateIntersectionT();
                        break;
                }
            }
        }
    }

    public void Pause()
    {
        _tempTime = _targetTime - Time.time;
        _isPause = true;
    }

    public void Play()
    {
        if (!_start)
        {
            InitLights();
            StartLights();
        }
        else
            _targetTime = Time.time + _tempTime;
        _isPause = false;
    }

    private void SwitchStateCarrefour()
    {
        //Green to Yellow
        if (Lights[0].GetState() == Enums.LightState.Green && Lights[1].GetState() == Enums.LightState.Red)
        {
            Lights[0].SetState(Enums.LightState.Yellow);
            Lights[2].SetState(Enums.LightState.Yellow);
            _targetTime = Time.time + YellowDelay;
            return;
        }
        //Yellow to Red
        if (Lights[0].GetState() == Enums.LightState.Yellow && Lights[1].GetState() == Enums.LightState.Red)
        {
            Lights[0].SetState(Enums.LightState.Red);
            Lights[2].SetState(Enums.LightState.Red);
            _prevSide = 0;
            _targetTime = Time.time + RedToGreenDelay;
            return;
        }
        //Red to Green
        if (Lights[0].GetState() == Enums.LightState.Red && Lights[1].GetState() == Enums.LightState.Red && _prevSide == 0)
        {
            Lights[1].SetState(Enums.LightState.Green);
            Lights[3].SetState(Enums.LightState.Green);
            _targetTime = Time.time + Delay;
            return;
        }
        
        //SWITCH
        //Green to Yellow
        if (Lights[1].GetState() == Enums.LightState.Green && Lights[0].GetState() == Enums.LightState.Red)
        {
            Lights[1].SetState(Enums.LightState.Yellow);
            Lights[3].SetState(Enums.LightState.Yellow);
            _targetTime = Time.time + YellowDelay;
            return;
        }
        //Yellow to Red
        if (Lights[1].GetState() == Enums.LightState.Yellow && Lights[0].GetState() == Enums.LightState.Red)
        {
            Lights[1].SetState(Enums.LightState.Red);
            Lights[3].SetState(Enums.LightState.Red);
            _prevSide = 1;
            _targetTime = Time.time + RedToGreenDelay;
            return;
        }
        //Red to Green
        if (Lights[1].GetState() == Enums.LightState.Red && Lights[0].GetState() == Enums.LightState.Red && _prevSide == 1)
        {
            Lights[0].SetState(Enums.LightState.Green);
            Lights[2].SetState(Enums.LightState.Green);
            _targetTime = Time.time + Delay;
            return;
        }
    }

    private void SwitchStateIntersectionT()
    {
        //Green to Yellow
        if (Lights[0].GetState() == Enums.LightState.Green && Lights[1].GetState() == Enums.LightState.Red)
        {
            Lights[0].SetState(Enums.LightState.Yellow);
            Lights[2].SetState(Enums.LightState.Yellow);
            _targetTime = Time.time + YellowDelay;
            return;
        }
        //Yellow to Red
        if (Lights[0].GetState() == Enums.LightState.Yellow && Lights[1].GetState() == Enums.LightState.Red)
        {
            Lights[0].SetState(Enums.LightState.Red);
            Lights[2].SetState(Enums.LightState.Red);
            _prevSide = 0;
            _targetTime = Time.time + RedToGreenDelay;
            return;
        }
        //Red to Green
        if (Lights[0].GetState() == Enums.LightState.Red && Lights[1].GetState() == Enums.LightState.Red && _prevSide == 0)
        {
            Lights[1].SetState(Enums.LightState.Green);
            _targetTime = Time.time + Delay;
            return;
        }

        //SWITCH
        //Green to Yellow
        if (Lights[1].GetState() == Enums.LightState.Green && Lights[0].GetState() == Enums.LightState.Red)
        {
            Lights[1].SetState(Enums.LightState.Yellow);
            _targetTime = Time.time + YellowDelay;
            return;
        }
        //Yellow to Red
        if (Lights[1].GetState() == Enums.LightState.Yellow && Lights[0].GetState() == Enums.LightState.Red)
        {
            Lights[1].SetState(Enums.LightState.Red);
            _prevSide = 1;
            _targetTime = Time.time + RedToGreenDelay;
            return;
        }
        //Red to Green
        if (Lights[1].GetState() == Enums.LightState.Red && Lights[0].GetState() == Enums.LightState.Red && _prevSide == 1)
        {
            Lights[0].SetState(Enums.LightState.Green);
            Lights[2].SetState(Enums.LightState.Green);
            _targetTime = Time.time + Delay;
            return;
        }
    }

    public void InitLights()
    {
        if (isRandom)
        {
            Delay = Delay + Random.Range(-5, 5);
            //Init
            switch (Type)
            {
                case Enums.IntersectionLightType.Carrefour:
                    if (Random.Range(0, 2) == 0)
                    {
                        _type = 0;
                        DefaultGreen = "01-03";
                        Lights[0].SetState(Enums.LightState.Green);
                        Lights[2].SetState(Enums.LightState.Green);
                        Lights[1].SetState(Enums.LightState.Red);
                        Lights[3].SetState(Enums.LightState.Red);
                    }
                    else
                    {
                        _type = 1;
                        DefaultGreen = "02-04";
                        Lights[0].SetState(Enums.LightState.Red);
                        Lights[2].SetState(Enums.LightState.Red);
                        Lights[1].SetState(Enums.LightState.Green);
                        Lights[3].SetState(Enums.LightState.Green);
                    }
                    break;
                case Enums.IntersectionLightType.IntersectionT:
                    if (Random.Range(0, 2) == 0)
                    {
                        _type = 0;
                        DefaultGreen = "01-03";
                        Lights[0].SetState(Enums.LightState.Green);
                        Lights[2].SetState(Enums.LightState.Green);
                        Lights[1].SetState(Enums.LightState.Red);
                    }
                    else
                    {
                        _type = 1;
                        DefaultGreen = "02";
                        Lights[0].SetState(Enums.LightState.Red);
                        Lights[2].SetState(Enums.LightState.Red);
                        Lights[1].SetState(Enums.LightState.Green);
                    }
                    break;
            }
        }
        else
        {
            switch (Type)
            {
                case Enums.IntersectionLightType.Carrefour:
                    if (DefaultGreen == "01-03")
                    {
                        _type = 0;
                        Lights[0].SetState(Enums.LightState.Green);
                        Lights[2].SetState(Enums.LightState.Green);
                        Lights[1].SetState(Enums.LightState.Red);
                        Lights[3].SetState(Enums.LightState.Red);
                    }
                    else
                    {
                        _type = 1;
                        Lights[0].SetState(Enums.LightState.Red);
                        Lights[2].SetState(Enums.LightState.Red);
                        Lights[1].SetState(Enums.LightState.Green);
                        Lights[3].SetState(Enums.LightState.Green);
                    }
                    break;

                case Enums.IntersectionLightType.IntersectionT:
                    if (DefaultGreen == "01-03")
                    {
                        _type = 0;
                        Lights[0].SetState(Enums.LightState.Green);
                        Lights[2].SetState(Enums.LightState.Green);
                        Lights[1].SetState(Enums.LightState.Red);
                    }
                    else
                    {
                        _type = 1;
                        Lights[0].SetState(Enums.LightState.Red);
                        Lights[2].SetState(Enums.LightState.Red);
                        Lights[1].SetState(Enums.LightState.Green);
                    }
                    break;
            }
        }
    }
    public Enums.LightState GetLightsStatus(int index)
    {
        if (index < Lights.Length)
            return Lights[index].GetState();
        else
            return Enums.LightState.None;
    }
    public void SetLightsStatus(int index, Enums.LightState state)
    {
        if (index < Lights.Length)
            Lights[index].SetState(state);
    }
    public void StartLights()
    {
        _targetTime = Time.time + Delay;
        _start = true;
    }

    public static void PauseAllLights()
    {
        Debug.Log("PauseLights");
        foreach(TrafficLightGroupController trafficLight in ObjectManager.TrafficLights)
        {
            trafficLight.Pause();
        }
    }

    public static void PlayAllLights()
    {
        Debug.Log("PlayLights");
        foreach (TrafficLightGroupController trafficLight in ObjectManager.TrafficLights)
        {
            trafficLight.Play();
        }
    }

    public static void LoadLightsFromFile(string path, bool isLightRandom)
    {
        int tmpSize = ObjectManager.TrafficLights.Length;
        if (!isLightRandom && File.Exists(path))
        {
            TrafficLightsXML trafficLightsObject = new TrafficLightsXML();
            XmlSerializer serializer = new XmlSerializer(typeof(TrafficLightsXML));
            using (StreamReader stream = new StreamReader(path, System.Text.Encoding.GetEncoding("UTF-8")))
            {
                trafficLightsObject = serializer.Deserialize(stream) as TrafficLightsXML;
                stream.Close();
            }
            int size = trafficLightsObject.Position.Length;

            TrafficLightGroupController light;
            for (int x = 0; x < size; x++)
            {
                for (int j = 0; j < tmpSize; j++)
                {
                    if (Tools.ParseVector3ToString(ObjectManager.TrafficLights[j].transform.position) == trafficLightsObject.Position[x])
                    {
                        light = ObjectManager.TrafficLights[j];
                        light.isRandom = false;
                        light.Delay = trafficLightsObject.Delay[x];
                        light.RedToGreenDelay = trafficLightsObject.RToG[x];
                        light.YellowDelay = trafficLightsObject.YToR[x];
                        light.DefaultGreen = trafficLightsObject.DefaultGreen[x];
                        light.InitLights();
                        break;
                    }
                }
            }
        }
        else
        {
            if (File.Exists(path))
            {
                TrafficLightsXML trafficLightsObject = new TrafficLightsXML();
                XmlSerializer serializer = new XmlSerializer(typeof(TrafficLightsXML));
                using (StreamReader stream = new StreamReader(path, System.Text.Encoding.GetEncoding("UTF-8")))
                {
                    trafficLightsObject = serializer.Deserialize(stream) as TrafficLightsXML;
                    stream.Close();
                }
                int size = trafficLightsObject.Position.Length;

                TrafficLightGroupController light;
                for (int x = 0; x < size; x++)
                {
                    for (int j = 0; j < tmpSize; j++)
                    {
                        if (Tools.ParseVector3ToString(ObjectManager.TrafficLights[j].transform.position) == trafficLightsObject.Position[x])
                        {
                            light = ObjectManager.TrafficLights[j];
                            light.isRandom = false;
                            light.Delay = trafficLightsObject.Delay[x];
                            light.RedToGreenDelay = trafficLightsObject.RToG[x];
                            light.YellowDelay = trafficLightsObject.YToR[x];
                            light.DefaultGreen = trafficLightsObject.DefaultGreen[x];
                            light.InitLights();
                            break;
                        }
                    }
                }
            }
            else
            {
                foreach (TrafficLightGroupController trafficLight in ObjectManager.TrafficLights)
                {
                    trafficLight.isRandom = true;
                    trafficLight.InitLights();
                }
            }
        }
    }
    public static void SaveLightsToFile(string path)
    {
        int tmpSize = ObjectManager.TrafficLights.Length;
        TrafficLightsXML trafficLightsObject = new TrafficLightsXML();

        TrafficLightGroupController light;
        trafficLightsObject.Position = new string[tmpSize];
        trafficLightsObject.Delay = new float[tmpSize];
        trafficLightsObject.RToG = new float[tmpSize];
        trafficLightsObject.YToR = new float[tmpSize];
        trafficLightsObject.DefaultGreen = new string[tmpSize];
        for (int x = 0; x < tmpSize; x++)
        {
            light = ObjectManager.TrafficLights[x];
            trafficLightsObject.Delay[x] = light.Delay;
            trafficLightsObject.RToG[x] = light.RedToGreenDelay;
            trafficLightsObject.YToR[x] = light.YellowDelay;
            trafficLightsObject.DefaultGreen[x] = light.DefaultGreen;
            trafficLightsObject.Position[x] = Tools.ParseVector3ToString(ObjectManager.TrafficLights[x].transform.position);
        }


        XmlSerializer xml = new XmlSerializer(typeof(TrafficLightsXML));
        using (StreamWriter stream = new StreamWriter(path, false, System.Text.Encoding.GetEncoding("UTF-8")))
        {
            xml.Serialize(stream, trafficLightsObject);
            stream.Close();
        }
    }
}