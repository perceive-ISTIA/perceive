﻿using UnityEngine;
using System.Collections;
using System;

public class PlannerPart : MonoBehaviour
{
    //Cached objects
    private Renderer _renderer;
    private Material _allMat;
    private Material _bloMat;
    private Material _bloEvMat;
    private Material _selMat;
    private Transform _thisTransform;
    private Transform _parent;
    private Material _materialOutline;
    private Info _infoDelegate;

    public Enums.PlannerPart Type;
    public Enums.PlannerBlockedType BlockedType { get; set; }
    public Transform Extremity01;
    public Transform Extremity02;
    public PlannerPart[] Adjacents; //Used to get adjacent transform when intersection
    public Waypoint[] WaypointsA;
    public Waypoint[] WaypointsB;
    public TrafficLightGroupController LightController;
    public bool isStart { get; set; }
    public bool isEnd { get; set; }
    public int Side { get; set; }

    private bool _isSelected = false;
    public bool isSelected
    { 
        get
        {
            return _isSelected;
        }
        set
        {
            _isSelected = value;
            UpdateMaterial();
        }            
    }

    private GameObject _roadBlock01;
    private GameObject _roadBlock01_INV;
    private GameObject _roadBlock02;
    private GameObject _roadBlock02_INV;
    private bool _allOutlined = false;

    private Enums.PlannerState _state;
    public Enums.PlannerState State
    {
        get
        {
            return _state;
        }
        set
        {
            _state = value;
            UpdateMaterial();
        }
    }

    public string Name
    {
        get
        {
            return string.Format("{0}_{1}_{2}", _parent.name, _thisTransform.name, _state); //Just split with "_" to get info'
        }
    }

    void Awake()
    {
        _thisTransform = transform;
        _parent = _thisTransform.parent;

        BlockedType = Enums.PlannerBlockedType.None;

        isStart = false;
        isEnd = false;
        Side = 0;

        _renderer = GetComponent<Renderer>();
        _infoDelegate = new Info(GetInfo);

        //Object pooling
        if (Type != Enums.PlannerPart.Intersection && Type != Enums.PlannerPart.SimpleCrossing)
        {
            if (Extremity02 != null && Extremity01 != null)
            {
                if (Type == Enums.PlannerPart.DoubleRoad)
                {
                    #region Extremity01
                    if (!Extremity01.FindChild("RoadBlock01(Clone)"))
                        _roadBlock01 = (GameObject)GameObject.Instantiate(Resources.Load("RoadBlocks/RoadBlock01"));
                    else
                        _roadBlock01 = Extremity01.FindChild("RoadBlock01(Clone)").gameObject;

                    if (!Extremity01.FindChild("RoadBlock02(Clone)"))
                        _roadBlock02 = (GameObject)GameObject.Instantiate(Resources.Load("RoadBlocks/RoadBlock02"));
                    else
                        _roadBlock02 = Extremity01.FindChild("RoadBlock02(Clone)").gameObject;
                    #endregion

                    #region Extremity02
                    if (Extremity02.name == "Extremity01")
                    {
                        if (!Extremity02.FindChild("RoadBlock01(Clone)"))
                            _roadBlock01_INV = (GameObject)GameObject.Instantiate(Resources.Load("RoadBlocks/RoadBlock01"));
                        else
                            _roadBlock01_INV = Extremity02.FindChild("RoadBlock01(Clone)").gameObject;

                        if (!Extremity02.FindChild("RoadBlock02(Clone)"))
                            _roadBlock02_INV = (GameObject)GameObject.Instantiate(Resources.Load("RoadBlocks/RoadBlock02"));
                        else
                            _roadBlock02_INV = Extremity02.FindChild("RoadBlock02(Clone)").gameObject;
                    }
                    else
                    {
                        if (!Extremity02.FindChild("RoadBlock01_INV(Clone)"))
                            _roadBlock01_INV = (GameObject)GameObject.Instantiate(Resources.Load("RoadBlocks/RoadBlock01_INV"));
                        else
                            _roadBlock01_INV = Extremity02.FindChild("RoadBlock01_INV(Clone)").gameObject;

                        if (!Extremity02.FindChild("RoadBlock02_INV(Clone)"))
                            _roadBlock02_INV = (GameObject)GameObject.Instantiate(Resources.Load("RoadBlocks/RoadBlock02_INV"));
                        else
                            _roadBlock02_INV = Extremity02.FindChild("RoadBlock02_INV(Clone)").gameObject;
                    }


                    #endregion

                }
                else
                {
                    #region Extremity01
                    if (!Extremity01.FindChild("RoadBlock01_simple(Clone)"))
                        _roadBlock01 = (GameObject)GameObject.Instantiate(Resources.Load("RoadBlocks/RoadBlock01_simple"));
                    else
                        _roadBlock01 = Extremity01.FindChild("RoadBlock01_simple(Clone)").gameObject;

                    if (!Extremity01.FindChild("RoadBlock02_simple(Clone)"))
                        _roadBlock02 = (GameObject)GameObject.Instantiate(Resources.Load("RoadBlocks/RoadBlock02_simple"));
                    else
                        _roadBlock02 = Extremity01.FindChild("RoadBlock01_simple(Clone)").gameObject;

                    #endregion

                    #region Extremity02
                    if (Extremity02.name == "Extremity01")
                    {
                        if (!Extremity02.FindChild("RoadBlock01_simple(Clone)"))
                            _roadBlock01_INV = (GameObject)GameObject.Instantiate(Resources.Load("RoadBlocks/RoadBlock01_simple"));
                        else
                            _roadBlock01_INV = Extremity02.FindChild("RoadBlock01_simple(Clone)").gameObject;

                        if (!Extremity02.FindChild("RoadBlock02_simple(Clone)"))
                            _roadBlock02_INV = (GameObject)GameObject.Instantiate(Resources.Load("RoadBlocks/RoadBlock02_simple"));
                        else
                            _roadBlock02_INV = Extremity02.FindChild("RoadBlock02_simple(Clone)").gameObject;
                    }
                    else
                    {
                        if (!Extremity02.FindChild("RoadBlock01_simple_INV(Clone)"))
                            _roadBlock01_INV = (GameObject)GameObject.Instantiate(Resources.Load("RoadBlocks/RoadBlock01_simple_INV"));
                        else
                            _roadBlock01_INV = Extremity02.FindChild("RoadBlock01_simple_INV(Clone)").gameObject;

                        if (!Extremity02.FindChild("RoadBlock02_simple_INV(Clone)"))
                            _roadBlock02_INV = (GameObject)GameObject.Instantiate(Resources.Load("RoadBlocks/RoadBlock02_simple_INV"));
                        else
                            _roadBlock02_INV = Extremity02.FindChild("RoadBlock02_simple_INV(Clone)").gameObject;
                    }

                    #endregion
                }
                _roadBlock01_INV.transform.SetParent(Extremity02, false);
                _roadBlock01.transform.SetParent(Extremity01, false);

                _roadBlock01_INV.transform.localPosition = Vector3.zero;
                _roadBlock01.transform.localPosition = Vector3.zero;

                _roadBlock01.SetActive(false);
                _roadBlock01_INV.SetActive(false);

                //--------------------------------------

                _roadBlock02_INV.transform.SetParent(Extremity02, false);
                _roadBlock02.transform.SetParent(Extremity01, false);

                _roadBlock02_INV.transform.localPosition = Vector3.zero;
                _roadBlock02.transform.localPosition = Vector3.zero;

                _roadBlock02.SetActive(false);
                _roadBlock02_INV.SetActive(false);
            }
        }
    }

    void OnMouseEnter()
    {
        if (PathPlanner.IsEditingExperiments)
            return;
        if (_isSelected)
            return;
        if(Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
        {
            _allOutlined = true;
            foreach(Transform tr in _parent)
            {
                tr.GetComponent<PlannerPart>().SetOutlined();
            }
        }
        else
            SetOutlined();
        ExpPath.OnHighlighted += _infoDelegate;
    }

    void OnMouseOver()
    {
        if (PathPlanner.IsEditingExperiments)
            return;
        if (_isSelected)
            return;
        if (_allOutlined && !(Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift)))
        {
            _allOutlined = false;
            ExpPath.OnHighlighted -= _infoDelegate;
            foreach (Transform tr in _parent)
            {
                tr.GetComponent<PlannerPart>().UpdateMaterial();
            }
            OnMouseEnter();
        }
        if(!_allOutlined && (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift)))
        {
            _allOutlined = true;
            foreach(Transform tr in _parent)
            {
                tr.GetComponent<PlannerPart>().SetOutlined();
            }
        }
    }
    void OnMouseExit()
    {
        if (PathPlanner.IsEditingExperiments)
            return;
        if (_isSelected)
            return;
        if(_allOutlined)
        {
            _allOutlined = false;
            foreach (Transform tr in _parent)
            {
                tr.GetComponent<PlannerPart>().UpdateMaterial();
            }
        }
        else
            UpdateMaterial();
        ExpPath.OnHighlighted -= _infoDelegate;
    }

    public void SetOutlined()
    {
        _renderer.material = _materialOutline;
    }

    public PlannerPartInfo GetInfo()
    {
        PlannerPartInfo info = new PlannerPartInfo(this);
        info.Name = _thisTransform.name;
        info.Road = _parent.parent.name;
        info.Type = Type;
        info.State = _state;
        info.isStart = isStart;
        info.Side = Side;
        info.isEnd = isEnd;
        info.BlockedType = BlockedType;
        info.Adjactents = new string[Adjacents.Length];
        for (int cnt = 0; cnt < info.Adjactents.Length; cnt++)
        {
            info.Adjactents[cnt] = Adjacents[cnt].Name;
        }
        return info;
    }

    private void UpdateMaterial()
    {
        if (_isSelected)
            return;
        switch (_state)
        {
            case Enums.PlannerState.Allowed:
                _renderer.material = _allMat;
                BlockedType = Enums.PlannerBlockedType.None;
                break;
            case Enums.PlannerState.BlockedEvent:
                _renderer.material = _bloEvMat;
                isStart = false;
                isEnd = false;
                break;
            case Enums.PlannerState.Blocked:
                _renderer.material = _bloMat;
                BlockedType = Enums.PlannerBlockedType.None;
                isStart = false;
                isEnd = false;
                break;
        }
        _infoDelegate = new Info(GetInfo);
    }

    public void SetMaterials(Material AllowedMaterial, Material BlockedMaterial, Material BlockedEventMaterial, Material OutlinedMat, Material selectedMat)
    {
        _allMat = AllowedMaterial;
        _bloMat = BlockedMaterial;
        _bloEvMat = BlockedEventMaterial;
        _materialOutline = OutlinedMat;
        _selMat = selectedMat;
    }

    public void SetSelectedMaterial()
    {
        _renderer.material = _selMat;
    }

    public void SwitchState()
    {
        if (_state == Enums.PlannerState.Blocked || _state == Enums.PlannerState.BlockedEvent)
            State = Enums.PlannerState.Allowed;
        else
            State = Enums.PlannerState.Blocked;
    }
    public void PopRoadBlock()
    {
        switch (BlockedType)
        {
            case Enums.PlannerBlockedType.RoadBlockConcrete:
                //Pop contrete road block
                _roadBlock01.SetActive(true);
                _roadBlock01_INV.SetActive(true);

                _roadBlock02.SetActive(false);
                _roadBlock02_INV.SetActive(false);
                break;
            case Enums.PlannerBlockedType.RoadBlockBarricade:
                //Pop contrete road block
                _roadBlock02.SetActive(true);
                _roadBlock02_INV.SetActive(true);

                _roadBlock01.SetActive(false);
                _roadBlock01_INV.SetActive(false);
                break;
            case Enums.PlannerBlockedType.None:
                if(_roadBlock01 != null)
                    _roadBlock01.SetActive(false);
                if (_roadBlock01_INV != null)
                    _roadBlock01_INV.SetActive(false);
                if (_roadBlock02 != null)
                    _roadBlock02.SetActive(false);
                if (_roadBlock02_INV != null)
                    _roadBlock02_INV.SetActive(false);
                break;
        }
    }

    public override string ToString()
    {
        return Name;
    }


}


public class PlannerPartInfo
{
    public string Name { get; set; }
    public string Road { get; set; }
    public bool isStart { get; set; }
    public int Side { get; set; }
    public bool isEnd { get; set; }
    public Enums.PlannerPart Type { get; set; }
    public Enums.PlannerState State { get; set; }
    public Enums.PlannerBlockedType BlockedType { get; set; }
    public string[] Adjactents { get; set; }

    public PlannerPartInfo(PlannerPart instance)
    {
        _instance = instance;
    }
    private PlannerPart _instance;
    public PlannerPart Instance
    {
        get
        {
            return _instance;
        }
        set
        {
            _instance = value;
        }
    }

    public override string ToString()
    {
        string str = string.Format("{0} ({1}) - Type: {2} - State: {3} - BlockedType: {4}", Name, Road, Type.ToString(), State.ToString(), BlockedType.ToString());
        if (Adjactents.Length > 0)
        {
            str = string.Format("{0} - AdjacentCount: {1}", str, Adjactents.Length);
        }
        return str;
    }
}
