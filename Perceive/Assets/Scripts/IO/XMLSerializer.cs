﻿using UnityEngine;
using System.Collections;
using System.Xml.Serialization;
using System.Collections.Generic;


#region Experiments
[XmlRoot("Experiments")]
public class ExperimentXML
{
    [XmlAttribute("Name")]
    public int Size { get; set; }
    [XmlArrayItem("Name")]
    public string[] Name { get; set; }
    [XmlArrayItem("Position")]
    public string[] Position { get; set; } //Vector3
    [XmlArrayItem("Rotation")]
    public string[] Rotation { get; set; } //Quaternion
    [XmlArrayItem("Scale")]
    public string[] Scale { get; set; } //Vector3
    [XmlArrayItem("Type")]
    public Enums.ExperimentType[] Type { get; set; } //Enum
}

[XmlRoot("ExperimentsResults")]
public class ExperimentResultsXML
{
    [XmlAttribute("Name")]
    public int Size { get; set; }
    [XmlArrayItem("Name")]
    public string[] Name { get; set; }
    [XmlArrayItem("Position")]
    public string[] Position { get; set; } //Vector3
    [XmlArrayItem("Rotation")]
    public string[] Rotation { get; set; } //Quaternion
    [XmlArrayItem("Scale")]
    public string[] Scale { get; set; } //Vector3
    [XmlArrayItem("Type")]
    public Enums.ExperimentType[] Type { get; set; } //Enum
    [XmlArrayItem("Result")]
    public string[] Result { get; set; } //Generic string result
}

#endregion

#region Global save
/// <summary>
/// This class is used to set the path and handle AI
/// </summary>
[XmlRoot("SessionRecord")]
public class SavedGame
{
    [XmlAttribute("Date")]
    public System.DateTime Date { get; set; }
    [XmlArray("SavedData")]
    [XmlArrayItem("Data")]
    public List<GameData> Data { get; set; }
    [XmlAttribute("DetectionAngle")]
    public float DetectionAngle { get; set; }
    [XmlAttribute("Tick")]
    public float Tick { get; set; }
    [XmlAttribute("DetectionDistance")]
    public float DetectionDistance { get; set; }
    [XmlAttribute("PlannerFile")]
    public string PlannerFile { get; set; }
    [XmlAttribute("LightRandom")]
    public bool isLightRandom { get; set; }
    [XmlAttribute("Rail")]
    public bool isRail { get; set; }
}

public class GameData
{
    [XmlAttribute("Time")]
    public float Time { get; set; }
    [XmlAttribute("PlayerPosition")]
    public string PlayerPosition { get; set; }
    [XmlAttribute("PlayerRotation")]
    public string PlayerRotation { get; set; }
    [XmlAttribute("PlayerSpeed")]
    public float PlayerSpeed { get; set; }
    [XmlAttribute("CurrentExperiment")]
    public string CurrentExperiment { get; set; }
    [XmlArray("AI")]
    [XmlArrayItem("item")]
    public AIRow[] DetectedAI { get; set; }

}

#endregion

#region AI
/// <summary>
/// This class is used to set the path and handle AI
/// </summary>
[XmlRoot("SessionRecord")]
public class SavedAI
{
    [XmlArray("SavedData")]
    [XmlArrayItem("Data")]
    public List<AIData> Data { get; set; }
}

public class AIRow
{
    [XmlAttribute("AIName")]
    public string Name { get; set; }
    [XmlAttribute("AIPosition")]
    public string Position { get; set; }
    [XmlAttribute("AIRotation")]
    public string Rotation { get; set; }
    [XmlAttribute("AIType")]
    public Enums.AIType Type { get; set; }
    [XmlAttribute("AISpeed")]
    public float Speed { get; set; }
    [XmlAttribute("AIState")]
    public Enums.AIState AIState { get; set; }
    [XmlAttribute("DistanceToPlayer")]
    public float DistanceToPlayer { get; set; }
}

public class AIData
{
    [XmlAttribute("Time")]
    public float Time { get; set; }
    [XmlArray("AI")]
    [XmlArrayItem("item")]
    public AIRow[] ActiveAI { get; set; }
}

#endregion

#region RoadPlanner
/// <summary>
/// This class is used to set the path and handle AI
/// </summary>
[XmlRoot("RoadFile")]
public class RoadFile
{
    [XmlArray("Road")]
    [XmlArrayItem("RoadData")]
    public GizmoData[] Data { get; set; }
}
/// <summary>
/// This class is used to save the road path data (waypoints, etc...)
/// </summary>
public class GizmoData
{
    [XmlAttribute("ID")]
    public int ID { get; set; }
    [XmlAttribute("SplitID")]
    public int SplitID { get; set; }
    [XmlAttribute("Total")]
    public int Total { get; set; }
    [XmlAttribute("Name")]
    public string Name { get; set; }
    [XmlAttribute("DrawBezier")]
    public bool DrawBezier { get; set; }
    [XmlAttribute("BezierPrecision")]
    public float BezierPrecision { get; set; }
    [XmlAttribute("BezierType")]
    public Enums.Bezier BezierType { get; set; }
    [XmlAttribute("State")]
    public Enums.WaypointState State { get; set; }
    [XmlAttribute("SplitState")]
    public Enums.SplitState SplitState { get; set; }
    [XmlAttribute("BezierPoints")]
    public string[] BezierPointString { get; set; }
    [XmlAttribute("_P1")]
    public string P1 { get; set; }
    [XmlAttribute("_P2")]
    public string P2 { get; set; }

    [XmlAttribute("IntersectionType")]
    public Enums.Intersection IntersectionType { get; set; }
    [XmlAttribute("IntersectBezierPointsLEFT")]
    public string[] IntersectBezierPointsString01 { get; set; }
    [XmlAttribute("_P1(LEFT)")]
    public string P1L { get; set; }
    [XmlAttribute("IntersectBezierPointsRIGHT")]
    public string[] IntersectBezierPointsString02 { get; set; }
    [XmlAttribute("_P1(RIGHT)")]
    public string P1R { get; set; }
    [XmlAttribute("IntersectBezierPointsTOP")]
    public string[] IntersectBezierPointsString03 { get; set; }
    [XmlAttribute("_P1(TOP)")]
    public string P1T { get; set; }
    [XmlAttribute("IntersectBezierPointsDOWN")]
    public string[] IntersectBezierPointsString04 { get; set; }
    [XmlAttribute("_P1(DOWN)")]
    public string P1D { get; set; }
}
#endregion

#region PathPlanner
[XmlRoot("RoadPlanner")]
public class RoadPlannerXML
{
    [XmlArray("AllowedPoints")]
    [XmlArrayItem("AllowedPointsItem")]
    public string[] AllowedPoints { get; set; } //Vector3

    [XmlArray("PlannedRoad")]
    [XmlArrayItem("PlannedRoadItem")]
    public string[] PlannedRoad { get; set; } //Vector3

    [XmlArrayItem("Experiment01Points")]
    public string[] Experiment01Points { get; set; } //Vector3

    [XmlAttribute("StartPosition")]
    public string StartPosition { get; set; } //Vector3
    [XmlAttribute("PopSide")]
    public int Side { get; set; } //A   B
    [XmlAttribute("EndPosition")]
    public string EndPosition { get; set; } //Vector3
    [XmlArray("AdjacentPoints")]
    [XmlArrayItem("AdjacentPointsItem")]
    public RoadPlannerAdjacentXML[] AdjacentPoints { get; set; }
}

public class RoadPlannerAdjacentXML
{
    [XmlAttribute("Position")]
    public string Position { get; set; }
    [XmlAttribute("Type")]
    public Enums.PlannerBlockedType BlockedType { get; set; }
}

#endregion

#region TrafficLights
[XmlRoot("TrafficLightsParameters")]
public class TrafficLightsXML
{
    [XmlArray("Traffic")]
    [XmlArrayItem("PositionItem")]
    public string[] Position { get; set; }
    [XmlArrayItem("DelayItem")]
    public float[] Delay { get; set; }
    [XmlArrayItem("RedToGreenItem")]
    public float[] RToG { get; set; }
    [XmlArrayItem("YellowToRed")]
    public float[] YToR { get; set; }
    [XmlArrayItem("DefaultGreen")]
    public string[] DefaultGreen { get; set; }
}


[XmlRoot("TrafficLightsDetails")]
public class TrafficLightsDetailsXML
{
    [XmlArray("Traffic")]
    [XmlArrayItem("Position")]
    public string[] Position { get; set; }
    [XmlArrayItem("DelayItem")]
    public List<TrafficLightsDetailsStateXML> Data { get; set; }
}

public class TrafficLightsDetailsStateXML
{
    [XmlAttribute("01")]
    public Enums.LightState[] State01 { get; set; }
    [XmlAttribute("02")]
    public Enums.LightState[] State02 { get; set; }
    [XmlAttribute("03")]
    public Enums.LightState[] State03 { get; set; }
    [XmlAttribute("04")]
    public Enums.LightState[] State04 { get; set; }

    public TrafficLightsDetailsStateXML(TrafficLightGroupController[] lights)
    {
        int size = lights.Length;
        State01 = new Enums.LightState[size];
        State02 = new Enums.LightState[size];
        State03 = new Enums.LightState[size];
        State04 = new Enums.LightState[size];
        for (int cnt = 0; cnt < size; cnt++)
        {
            State01[cnt] = lights[cnt].GetLightsStatus(0);
            State02[cnt] = lights[cnt].GetLightsStatus(1);
            State03[cnt] = lights[cnt].GetLightsStatus(2);
            State04[cnt] = lights[cnt].GetLightsStatus(3);
        }
    }

    public TrafficLightsDetailsStateXML()
    {

    }
}

#endregion
