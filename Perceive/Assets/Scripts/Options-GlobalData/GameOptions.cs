﻿using UnityEngine;
using System.Collections;
using System;

public delegate void OptionsChangedEvent(object sender, EventArgs e);
public class GameOptions
{
    #region Events
    //Event:
    public event OptionsChangedEvent OptionsChanged;
    protected virtual void OnOptionsChanged(EventArgs e)
    {
        if (OptionsChanged != null)
            OptionsChanged(this, e);
    }
    #endregion

    #region Properties
    private static GameOptions _instance = null;
    public static GameOptions Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new GameOptions();
            }
            return _instance;
        }
    }

    #region Options

    #region Graphics
    public int Quality { get; set; }
    public int Resolution { get; set; }
    public int Vegetation { get; set; }
    #endregion

    #region Saves
    public string SavePath { get; set; }
    public float DataInterval { get; set; }
    #endregion

    #region Car
    public float PlayerMaxSpeed { get; set; }
    public float PlayerAcceleration { get; set; }
    public float PlayerDeceleration { get; set; }
    #endregion

    #endregion

    #region Experimentation

    #region AI
    public int CarCount { get; set; }
    public string[] CarModels { get; set; }
    public string[] SelectedCars { get; set; }
    public float[] CarMaxSpeed { get; set; }
    public float[] CarStopDuration { get; set; }

    public int PedestrianCount { get; set; }
    public string[] PedestrianModels { get; set; }
    public string[] SelectedPedestrians { get; set; }
    public float[] PedestrianMaxSpeed { get; set; }
    #endregion

    #region PathPlanner
    public bool isLightsRandom { get; set; }
    public bool isRail { get; set; }
    public bool isPlanner { get; set; }
    public string PlannedPathFile { get; set; }
    public string PlannedPathExperimentFile { get; set; }

    #endregion

    #region Experiment01
    public float Exp01TimeGlobal { get; set; }
    public float Exp01TimeBlack { get; set; }
    public float Exp01Offset { get; set; }
    #endregion
    #endregion

    #endregion
    private GameOptions()
    {
        LoadOptions();
    }

    public void LoadOptions()
    {
        #region Options

        #region Graphics
        if (PlayerPrefs.HasKey("resolution"))
            Resolution = PlayerPrefs.GetInt("resolution");
        else
            Resolution = 0;

        if (PlayerPrefs.HasKey("qualitySettings"))
            Quality = PlayerPrefs.GetInt("qualitySettings");
        else
            Quality = 0;

        if (PlayerPrefs.HasKey("vegetation"))
            Vegetation = PlayerPrefs.GetInt("vegetation");
        else
            Vegetation = 3;
        #endregion

        #region Save
        if (PlayerPrefs.HasKey("savePath"))
            SavePath = PlayerPrefs.GetString("savePath");
        else
        {
            if (!System.IO.Directory.Exists(string.Format("{0}/Saves", Application.dataPath)))
                System.IO.Directory.CreateDirectory(string.Format("{0}/Saves", Application.dataPath));
            SavePath = string.Format("{0}/Saves", Application.dataPath);
        }
        if (PlayerPrefs.HasKey("dataInterval"))
            DataInterval = PlayerPrefs.GetFloat("dataInterval");
        else
            DataInterval = 0.1f;
        #endregion

        #region Car
        if (PlayerPrefs.HasKey("playerMaxSpeed"))
            PlayerMaxSpeed = PlayerPrefs.GetFloat("playerMaxSpeed");
        else
            PlayerMaxSpeed = 50.0f;

        if (PlayerPrefs.HasKey("playerAcceleration"))
            PlayerAcceleration = PlayerPrefs.GetFloat("playerAcceleration");
        else
            PlayerAcceleration = 1.0f;

        if (PlayerPrefs.HasKey("playerDeceleration"))
            PlayerDeceleration = PlayerPrefs.GetFloat("playerDeceleration");
        else
            PlayerDeceleration = 2.0f;
        #endregion
        #endregion

        #region Experimentation

        #region AI
        if (PlayerPrefs.HasKey("carCount"))
            CarCount = PlayerPrefs.GetInt("carCount");
        else
            CarCount = 0;

        if (PlayerPrefs.HasKey("carModels"))
            CarModels = PlayerPrefsGame.GetStringArray("carModels");
        else
            CarModels = new string[2] { "Cop", "MiniTruck" };

        if (PlayerPrefs.HasKey("selectedCars"))
            SelectedCars = PlayerPrefsGame.GetStringArray("selectedCars");
        else
            SelectedCars = null;

        if (PlayerPrefs.HasKey("carSpeed"))
            CarMaxSpeed = PlayerPrefsGame.GetFloatArray("carSpeed");
        else
            CarMaxSpeed = new float[2] { 50, 40 };

        if (PlayerPrefs.HasKey("carStopDuration"))
            CarStopDuration = PlayerPrefsGame.GetFloatArray("carStopDuration");
        else
            CarStopDuration = new float[2] { 2, 3 };

        if (PlayerPrefs.HasKey("pedCount"))
            PedestrianCount = PlayerPrefs.GetInt("pedCount");
        else
            PedestrianCount = 0;

        if (PlayerPrefs.HasKey("pedModels"))
            PedestrianModels = PlayerPrefsGame.GetStringArray("pedModels");
        else
            PedestrianModels = new string[4] { "Girl01", "Girl02", "Boy01", "Boy02" };

        if (PlayerPrefs.HasKey("selectedPeds"))
            SelectedPedestrians = PlayerPrefsGame.GetStringArray("selectedPeds");
        else
            SelectedPedestrians = null;

        if (PlayerPrefs.HasKey("pedSpeed"))
            PedestrianMaxSpeed = PlayerPrefsGame.GetFloatArray("pedSpeed");
        else
            PedestrianMaxSpeed = new float[4] { 40, 40, 40, 40 };
        #endregion

        #region PathPlanner
        if (PlayerPrefs.HasKey("pathPlannerFile"))
            PlannedPathFile = PlayerPrefs.GetString("pathPlannerFile");
        else
            PlannedPathFile = "Aucun fichier sélectioné";
        if (PlayerPrefs.HasKey("pathPlannerFile"))
            PlannedPathExperimentFile = PlayerPrefs.GetString("pathPlannerExpFile");
        else
            PlannedPathExperimentFile = "";
        if (PlayerPrefs.HasKey("isLightsRandom"))
            isLightsRandom = PlayerPrefsGame.GetBool("isLightsRandom");
        else
            isLightsRandom = true;
        if (PlayerPrefs.HasKey("isPlanner"))
            isPlanner = PlayerPrefsGame.GetBool("isPlanner");
        else
            isPlanner = true;
        if (PlayerPrefs.HasKey("isRail"))
            isRail = PlayerPrefsGame.GetBool("isRail");
        else
            isRail = false;
        #endregion

        #region Experiment01
        if (PlayerPrefs.HasKey("exp01TimeGlobal"))
            Exp01TimeGlobal = PlayerPrefs.GetFloat("exp01TimeGlobal");
        else
            Exp01TimeGlobal = 2.0f;
        if (PlayerPrefs.HasKey("exp01TimeBlack"))
            Exp01TimeBlack = PlayerPrefs.GetFloat("exp01TimeBlack");
        else
            Exp01TimeBlack = 2.0f;
        if (PlayerPrefs.HasKey("exp01Offset"))
            Exp01Offset = PlayerPrefs.GetFloat("exp01Offset");
        else
            Exp01Offset = 0.0f;
        #endregion

        #endregion
    }

    public void Save()
    {
        #region Options

        #region Graphics
        PlayerPrefs.SetInt("resolution", Resolution);
        PlayerPrefs.SetInt("qualitySettings", Quality);
        PlayerPrefs.SetInt("vegetation", Vegetation);
        #endregion

        #region Save
        PlayerPrefs.SetString("savePath", SavePath);
        PlayerPrefs.SetFloat("dataInterval", DataInterval);
        #endregion

        #region Car
        PlayerPrefs.SetFloat("playerMaxSpeed", PlayerMaxSpeed);
        PlayerPrefs.SetFloat("playerAcceleration", PlayerAcceleration);
        PlayerPrefs.SetFloat("playerDeceleration", PlayerDeceleration);
        #endregion
        #endregion

        #region Experimentation

        #region AI
        PlayerPrefs.SetInt("carCount", CarCount);
        PlayerPrefsGame.SetStringArray("carModels", CarModels);
        if(SelectedCars != null)
            PlayerPrefsGame.SetStringArray("selectedCars", SelectedCars);
        PlayerPrefsGame.SetFloatArray("carSpeed", CarMaxSpeed);
        PlayerPrefsGame.SetFloatArray("carStopDuration", CarStopDuration);

        PlayerPrefs.SetInt("pedCount", PedestrianCount);
        PlayerPrefsGame.SetStringArray("pedModels", PedestrianModels);
        if(SelectedPedestrians != null)
            PlayerPrefsGame.SetStringArray("selectedPeds", SelectedPedestrians);
        PlayerPrefsGame.SetFloatArray("pedSpeed", PedestrianMaxSpeed);
        #endregion

        #region PathPlanner
        PlayerPrefs.SetString("pathPlannerFile", PlannedPathFile);
        PlayerPrefs.SetString("pathPlannerExpFile", PlannedPathExperimentFile);
        PlayerPrefsGame.SetBool("isLightsRandom", isLightsRandom);
        PlayerPrefsGame.SetBool("isPlanner", isPlanner);
        PlayerPrefsGame.SetBool("isRail", isRail);
        #endregion

        #region Experiment01
        PlayerPrefs.SetFloat("exp01TimeGlobal", Exp01TimeGlobal);
        PlayerPrefs.SetFloat("exp01TimeBlack", Exp01TimeBlack);
        PlayerPrefs.SetFloat("exp01Offset", Exp01Offset);
        #endregion
        #endregion

        //Changed event:
        OnOptionsChanged(new EventArgs());
    }
}
