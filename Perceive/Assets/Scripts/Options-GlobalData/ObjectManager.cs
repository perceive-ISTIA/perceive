﻿using UnityEngine;
using System.Collections;
using System;

public class ObjectManager : MonoBehaviour
{
    public Flare[] TrafficLightFlares;

    public Material PathPlannerAllowedMaterial;
    public Material PathPlannerBlockedMaterial;
    public Material PathPlannerEventMaterial;
    public Material PathPlannerOutlinedMaterial;
    public Material PathPlannerSelectedMaterial;

    public Material ExperimentOutlinedMaterial;
    public Material ExperimentNormalMaterial;

    public static string[] Resolutions = new string[4] { "1920x1080", "1600x900", "1366x768", "1280x720" };
    public static string[] Quality = new string[6] { Enums.QualitySettings.Fastest.ToString(), Enums.QualitySettings.Fast.ToString(), Enums.QualitySettings.Simple.ToString(), Enums.QualitySettings.Good.ToString(), Enums.QualitySettings.Beautiful.ToString(), Enums.QualitySettings.Fantastic.ToString() };

    private static GameOptions _options;


    public static TrafficLightGroupController[] TrafficLights;
    private static int _trafficLightsLength = 0;
    public static int TrafficLightsLength
    {
        get
        {
            return _trafficLightsLength;
        }
    }

    public static Road[] Roads;
    public static Waypoint[][] Waypoints;
    public static Waypoint[][] SplitPoints;
    private static int _roadsLength = 0;
    public static int RoadsLength
    {
        get
        {
            return _roadsLength;
        }
    }

    void Awake()
    {
        //Load graphics options :
        _options = GameOptions.Instance;
        _options.OptionsChanged += new OptionsChangedEvent(this._options_OptionsChanged);
        _options.LoadOptions();

        InitStaticObjects();

        string[] split = Resolutions[_options.Resolution].Split('x');
        int width = int.Parse(split[0]);
        int height = int.Parse(split[1]);
        Screen.SetResolution(width, height, true);

        QualitySettings.SetQualityLevel(_options.Quality);
    }


    private void InitStaticObjects()
    {
        GameObject[] tmp = GameObject.FindGameObjectsWithTag("Road");
        _roadsLength = tmp.Length;
        Roads = new Road[_roadsLength];
        for (int cnt = 0; cnt < _roadsLength; cnt++ )
        {
            Roads[cnt] = tmp[cnt].GetComponent<Road>();
            Roads[cnt].ID = cnt;
        }

        tmp = GameObject.FindGameObjectsWithTag("LightIntersection");
        _trafficLightsLength = tmp.Length;
        TrafficLights = new TrafficLightGroupController[_trafficLightsLength];
        for (int cnt = 0; cnt < _trafficLightsLength; cnt++)
        {
            TrafficLights[cnt] = tmp[cnt].GetComponent<TrafficLightGroupController>();
        }

        Waypoints = new Waypoint[_roadsLength][];
        SplitPoints = new Waypoint[_roadsLength][];
        for(int cnt = 0; cnt < _roadsLength; cnt ++)
        {
            Waypoints[cnt] = new Waypoint[Roads[cnt].transform.childCount];
            
            for (int i = 0; i < Roads[cnt].transform.childCount; i++ )
            {
                Waypoints[cnt][i] = Roads[cnt].transform.GetChild(i).GetComponent<Waypoint>();
            }
            if (Roads[cnt].SplitPoints.Count > 0)
            {
                SplitPoints[cnt] = new Waypoint[Roads[cnt].SplitPoints.Count];
                for (int i = 0; i < Roads[cnt].SplitPoints.Count; i++)
                {
                    if(Roads[cnt].SplitPoints[i] != null)
                        SplitPoints[cnt][i] = Roads[cnt].SplitPoints[i].GetComponent<Waypoint>();
                }
            }
        }
    }

    public static GameOptions GetOptions()
    {
        return _options;
    }

    private void _options_OptionsChanged(object sender, EventArgs e)
    {
        _options.LoadOptions();
    }

}
