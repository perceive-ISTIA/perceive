﻿using UnityEngine;
using System.Collections;

public static class Enums
{
    /// <summary>
    /// Indicates the interserction state of the waypoint
    /// </summary>
    public enum Intersection { None, Lights, Stop, Right, SimpleCrossing, Split2 };
    /// <summary>
    /// Bezier algorithms
    /// </summary>
    public enum Bezier { Quadratic, Cubic };
    /// <summary>
    /// Set the behavior of an AI
    /// </summary>
    public enum AIType { Pedestrian, Vehicle };
    /// <summary>
    /// Indicates if the waypoint is the start/end of the road
    /// </summary>
    public enum WaypointState { None, Begin, End };
    public enum SplitState { None, Begin, Split, End };
    public enum AIState { Moving, IntersecStop, IntersecLights, IntersecRight, IntersecSimpleCrossing, TrafficJam, Stopped, OnSpawn };

    public enum PlannerPart { DoubleRoad, SingleRoad, Intersection, SimpleCrossing };

    public enum QualitySettings { Fastest, Fast, Simple, Good, Beautiful, Fantastic};

    public enum PlannerState { Allowed, Blocked, BlockedEvent };

    public enum PlannerBlockedType { TrafficJam, Construction, RoadBlockConcrete, RoadBlockBarricade, None };

    public enum ExperimentType { Experiment01 };

    public enum LightState { Red, Yellow, Green, None, All };

    public enum IntersectionLightType { Carrefour, IntersectionT, Simple, Clignotement };
}
