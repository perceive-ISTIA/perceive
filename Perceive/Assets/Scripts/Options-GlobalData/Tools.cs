﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngineInternal;
using System.IO;
using System.Text;

namespace UnityEngine
{
    public static class Tools
    {
        public enum EnableStatement { Enable, Disable, Default };

        public static Vector3 QuadraticBezier(Vector3 P0, Vector3 P2, float t, int side)
        {
            Vector3 P1;
            if(side == 1)
                P1 = new Vector3(P0.x, /*(P0.y + P2.y)/2.0f*/P0.y, P2.z);
            else
                P1 = new Vector3(P2.x, /*(P0.y + P2.y)/2.0f*/P0.y, P0.z);
            return (Mathf.Pow((1-t), 2) * P0 + 2*t*(1-t)*P1 + Mathf.Pow(t, 2) * P2);
        }
        public static Vector3 QuadraticBezier(Vector3 P0, Vector3 P1, Vector3 P2, float t)
        {
            return (Mathf.Pow((1 - t), 2) * P0 + 2 * t * (1 - t) * P1 + Mathf.Pow(t, 2) * P2);
        }
        public static Vector3 CubicBezier(Vector3 P0, Vector3 P1, Vector3 P2, Vector3 P3, float t)
        {
            return (Mathf.Pow((1 - t), 3) * P0 + 3 * t * Mathf.Pow((1 - t), 2) * P1 + 3 * Mathf.Pow(t, 2) * (1 - t) * P2 + P3 * Mathf.Pow(t, 3));
        }


        public static List<T> GetComponentsInChildren<T>(Transform baseTr) where T : UnityEngine.Component
        {
            List<T> list = new List<T>();
            if (baseTr.GetComponent(typeof(T)) != null)
            {
                list.AddRange(baseTr.GetComponents<T>());
            }
            if (baseTr.childCount > 0)
            {
                foreach (Transform tr in baseTr)
                {
                    if (tr.GetComponent(typeof(T)) != null)
                        list.AddRange(tr.GetComponents<T>());
                    if (tr.childCount > 0)
                    {
                        List<T> tmp = GetComponentsInChildren<T>(tr);
                        list.AddRange(tmp);
                    }
                }
            }
            return list;
        }

        public static Vector3 ParseStringToVector3(string str)
        {
            try
            {
                string[] vectorParts = str.Split(',');
                return new Vector3(
                    float.Parse(vectorParts[0].Replace("(", "").Trim()),
                    float.Parse(vectorParts[1].Trim()),
                    float.Parse(vectorParts[2].Replace(")", "").Trim()));
            }
            catch (Exception e)
            {
                Debug.Log(str);
                return Vector3.forward;
            }
        }

        public static Quaternion ParseStringToQuaternion(string str)
        {
            string[] quaternionParts = str.Split(',');
            return new Quaternion(
                float.Parse(quaternionParts[0].Replace("(", "").Trim()),
                float.Parse(quaternionParts[1].Trim()),
                float.Parse(quaternionParts[2].Trim()),
                float.Parse(quaternionParts[3].Replace(")", "").Trim()));
        }

        public static string ParseVector3ToString(Vector3 vector)
        {
            return string.Format("({0},{1},{2})", vector.x, vector.y, vector.z);
        }

        public static string ParseQuaternionToString(Quaternion quaternion)
        {
            return string.Format("({0},{1},{2},{3})", quaternion.x, quaternion.y, quaternion.z, quaternion.w);
        }

        public static void AppendToCSV(string line, string path)
        {
            using(StreamWriter wr = File.AppendText(path))
            {
                wr.WriteLine(line);
            }
        }


        /// <summary>
        /// Simple bubble sort
        /// </summary>
        /// <param name="list"></param>
        /// <param name="playerPos"></param>
        public static void BubbleSortDistance(Transform[] list, Vector3 playerPos)
        {
            if (list == null)
                return;
            Transform tmp;
            bool sort = true;
            int i = list.Length;

            //Saving transforms
            Transform[] tr = new Transform[i];
            list.CopyTo(tr, 0);
            do
            {
                sort = false;
                for (int j = 0; j < i - 1; j++)
                {
                    if (Vector3.Distance(tr[j].position, playerPos) > Vector3.Distance(tr[j + 1].position, playerPos))
                    {
                        tmp = list[j];
                        list[j] = list[j + 1];
                        list[j + 1] = tmp;
                        sort = true;
                    }
                }
                i--;

            } while (sort && i > 0);

            foreach (Transform ai in tr)
            {
                Debug.Log(Vector3.Distance(ai.position, playerPos));
            }
            Debug.Log("-------------");
        }

        /// <summary>
        /// Simple bubble sort
        /// </summary>
        /// <param name="list"></param>
        /// <param name="playerPos"></param>
        public static void BubbleSortDistance(GameObject[] list, Vector3 playerPos)
        {
            if (list == null)
                return;
            GameObject tmp;
            bool sort = true;
            int i = list.Length;

            //Saving transforms
            Transform[] tr = new Transform[i];
            for (int x = 0; x < i; x++)
            {
                tr[x] = list[x].transform;
            }
            do
            {
                sort = false;
                for (int j = 0; j < i - 1; j++)
                {
                    if ((tr[j].position - playerPos).sqrMagnitude > (tr[j + 1].position - playerPos).sqrMagnitude)
                    {
                        tmp = list[j];
                        list[j] = list[j + 1];
                        list[j + 1] = tmp;
                        sort = true;
                    }
                }
                i--;

            } while (sort && i > 0);
        }

        public static bool CheckPointInTriangle(Vector3 point, Vector3[] triPoints)
        {
            if (triPoints.Length != 3)
                return false;
            Vector3 v0 = triPoints[2] - triPoints[0];
            Vector3 v1 = triPoints[1] - triPoints[0];
            Vector3 v2 = point - triPoints[0];

            //Dot products
            float dot00 = Vector3.Dot(v0, v0);
            float dot01 = Vector3.Dot(v0, v1);
            float dot02 = Vector3.Dot(v0, v2);
            float dot11 = Vector3.Dot(v1, v1);
            float dot12 = Vector3.Dot(v1, v2);

            //Compute barycentric coodinates
            float invDen = 1f / (dot00 * dot11 - dot01 * dot01);
            float u = (dot11 * dot02 - dot01 * dot12) * invDen;
            float v = (dot00 * dot12 - dot01 * dot02) * invDen;

            //Check if the point is in triangle
            return (u >= 0) && (v >= 0) && (u + v < 1);
        }

        #region Rendering functions
        public static Bounds GetBounds(GameObject obj)
        {
            List<Renderer> tmp;
            tmp = GetChildsRenderersCenters(obj.transform);
            Bounds bounds = tmp[0].bounds;
            foreach (Renderer rnd in tmp)
            {
                bounds.Encapsulate(rnd.bounds);
            }

            return bounds;
        }

        public static List<Renderer> GetChildsRenderersCenters(Transform baseTr)
        {
            List<Renderer> rnd = new List<Renderer>();
            if (baseTr.GetComponent<Renderer>() != null)
            {
                if (!rnd.Contains(baseTr.GetComponent<Renderer>()))
                    rnd.Add(baseTr.GetComponent<Renderer>());
            }
            foreach (Transform tr in baseTr)
            {
                if (tr.GetComponent<Renderer>() != null)
                {
                    if (!rnd.Contains(tr.GetComponent<Renderer>()))
                        rnd.Add(tr.GetComponent<Renderer>());
                }
                if (tr.childCount > 0)
                {
                    List<Renderer> childs = GetChildsRenderersCenters(tr);
                    foreach (Renderer v3 in childs)
                    {
                        if (!rnd.Contains(v3))
                            rnd.Add(v3);
                    }
                }
            }
            return rnd;
        }
        #endregion


        #region Maths
        /// <summary>
        /// Calculate an acceleration with a second order model
        /// </summary>
        /// <param name="MaxSpeed">Max speed of the system</param>
        /// <param name="deltaMaxSpeed">Used to calculate the final value. This value is the MaxSpeed - (MaxSpeed * delta) (percentage)</param>
        /// <param name="wn">Natural frequency</param>
        /// <param name="z">Damping coefficient</param>
        /// <param name="deltaTime">Time interval between each point</param>
        /// <returns>Acceleration curve</returns>
        public static List<float> CalculateSpeedCurve(float MaxSpeed, float deltaMaxSpeed = 0.02f, float wn = 1.5f, float z = 1.1f, float deltaTime = 0.1f)
        {
            List<float> speed = new List<float>();
            if(z == 1.1f)
                z = (MaxSpeed * 2/* + 1.1f) *// 100f) + 1.1f;
            float l1 = -z * wn - wn * Mathf.Sqrt(Mathf.Pow(z, 2) - 1);
            float l2 = -z * wn + wn * Mathf.Sqrt(Mathf.Pow(z, 2) - 1);
            float K1 = (MaxSpeed * l2) / (l2 - l1);
            float K2 = (MaxSpeed * l1) / (l2 - l1);

            float t = 0.0f;
            float deltaMax = MaxSpeed - (MaxSpeed * deltaMaxSpeed);
            float Ht = 0.0f;
            do
            {
                Ht = K1 * (1 - Mathf.Exp(l1 * t)) - K2 * (1 - Mathf.Exp(l2 * t));
                speed.Add((Ht * Time.deltaTime) / 4);
                t += deltaTime;
            } while (Ht < deltaMax);

            return speed;
        }


        #endregion
    }
}
