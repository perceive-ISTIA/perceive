﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Cette classe donne accés a une biblioteque de fonction pour enregistrer des 
/// objets complexes dans les players prefs.
/// </summary>
public class  PlayerPrefsGame {

    public static bool GetBool(string key){
        return PlayerPrefs.GetInt(key) == 1;
    }
    public static void SetBool(string key, bool value){
        if (value) PlayerPrefs.SetInt(key, 1);
        else PlayerPrefs.SetInt(key, 0);
    }

    public static T GetEnum<T>(string key){
        return (T)Enum.Parse(typeof(T), PlayerPrefs.GetString(key));
    }
    public static void SetEnum<T>(string key, T value){
        PlayerPrefs.SetString(key, value.ToString());
    }

    public static Resolution GetResolution(string key) {
        return new Resolution(){
            height = PlayerPrefs.GetInt(key + "height"),
            width = PlayerPrefs.GetInt(key + "width"),
            refreshRate = PlayerPrefs.GetInt(key + "refreshRate")
        };
    }
    public static void SetResolution(string key, Resolution value) {
        PlayerPrefs.SetInt(key + "height", value.height);
        PlayerPrefs.SetInt(key + "width", value.width);
        PlayerPrefs.SetInt(key + "refreshRate", value.refreshRate);
    }

    public static Dictionary<TKey, TValue> GetDictionary<TKey, TValue>(string key, Parse<TKey> delegateTKeyParse, Parse<TValue> delegateTValueParse){
        // Number of data in the list
        int nbDictionary = PlayerPrefs.GetInt(key);
        Dictionary<TKey, TValue> dico = new Dictionary<TKey, TValue>(nbDictionary);
        // InsertData
        for (int i = 0; i < nbDictionary; i ++){
            TKey newKey = delegateTKeyParse(PlayerPrefs.GetString(key + "_Key_" + i.ToString()));
            TValue newValue = delegateTValueParse(PlayerPrefs.GetString(key + "_Value_" + i.ToString()));
            dico.Add(newKey, newValue);
        }
        return dico;
    }
    public static void SetDictionary<TKey, TValue>(string key, Dictionary<TKey, TValue> value){
        // Number of data in the list
        PlayerPrefs.SetInt(key, value.Count);
        // InsertData
        int i = 0;
        foreach (KeyValuePair<TKey, TValue> pair in value) {
            PlayerPrefs.SetString(key + "_Key_" + i.ToString(), pair.Key.ToString());
            PlayerPrefs.SetString(key + "_Value_" + i.ToString(), pair.Value.ToString());
            i++;
        }
    }

    public static List<TValue> GetList<TValue>(string key, Parse<TValue> delegateTValueParse)
    {
        // Number of data in the list
        int nbList = PlayerPrefs.GetInt(key);
        List<TValue> list = new List<TValue>(nbList);
        // InsertData
        for (int i = 0; i < nbList; i++)
        {
            TValue newValue = delegateTValueParse(PlayerPrefs.GetString(key + "_Value_" + i.ToString()));
            list.Add(newValue);
        }
        return list;
    }
    public static void SetList<TValue>(string key, List<TValue> value)
    {
        // Number of data in the list
        PlayerPrefs.SetInt(key, value.Count);
        // InsertData
        int i = 0;
        foreach (TValue pair in value)
        {
            PlayerPrefs.SetString(key + "_Value_" + i.ToString(), pair.ToString());
            i++;
        }
    }

    public delegate T Parse<T>(string value);

    static private int endianDiff1;
    static private int endianDiff2;
    static private int idx;
    static private byte[] byteBlock;

    private enum ArrayType { Float, Int32, Bool, String, Vector2, Vector3, Quaternion, Color }

    private static void Initialize()
    {
        if (System.BitConverter.IsLittleEndian)
        {
            endianDiff1 = 0;
            endianDiff2 = 0;
        }
        else
        {
            endianDiff1 = 3;
            endianDiff2 = 1;
        }
        if (byteBlock == null)
        {
            byteBlock = new byte[4];
        }
        idx = 1;
    }
    public static bool SetStringArray(String key, String[] stringArray)
    {
        var bytes = new byte[stringArray.Length + 1];
        bytes[0] = System.Convert.ToByte(ArrayType.String);	// Identifier
        Initialize();

        // Store the length of each string that's in stringArray, so we can extract the correct strings in GetStringArray
        for (var i = 0; i < stringArray.Length; i++)
        {
            if (stringArray[i] == null)
            {
                Debug.LogError("Can't save null entries in the string array when setting " + key);
                return false;
            }
            if (stringArray[i].Length > 255)
            {
                Debug.LogError("Strings cannot be longer than 255 characters when setting " + key);
                return false;
            }
            bytes[idx++] = (byte)stringArray[i].Length;
        }

        try
        {
            PlayerPrefs.SetString(key, System.Convert.ToBase64String(bytes) + "|" + String.Join("", stringArray));
        }
        catch
        {
            return false;
        }
        return true;
    }

    public static String[] GetStringArray(String key)
    {
        if (PlayerPrefs.HasKey(key))
        {
            var completeString = PlayerPrefs.GetString(key);
            var separatorIndex = completeString.IndexOf("|"[0]);
            if (separatorIndex < 4)
            {
                Debug.LogError("Corrupt preference file for " + key);
                return new String[0];
            }
            var bytes = System.Convert.FromBase64String(completeString.Substring(0, separatorIndex));
            if ((ArrayType)bytes[0] != ArrayType.String)
            {
                Debug.LogError(key + " is not a string array");
                return new String[0];
            }
            Initialize();

            var numberOfEntries = bytes.Length - 1;
            var stringArray = new String[numberOfEntries];
            var stringIndex = separatorIndex + 1;
            for (var i = 0; i < numberOfEntries; i++)
            {
                int stringLength = bytes[idx++];
                if (stringIndex + stringLength > completeString.Length)
                {
                    Debug.LogError("Corrupt preference file for " + key);
                    return new String[0];
                }
                stringArray[i] = completeString.Substring(stringIndex, stringLength);
                stringIndex += stringLength;
            }

            return stringArray;
        }
        return new String[0];
    }

    public static String[] GetStringArray(String key, String defaultValue, int defaultSize)
    {
        if (PlayerPrefs.HasKey(key))
        {
            return GetStringArray(key);
        }
        var stringArray = new String[defaultSize];
        for (int i = 0; i < defaultSize; i++)
        {
            stringArray[i] = defaultValue;
        }
        return stringArray;
    }

    public static bool SetFloatArray(String key, float[] floatArray)
    {
        return SetValue(key, floatArray, ArrayType.Float, 1, ConvertFromFloat);
    }

    public static float[] GetFloatArray(String key)
    {
        var floatList = new List<float>();
        GetValue(key, floatList, ArrayType.Float, 1, ConvertToFloat);
        return floatList.ToArray();
    }

    public static float[] GetFloatArray(String key, float defaultValue, int defaultSize)
    {
        if (PlayerPrefs.HasKey(key))
        {
            return GetFloatArray(key);
        }
        var floatArray = new float[defaultSize];
        for (int i = 0; i < defaultSize; i++)
        {
            floatArray[i] = defaultValue;
        }
        return floatArray;
    }

    private static void GetValue<T>(String key, T list, ArrayType arrayType, int vectorNumber, Action<T, byte[]> convert) where T : IList
    {
        if (PlayerPrefs.HasKey(key))
        {
            var bytes = System.Convert.FromBase64String(PlayerPrefs.GetString(key));
            if ((bytes.Length - 1) % (vectorNumber * 4) != 0)
            {
                Debug.LogError("Corrupt preference file for " + key);
                return;
            }
            if ((ArrayType)bytes[0] != arrayType)
            {
                Debug.LogError(key + " is not a " + arrayType.ToString() + " array");
                return;
            }
            Initialize();

            var end = (bytes.Length - 1) / (vectorNumber * 4);
            for (var i = 0; i < end; i++)
            {
                convert(list, bytes);
            }
        }
    }

    private static void ConvertToInt(List<int> list, byte[] bytes)
    {
        list.Add(ConvertBytesToInt32(bytes));
    }

    private static void ConvertToFloat(List<float> list, byte[] bytes)
    {
        list.Add(ConvertBytesToFloat(bytes));
    }

    private static bool SetValue<T>(String key, T array, ArrayType arrayType, int vectorNumber, Action<T, byte[], int> convert) where T : IList
    {
        var bytes = new byte[(4 * array.Count) * vectorNumber + 1];
        bytes[0] = System.Convert.ToByte(arrayType);	// Identifier
        Initialize();

        for (var i = 0; i < array.Count; i++)
        {
            convert(array, bytes, i);
        }
        return SaveBytes(key, bytes);
    }

    private static void ConvertFromFloat(float[] array, byte[] bytes, int i)
    {
        ConvertFloatToBytes(array[i], bytes);
    }

    private static void ConvertFloatToBytes(float f, byte[] bytes)
    {
        byteBlock = System.BitConverter.GetBytes(f);
        ConvertTo4Bytes(bytes);
    }

    private static float ConvertBytesToFloat(byte[] bytes)
    {
        ConvertFrom4Bytes(bytes);
        return System.BitConverter.ToSingle(byteBlock, 0);
    }

    private static void ConvertInt32ToBytes(int i, byte[] bytes)
    {
        byteBlock = System.BitConverter.GetBytes(i);
        ConvertTo4Bytes(bytes);
    }

    private static int ConvertBytesToInt32(byte[] bytes)
    {
        ConvertFrom4Bytes(bytes);
        return System.BitConverter.ToInt32(byteBlock, 0);
    }

    private static void ConvertTo4Bytes(byte[] bytes)
    {
        bytes[idx] = byteBlock[endianDiff1];
        bytes[idx + 1] = byteBlock[1 + endianDiff2];
        bytes[idx + 2] = byteBlock[2 - endianDiff2];
        bytes[idx + 3] = byteBlock[3 - endianDiff1];
        idx += 4;
    }

    private static void ConvertFrom4Bytes(byte[] bytes)
    {
        byteBlock[endianDiff1] = bytes[idx];
        byteBlock[1 + endianDiff2] = bytes[idx + 1];
        byteBlock[2 - endianDiff2] = bytes[idx + 2];
        byteBlock[3 - endianDiff1] = bytes[idx + 3];
        idx += 4;
    }

    private static bool SaveBytes(String key, byte[] bytes)
    {
        try
        {
            PlayerPrefs.SetString(key, System.Convert.ToBase64String(bytes));
        }
        catch
        {
            return false;
        }
        return true;
    }
}
