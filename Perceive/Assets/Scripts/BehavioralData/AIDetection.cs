﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AIDetection : MonoBehaviour
{
    public float Angle = 90f;
    public float Distance = 50f;
    public Transform Player;

    public List<AIComponent> DetectedCarsAI { get; set; }
    private Transform[] _aiCar;
    private int _aiCarSize;

    public List<AIComponent> DetectedPedestriansAI { get; set; }
    private Transform[] _aiPedestrians;
    private int _aiPedestrianSize;

    private GameObject P2;
    private Transform _p2Tr;
    private GameObject P3;
    private Transform _p3Tr;
    private RaycastHit _hit;

    private Transform _thisTransform;

    private bool _init = false;

    void Awake()
    {
        _thisTransform = transform;
    }


	// Use this for initialization
	void Start () 
    {
        P2 = new GameObject("_P2");
        _p2Tr = P2.transform;
        _p2Tr.SetParent(_thisTransform);
        _p2Tr.localScale = new Vector3(1, 1, 1);
        _p2Tr.localPosition = new Vector3(-Mathf.Tan(Angle * Mathf.Deg2Rad / 2.0f) * Distance, 0f, Distance);


        P3 = new GameObject("_P3");
        _p3Tr = P3.transform;
        _p3Tr.SetParent(_thisTransform);
        _p3Tr.localScale = new Vector3(1, 1, 1);
        _p3Tr.localPosition = new Vector3(Mathf.Tan(Angle * Mathf.Deg2Rad / 2.0f) * Distance, 0f, Distance);
	}
	
	// Update is called once per frame
	void Update ()
    {
        if(_init)
        {
            #region Cars
            //Check if the number of AI is the same (no destroy)
            /*if (GameObject.FindGameObjectsWithTag("Car").Length != _aiCar.Length)
                _aiCar = GameObject.FindGameObjectsWithTag("Car");*/

            //Update the sorted array
            //Tools.BubbleSortDistance(_aiCar, Player.position);

            Vector3[] tri = new Vector3[3]
            {
                new Vector3(Player.position.x, 0, Player.position.z),
                new Vector3(_p2Tr.position.x, 0, _p2Tr.position.z),
                new Vector3(_p3Tr.position.x, 0, _p3Tr.position.z)
            };


            //Check if go  is in the vision cone
            DetectedCarsAI = new List<AIComponent>();
            for (int cnt = 0; cnt < _aiCarSize; cnt++)
            {
                /*if (Vector3.Distance(_aiCar[cnt].position, Player.position).sqrMagnitude > Mathf.Pow(Distance, 2))
                    break;*/
                if (Tools.CheckPointInTriangle(new Vector3(_aiCar[cnt].position.x, 0, _aiCar[cnt].position.z), tri))
                {
                    DetectedCarsAI.Add(_aiCar[cnt].GetComponent<AIComponent>());
                }
            }

            
            for(int cnt = 0; cnt < DetectedCarsAI.Count; cnt ++)
            {
                //Debug.DrawLine(Player.position, DetectedCarsAI[cnt].transform.position);
                if (Physics.Raycast(Player.position, (DetectedCarsAI[cnt].transform.position - Player.position).normalized, out _hit, Mathf.Infinity, 1 << 8))
                {
                    if(_hit.transform.name != "Back" && _hit.transform.name != "BackCollider" && _hit.transform.name != "FrontCollider" && _hit.transform.name != "Front" && _hit.transform.name != "Mesh" && _hit.transform.tag != "Car")
                        DetectedCarsAI.Remove(DetectedCarsAI[cnt]);
                }
            }
            #endregion

            #region Pedestrians
            //Check if the number of AI is the same (no destroy)
            /*if (GameObject.FindGameObjectsWithTag("Pedestrian").Length != _aiPedestrians.Length)
                _aiPedestrians = GameObject.FindGameObjectsWithTag("Pedestrian");*/

            //Update the sorted array
            //Tools.BubbleSortDistance(_aiPedestrians, Player.position);
            
            //Check if go  is in the vision cone
            DetectedPedestriansAI = new List<AIComponent>();
            for (int cnt = 0; cnt < _aiPedestrianSize; cnt++)
            {
                /*if (Vector3.Distance(_aiPedestrians[cnt].position, Player.position).sqrMagnitude > Mathf.Pow(Distance, 2))
                    break;*/
                if (Tools.CheckPointInTriangle(new Vector3(_aiPedestrians[cnt].position.x, 0, _aiPedestrians[cnt].position.z), tri))
                    DetectedPedestriansAI.Add(_aiPedestrians[cnt].GetComponent<AIComponent>());
            }

            for (int cnt = 0; cnt < DetectedPedestriansAI.Count; cnt++)
            {
                if (Physics.Raycast(Player.position, (DetectedPedestriansAI[cnt].transform.position - Player.position).normalized, out _hit, Mathf.Infinity, 1 << 8))
                {
                    if (_hit.transform.tag != "Pedestrian")
                    {
                        DetectedPedestriansAI.Remove(DetectedPedestriansAI[cnt]);
                    }
                }
            }
            #endregion
        }
	}

    public void InitDetection()
    {
        GameObject[] tmp  = GameObject.FindGameObjectsWithTag("Car");
        _aiCarSize = tmp.Length;
        _aiCar = new Transform[_aiCarSize];
        for (int cnt = 0; cnt < _aiCarSize; cnt++)
        {
            _aiCar[cnt] = tmp[cnt].transform;
        }

        //Sort the initial list by distance
        //Tools.BubbleSortDistance(_aiCar, Player.position);

        tmp = GameObject.FindGameObjectsWithTag("Pedestrian");
        _aiPedestrianSize = tmp.Length;
        _aiPedestrians = new Transform[_aiPedestrianSize];
        for (int cnt = 0; cnt < _aiPedestrianSize; cnt++)
        {
            _aiPedestrians[cnt] = tmp[cnt].transform;
        }

        //Tools.BubbleSortDistance(_aiPedestrians, Player.position);

        _init = true;
    }

    public void StopDetection()
    {
        _init = false;
        _aiCar = null;
        _aiPedestrians = null;
        DetectedCarsAI = null;
        DetectedPedestriansAI = null;
    }

    void OnDrawGizmos()
    {
        if (P2 != null && P3 != null)
        {
            Gizmos.color = Color.white;
            Gizmos.DrawSphere(_p2Tr.position, 2f);
            Gizmos.DrawSphere(_p3Tr.position, 2f);

            Gizmos.DrawLine(_p2Tr.position, _p3Tr.position);
            Gizmos.DrawLine(_p2Tr.position, Player.position);
            Gizmos.DrawLine(Player.position, _p3Tr.position);
        }
    }
}
