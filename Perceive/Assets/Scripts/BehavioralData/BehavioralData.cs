﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Xml;
using System.Xml.Serialization;
using System.IO;


public class BehavioralData : MonoBehaviour
{
    private Transform _player;
    private SavedGame _data;
    private TrafficLightsDetailsXML _trafficDetails;
    private SavedAI _AIdata;
    public AIDetection AIDetectionScript;
    public AIController AIController;
    public AutoMove AutoMoveObject;
    public ExperimentController ExpController;

    private bool _isAutoMove = false;
    public string LastSaveName { get; set; }

    private float _timeInterval = 0.01f;
    private int _index = 0;
    private bool _isAI = false;
    private string _subjectName = "";
    private string _date;


    //Cache/Temp
    private int _carLength = 0;
    private int _pedestrianLength = 0;

    public void StartRecording(float timeInterval, bool isAI, bool isAutoMove, string subjectName, string plannerFile, bool randomLight, bool rail)
    {
        _data = new SavedGame();
        _AIdata = new SavedAI();
        _trafficDetails = new TrafficLightsDetailsXML();

        _data.Data = new List<GameData>();
        _AIdata.Data = new List<AIData>();
        _player = GameObject.FindGameObjectWithTag("Player").transform;
        _timeInterval = timeInterval;
        _isAI = isAI;
        _subjectName = subjectName;
        _isAutoMove = isAutoMove;
        AIDetectionScript.InitDetection();

        _data.DetectionAngle = AIDetectionScript.Angle;
        _data.DetectionDistance = AIDetectionScript.Distance;
        _data.Tick = timeInterval;
        _data.PlannerFile = plannerFile;
        _data.isLightRandom = randomLight;
        _data.isRail = rail;
        _data.Date = System.DateTime.Now;

        if (_isAI)
            _carLength = AIController.CarList.Length;
        else
            _carLength = 0;
        if (_isAI)
            _pedestrianLength = AIController.PedestrianList.Length;
        else
            _pedestrianLength = 0;

        #region Lights
        _date = System.DateTime.Now.ToString().Replace("/", "").Replace(":", "");
        _date = string.Format("{0}-{1}", _subjectName, _date);
        TrafficLightGroupController.SaveLightsToFile(string.Format("{0}\\Saves\\{1}_LIGHTS.xml", Application.dataPath, _date));

        _trafficDetails.Data = new List<TrafficLightsDetailsStateXML>();
        _trafficDetails.Position = new string[ObjectManager.TrafficLights.Length];
        for (int cnt = 0; cnt < ObjectManager.TrafficLights.Length; cnt++)
        {
            _trafficDetails.Position[cnt] = Tools.ParseVector3ToString(ObjectManager.TrafficLights[cnt].transform.position);
        }
        #endregion

            InvokeRepeating("PutDataToLists", 0f, _timeInterval);
    }

    public float StopRecording()
    {
        CancelInvoke("PutDataToLists");

        if (!Directory.Exists(Application.dataPath + "\\Saves"))
            Directory.CreateDirectory(Application.dataPath + "\\Saves");

        XmlSerializer xml = new XmlSerializer(typeof(SavedGame));
        using (StreamWriter stream = new StreamWriter(string.Format("{0}\\Saves\\{1}_SAVE.xml", Application.dataPath, _date), false, System.Text.Encoding.GetEncoding("UTF-8")))
        {
            xml.Serialize(stream, _data);
            stream.Close();
        }

        xml = new XmlSerializer(typeof(TrafficLightsDetailsXML));
        using (StreamWriter stream = new StreamWriter(string.Format("{0}\\Saves\\{1}_LIGHTSDETAILS.xml", Application.dataPath, _date), false, System.Text.Encoding.GetEncoding("UTF-8")))
        {
            xml.Serialize(stream, _trafficDetails);
            stream.Close();
        }

        if (_isAI)
        {
            xml = new XmlSerializer(typeof(SavedAI));

            using (StreamWriter stream = new StreamWriter(string.Format("{0}\\Saves\\{1}_AI.xml", Application.dataPath, _date), false, System.Text.Encoding.GetEncoding("UTF-8")))
            {
                xml.Serialize(stream, _AIdata);
                stream.Close();
            }
        }
        LastSaveName = string.Format("{0}\\Saves\\{1}_SAVE.xml", Application.dataPath, _date);

        AIDetectionScript.StopDetection();
        ExpController.SaveResults(string.Format("{0}\\Saves\\{1}_RESULTS.xml", Application.dataPath, _date));

        return _timeInterval * _data.Data.Count;
    }

    private void PutDataToLists()
    {
        _data.Data.Add(new GameData());
        _index = _data.Data.Count - 1;
        _data.Data[_index].Time = _timeInterval * _data.Data.Count;
        _data.Data[_index].PlayerPosition = Tools.ParseVector3ToString(_player.position);
        _data.Data[_index].PlayerRotation = Tools.ParseQuaternionToString(_player.rotation);
        _data.Data[_index].CurrentExperiment = ExpController.CurrentExperiment;
        if(_isAutoMove)
            _data.Data[_index].PlayerSpeed = AutoMoveObject.CurrentSpeed;
        else
            _data.Data[_index].PlayerSpeed = 0.0f;

        #region Lights
        _trafficDetails.Data.Add(new TrafficLightsDetailsStateXML(ObjectManager.TrafficLights));
        #endregion


        //Detected AI
        if(_isAI)
        { 
            _data.Data[_index].DetectedAI = new AIRow[AIDetectionScript.DetectedCarsAI.Count + AIDetectionScript.DetectedPedestriansAI.Count];
            for (int cnt = 0; cnt < AIDetectionScript.DetectedCarsAI.Count; cnt++)
            {
                _data.Data[_index].DetectedAI[cnt] = new AIRow();
                _data.Data[_index].DetectedAI[cnt].AIState = AIDetectionScript.DetectedCarsAI[cnt].State;
                _data.Data[_index].DetectedAI[cnt].Name = AIDetectionScript.DetectedCarsAI[cnt].transform.name;
                _data.Data[_index].DetectedAI[cnt].Position = Tools.ParseVector3ToString(AIDetectionScript.DetectedCarsAI[cnt].transform.position);
                _data.Data[_index].DetectedAI[cnt].Rotation = Tools.ParseQuaternionToString(AIDetectionScript.DetectedCarsAI[cnt].transform.rotation);
                _data.Data[_index].DetectedAI[cnt].Speed = AIDetectionScript.DetectedCarsAI[cnt].Speed;
                _data.Data[_index].DetectedAI[cnt].Type = AIDetectionScript.DetectedCarsAI[cnt].Type;
                _data.Data[_index].DetectedAI[cnt].DistanceToPlayer = (AIDetectionScript.DetectedCarsAI[cnt].transform.position - _player.position).magnitude;
            }

        /* ====
         *  AI
         * ==== */
        //Detected AI
            _AIdata.Data.Add(new AIData());
            _AIdata.Data[_index].Time = _timeInterval * _AIdata.Data.Count;
            _AIdata.Data[_index].ActiveAI = new AIRow[_carLength + _pedestrianLength];
            for (int cnt = 0; cnt < _carLength; cnt++)
            {
                _AIdata.Data[_index].ActiveAI[cnt] = new AIRow();
                _AIdata.Data[_index].ActiveAI[cnt].AIState = AIController.CarList[cnt].State;
                _AIdata.Data[_index].ActiveAI[cnt].Name = AIController.CarList[cnt].transform.name;
                _AIdata.Data[_index].ActiveAI[cnt].Position = Tools.ParseVector3ToString(AIController.CarList[cnt].transform.position);
                _AIdata.Data[_index].ActiveAI[cnt].Rotation = Tools.ParseQuaternionToString(AIController.CarList[cnt].transform.rotation);
                _AIdata.Data[_index].ActiveAI[cnt].Speed = AIController.CarList[cnt].Speed;
                _AIdata.Data[_index].ActiveAI[cnt].Type = AIController.CarList[cnt].Type;
                _AIdata.Data[_index].ActiveAI[cnt].DistanceToPlayer = (AIController.CarList[cnt].transform.position - _player.transform.position).magnitude;
            }
        }
    }
}

