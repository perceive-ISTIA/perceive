﻿using UnityEngine;
using System.Collections;

[SerializeField]
public class StartPoint : MonoBehaviour {

    public ExpPath path;
    public Waypoint wp;

    void OnMouseDown()
    {
        if (path != null)
            path.DrawRoad(this);
    }

}
