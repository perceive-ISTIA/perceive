﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ExpExp01 : MonoBehaviour
{

    private Exp01Panel _panel;
    public Exp01Panel panel
    {
        get
        {
            return _panel;
        }
    }

    public Exp01Panel GetNewPanelReference()
    {
        _panel = new Exp01Panel(transform.GetComponent<RectTransform>(), transform);
        return _panel;
    }

    public void Apply()
    {
        _panel.Apply();
    }

    public void Cancel()
    {
        _panel.Cancel();
    }
}

public class Exp01Panel : IPanel
{
    //UI ELEMENTS
    private InputField _inputTimeGlobal;
    private InputField _inputTimeBlack;
    private InputField _inputOffset;
    
    private RectTransform _thisRect;
    public RectTransform rectTransform
    {
        get
        {
            return _thisRect;
        }
        set
        {
            _thisRect = value;
        }
    }

    private Transform _thisTransform;
    public Transform transform
    {
        get
        {
            return _thisTransform;
        }
        set
        {
            _thisTransform = value;
        }
    }




    public Exp01Panel(RectTransform rect, Transform tr)
    {
        _thisRect = rect;
        _thisTransform = tr;

        Transform controls = _thisTransform.FindChild("Controls");

        _inputOffset = controls.FindChild("InputOffset").GetComponent<InputField>();
        _inputTimeBlack = controls.FindChild("InputTimeBlack").GetComponent<InputField>();
        _inputTimeGlobal = controls.FindChild("InputTimeGlobal").GetComponent<InputField>();
    }

    public void Init()
    {
        _inputOffset.text = string.Format("{0}", ObjectManager.GetOptions().Exp01Offset);
        _inputTimeBlack.text = string.Format("{0}", ObjectManager.GetOptions().Exp01TimeBlack);
        _inputTimeGlobal.text = string.Format("{0}", ObjectManager.GetOptions().Exp01TimeGlobal);
    }

    public void Hide()
    {
        _thisTransform.gameObject.SetActive(false);
    }

    public void Show()
    {
        _thisTransform.gameObject.SetActive(true);
    }

    public void Apply()
    {
        ObjectManager.GetOptions().Exp01Offset = float.Parse(_inputOffset.text);
        ObjectManager.GetOptions().Exp01TimeBlack = float.Parse(_inputTimeBlack.text);
        ObjectManager.GetOptions().Exp01TimeGlobal = float.Parse(_inputTimeGlobal.text);

        ObjectManager.GetOptions().Save();

        Hide();
    }

    public void Cancel()
    {
        _inputOffset.text = string.Format("{0}", ObjectManager.GetOptions().Exp01Offset);
        _inputTimeBlack.text = string.Format("{0}", ObjectManager.GetOptions().Exp01TimeBlack);
        _inputTimeGlobal.text = string.Format("{0}", ObjectManager.GetOptions().Exp01TimeGlobal);

        Hide();
    }
}