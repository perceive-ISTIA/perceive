﻿using UnityEngine;
using System.Collections;

public class GUIController : MonoBehaviour
{
    public PauseMenu PanelPause;
    public GameObject Experiment01GUI;
    private Transform _exp01AnswerPanel;
    private Transform _exp01BlackPanel;
    public FinishedMenu PanelFinished;
    public GameObject PanelMainMenu;
    public Replay PanelReplay;
    public AutoMove AutoMoveScript;

    private bool _replayOnPlay = false;
	// Update is called once per frame
    void Awake()
    {
        AutoMoveScript.AutoMoveFinished += new AutoMoveHandler(this.OnAutoMoveFinished);
        _exp01AnswerPanel = Experiment01GUI.transform.FindChild("PanelAnswer");
        _exp01BlackPanel = Experiment01GUI.transform.FindChild("PanelBlack");
    }
	void Update ()
    {
	    if(Input.GetKeyDown(KeyCode.Escape))
        {
            if (!PanelMainMenu.activeInHierarchy)
            {
                PanelPause.gameObject.SetActive(!PanelPause.gameObject.activeInHierarchy);
                if (PanelPause.gameObject.activeInHierarchy)
                {
                    //Freeze the game
                    AutoMoveScript.FreezeMovement();
                    TrafficLightGroupController.PauseAllLights();
                }
                else
                {
                    //Unfreeze the game
                    AutoMoveScript.UnFreezeMovement();
                    TrafficLightGroupController.PlayAllLights();
                }
            }
        }

        if (PanelPause.OnFinished)
        {
            PanelMainMenu.SetActive(true);
            _exp01AnswerPanel.gameObject.SetActive(false);
            _exp01BlackPanel.gameObject.SetActive(false);
            PanelFinished.gameObject.SetActive(true);
            PanelFinished.InitFinishedPanel(PanelPause.LastSimulationTime, PanelPause.LastSaveName);
            PanelPause.OnFinished = false;
            TrafficLightGroupController.PauseAllLights();
        }

        if (PanelReplay.OnPlay && PanelReplay.ReplayMenuGUI.gameObject.activeInHierarchy && !_replayOnPlay)
        {
            PanelMainMenu.SetActive(false);
            PanelFinished.gameObject.SetActive(false);
            _replayOnPlay = true;
        }
        if (_replayOnPlay && !PanelReplay.OnPlay)
        {
            PanelMainMenu.SetActive(true);
            _replayOnPlay = false;
        }
	}

    private void OnAutoMoveFinished(object sender, System.EventArgs args)
    {
        PanelPause.OnBTNFinish(); //Simulate finished click
    }
}
