﻿using UnityEngine;
using System.IO;
using UnityEngine.UI;
using System.Collections;
using System.Reflection;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Xml.Serialization;

public delegate PlannerPartInfo Info();
public delegate IExperimentInfo Exp();
public class ExpPath : MonoBehaviour
{

    public Camera CarCamera;
    public Camera PathPlannerCamera;
    public Camera ExperimentExampleCamera;
    private PathPlanner _pathPlanner;
    private Canvas _thisCanevas;
    public GameObject PathPlannerCanevas;
    public Sprite Intersection3;
    public Sprite Intersection4;
    private bool _onDrawPath = false;
    //Global event:
    public static event Info OnHighlighted;
    public static event Exp OnExperimentHighlighted;
    public static event Info OnSelected;

    private List<GameObject> AttachPoints = new List<GameObject>();
    private List<StartPoint> PlannedRoad = new List<StartPoint>();
    private LineRenderer VisualPlannedRoad;
    private int VisualCount;

    void Awake()
    {
        _pathPlanner = GameObject.Find("PathPlanner").GetComponent<PathPlanner>();
        _thisCanevas = GameObject.Find("GUI").GetComponent<Canvas>();
    }

    public ExpPathPanel GetNewPanelReference()
    {
        _panel = new ExpPathPanel(transform.GetComponent<RectTransform>(), transform, PathPlannerCanevas.transform.FindChild("Panel"), PathPlannerCanevas.transform.FindChild("PanelLights"), PathPlannerCanevas.transform.FindChild("PanelPartState"), PathPlannerCanevas.transform.FindChild("PanelExperiment"), _pathPlanner);
        return _panel;
    }

    void Update()
    {
        if (_onDrawPath)
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                _pathPlanner.ShowRoadMesh = true;
                _onDrawPath = false;

                //Disable caneva
                _thisCanevas.enabled = true;
                PathPlannerCanevas.SetActive(false);

                CarCamera.transform.parent.gameObject.SetActive(true);
                PathPlannerCamera.gameObject.SetActive(false);
            }
            if (OnHighlighted != null && OnSelected == null)
            {
                _panel.UpdateInfos(OnHighlighted());
                return;
            }

            if (OnExperimentHighlighted != null)
            {
                //_panel.UpdateExperimentInfos(OnExperimentHighlighted());
                return;
            }
        }
    }

    private ExpPathPanel _panel;
    public ExpPathPanel panel
    {
        get
        {
            return _panel;
        }
    }

    public void ToggleRailStatus()
    {
        _panel.ToggleRailStatus();
    }

    public void StartSetPath()
    {
        CarCamera.transform.parent.gameObject.SetActive(false);
        PathPlannerCamera.gameObject.SetActive(true);
        _pathPlanner.ShowRoadMesh = false;
        _onDrawPath = true;

        //Disable caneva
        _thisCanevas.enabled = false;
        PathPlannerCanevas.SetActive(true);

        GetNewPanelReference();
        _panel.InitPathPanel(Intersection3, Intersection4);
    }

    public void StartEditExperiments()
    {
        _panel.StartEditExperiments();
    }

    public void Back()
    {
        _pathPlanner.ShowRoadMesh = true; //Show roads and disable planner objects
        _onDrawPath = false; // Reset draw bool

        //Disable caneva
        PathPlannerCanevas.SetActive(false);
        _thisCanevas.enabled = true;

        CarCamera.transform.parent.gameObject.SetActive(true);
        ExperimentExampleCamera.gameObject.SetActive(false);
        PathPlannerCamera.gameObject.SetActive(false);


        //On detruit tout les points StartPoint déjà present
        if (GameObject.Find("StartPointList") != null)
            Destroy(GameObject.Find("StartPointList"));

        //On detruit le tracer de la route
        if (VisualPlannedRoad != null)
        {
            Destroy(VisualPlannedRoad.gameObject);
            VisualPlannedRoad = null;
        }
        VisualCount = 0;
        AttachPoints.Clear();
        PlannedRoad.Clear();

    }

    public void Save(Text nom)
    {
        string path = string.Format("{0}\\RoadPath\\{1}.xml", Application.dataPath, nom.text);
        if (File.Exists(path))
            File.Delete(path);
        FileStream outFile = File.Create(path);
        XmlSerializer xmlserializer = new XmlSerializer(typeof(RoadPlannerXML));
        RoadPlannerXML planner = new RoadPlannerXML();
        planner.PlannedRoad = new string[PlannedRoad.Count];
        for(int i = 0; i < PlannedRoad.Count;i++)
        {
            if(i == 0)
                planner.StartPosition = Tools.ParseVector3ToString(PlannedRoad[i].wp.transform.position);
            else if (i == PlannedRoad.Count - 1)
                planner.EndPosition = Tools.ParseVector3ToString(PlannedRoad[i].wp.transform.position);

            planner.PlannedRoad[i] = Tools.ParseVector3ToString(PlannedRoad[i].wp.transform.position);
        }

        List<String> allow = new List<String>();
        foreach (var part in _pathPlanner.PartsScript.Where(p => p.State == Enums.PlannerState.Allowed))
        {
            allow.Add(Tools.ParseVector3ToString(part.transform.position));
        }
        planner.AllowedPoints = allow.ToArray();

        List<RoadPlannerAdjacentXML> adj = new List<RoadPlannerAdjacentXML>();
        foreach (var part in _pathPlanner.PartsScript.Where(p => p.State == Enums.PlannerState.BlockedEvent))
        {
            RoadPlannerAdjacentXML r = new RoadPlannerAdjacentXML();
            r.BlockedType = part.BlockedType;
            r.Position = Tools.ParseVector3ToString(part.transform.position);
            adj.Add(r);
        }
        planner.AdjacentPoints = adj.ToArray();

        xmlserializer.Serialize(outFile, planner);
        outFile.Close();


        if (File.Exists(string.Format("{0}\\RoadPath\\{1}_LIGHTS.xml", Application.dataPath, nom.text)))
            File.Delete(string.Format("{0}\\RoadPath\\{1}_LIGHTS.xml", Application.dataPath, nom.text));
        TrafficLightGroupController.SaveLightsToFile(string.Format("{0}\\RoadPath\\{1}_LIGHTS.xml", Application.dataPath, nom.text));

        ExperimentXML experimentsObject = new ExperimentXML();
        var _experimentParent = GameObject.Find("Experiments").transform;
        int size = _experimentParent.childCount;
        experimentsObject.Name = new string[size];
        experimentsObject.Position = new string[size];
        experimentsObject.Rotation = new string[size];
        experimentsObject.Scale = new string[size];
        experimentsObject.Size = size;
        experimentsObject.Type = new Enums.ExperimentType[size];
        //Experiments data
        Transform trTmp;
        for (int cnt = 0; cnt < size; cnt++)
        {
            trTmp = _experimentParent.GetChild(cnt);
            experimentsObject.Name[cnt] = trTmp.name;
            experimentsObject.Position[cnt] = Tools.ParseVector3ToString(trTmp.position);
            experimentsObject.Rotation[cnt] = Tools.ParseQuaternionToString(trTmp.rotation);
            experimentsObject.Scale[cnt] = Tools.ParseVector3ToString(trTmp.localScale);

            //Switching all experiments:
            if (trTmp.GetComponent<Experiment01>() != null)
                experimentsObject.Type[cnt] = trTmp.GetComponent<Experiment01>().IExpInstance.Type;
        }
        ExperimentController.SaveExperiments(string.Format("{0}\\RoadPath\\{1}_EXPERIMENTS.xml", Application.dataPath, nom.text), experimentsObject);

        panel.setDropbox();
        _pathPlanner.Load(path);

        ObjectManager.GetOptions().PlannedPathFile = path;
        ObjectManager.GetOptions().PlannedPathExperimentFile = path.Replace(".xml", "_EXPERIMENTS.xml");
        ObjectManager.GetOptions().Save();
        Debug.Log("Saved");
    }

    public void Load(Text nom)
    {
        string path = string.Format("{0}\\RoadPath\\{1}.xml", Application.dataPath, nom.text);
        panel.setNom(nom.text);
        FileStream file = new FileStream(path, FileMode.Open);
        XmlSerializer xmlserializer = new XmlSerializer(typeof(RoadPlannerXML));
        byte[] buffer = new byte[file.Length];
        file.Read(buffer, 0, (int)file.Length);
        MemoryStream stream = new MemoryStream(buffer);
        RoadPlannerXML temp = (RoadPlannerXML)xmlserializer.Deserialize(stream);
        NewRoad();
        List<StartPoint> spTemp = PosToStartPoint(temp);
        spTemp.ForEach(sp => DrawRoad(sp));
        file.Close();
        _pathPlanner.Load(path);

        ObjectManager.GetOptions().PlannedPathFile = path;
        ObjectManager.GetOptions().PlannedPathExperimentFile = path.Replace(".xml","_EXPERIMENTS.xml");
        ObjectManager.GetOptions().Save();
        Debug.Log("Loaded");
    }

    private List<StartPoint> PosToStartPoint(RoadPlannerXML temp)
    {
        List<StartPoint> list = new List<StartPoint>();
        foreach (String s in temp.PlannedRoad)
        {
            foreach (GameObject go in AttachPoints)
            {
                StartPoint sp = go.GetComponent<StartPoint>();
                if (Tools.ParseVector3ToString(sp.wp.transform.position) == s)
                {
                    list.Add(sp);
                    break;
                }
            }
        }
        return list;
    }

    public void NewRoad()
    {
        //On detruit tout les points StartPoint déjà present
        if (GameObject.Find("StartPointList")!= null)
            Destroy(GameObject.Find("StartPointList"));

        //On detruit le tracer de la route
        if (VisualPlannedRoad != null)
        {
            Destroy(VisualPlannedRoad.gameObject);
            VisualPlannedRoad = null;
        }
        VisualCount = 0;
        AttachPoints.Clear();
        PlannedRoad.Clear();

        //On crée de nouveaux StartPoint pour chaque waypoint
        var waypoints = new List<Waypoint>(GameObject.Find("Roads").GetComponentsInChildren<Waypoint>());
        var startList = new GameObject("StartPointList");
        foreach (var wp in waypoints)
        {
            var pos = wp.transform.position;
            var go = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            go.transform.position = new Vector3(pos.x, pos.y + 20, pos.z);
            go.transform.localScale = new Vector3(20, 20, 20);
            var st = go.AddComponent<StartPoint>();
            st.path = this;
            st.wp = wp;
            AttachPoints.Add(go);
            go.SetActive(wp.State == Enums.WaypointState.Begin);
            go.transform.SetParent(startList.transform);
        }
    }

    public void Apply()
    {
        _panel.Apply();
    }

    public void Cancel()
    {
        _panel.Cancel();
    }

    public void AnnulerDerniereRoute()
    {
        //On detruit l'affichage de la route
        if (VisualPlannedRoad != null)
            if (VisualPlannedRoad.gameObject != null)
                Destroy(VisualPlannedRoad.gameObject);
        VisualPlannedRoad = null;
        VisualCount = 0;
        //On supprime le dernier point
        PlannedRoad.RemoveAt(PlannedRoad.Count - 1);
        //On copie le dernier point
        var tempRoad = new List<StartPoint>(PlannedRoad);
        PlannedRoad.Clear();
        //On redessine la route
        foreach (var roadPoint in tempRoad)
        {
            DrawRoad(roadPoint);
        }
    }

    public void DrawRoad(StartPoint startPoint)
    {
        //On crée le visuel de la route
        if (VisualPlannedRoad == null)
        {
            var GoPlannedRoad = new GameObject("VisualPlannedRoad");
            VisualPlannedRoad = GoPlannedRoad.AddComponent<LineRenderer>();
            VisualPlannedRoad.SetWidth(10, 10);
            VisualPlannedRoad.SetVertexCount(1);
            VisualCount = 0;
            VisualPlannedRoad.SetPosition(0, new Vector3(startPoint.transform.position.x, startPoint.transform.position.y + 20, startPoint.transform.position.z));
        }
        else
        {
            //On dessine la route
            //On recupere les waypoint de la route qu'on parcours
            var waypointsList = PlannedRoad[PlannedRoad.Count - 1].wp.transform.parent.gameObject.GetComponentsInChildren<Waypoint>();
            drawLine(waypointsList, startPoint);
            
        }

        PlannedRoad.Add(startPoint); //On ajoute le StartPoint au tableau de route

        foreach (var at in AttachPoints.Where(at => at.activeSelf == true))
        {
            at.SetActive(false);
        }

        //On itére sur les points de la nouvelle route pour trouver toutes les intersections
        startPoint.wp.GetComponentInParent<Road>().GetComponentsInChildren<Waypoint>().ToList().ForEach(w =>
        {
            if (w.IntersectionPoints.Length > 0)
            {
                w.IntersectionPoints.ToList().ForEach(wpIntersection =>
                {
                    //On actives les StartPoints lié à ses waypoints
                    if (wpIntersection != null)
                    {
                        foreach (var at in AttachPoints)
                        {
                            var atWp = at.GetComponent<StartPoint>().wp.gameObject;
                            if (atWp == wpIntersection)
                                at.SetActive(true);
                        }
                    }
                });
            }
        });
        //On desactive le point d'entrer de notre route pour eviter de le re-selectioner
        startPoint.gameObject.SetActive(false);
    }
    
    public void drawLine(Waypoint[] waypointsList, StartPoint startPoint, bool start = false)
    {
            foreach (var waypoint in waypointsList)
            {
                //On cherche d'abord notre point de départ
                if (!start)
                {
                    var st = PlannedRoad[PlannedRoad.Count - 1];
                    if (waypoint.name == st.wp.name)
                        start = true;
                }
                //On commence à déssiner la route une fois la route trouver
                if (start)
                {
                    VisualCount++;
                    VisualPlannedRoad.SetVertexCount(VisualCount + 1);
                    VisualPlannedRoad.SetPosition(VisualCount, new Vector3(waypoint.transform.position.x, waypoint.transform.position.y + 20, waypoint.transform.position.z));
                    if (waypoint.IntersectionPoints.Contains(startPoint.wp.gameObject))
                        break; //On s'arrete lorsqu'on trouve notre sortie de route
                    if (waypoint.State == Enums.WaypointState.End)
                        if (waypoint.IntersectionPoints.Length > 0)
                        {
                            if(waypoint.IntersectionPoints[0] != null)
                                drawLine(waypoint.IntersectionPoints[0].transform.parent.gameObject.GetComponentsInChildren<Waypoint>(), startPoint,true);
                        }
                }
            }
    }


    public void UpdateLightValues()
    {
        _panel.UpdateLightPanelValues();
    }
}


