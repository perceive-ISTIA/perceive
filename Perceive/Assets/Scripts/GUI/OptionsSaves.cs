﻿using UnityEngine.UI;
using UnityEngine;
using System.Collections;

public class OptionsSaves : MonoBehaviour
{
    private SavesPanel _panel;
    public SavesPanel panel
    {
        get
        {
            return _panel;
        }
    }

    public SavesPanel GetNewPanelReference()
    {
        _panel = new SavesPanel(transform.GetComponent<RectTransform>(), transform);
        return _panel;
    }

    public void Apply()
    {
        _panel.Apply();
    }

    public void Cancel()
    {
        _panel.Cancel();
    }

    public void Hide()
    {
        _panel.Hide();
    }

    public void Show()
    {
        _panel.Show();
    }
}

public class SavesPanel : IPanel
{
    private InputField _savesPath;
    private InputField _savesInterval;
    public SavesPanel(RectTransform rect, Transform tr)
    {
        _rect = rect;
        _tr = tr;

        _savesPath = _tr.FindChild("Controls").FindChild("InputField").GetComponent<InputField>();
        _savesInterval = _tr.FindChild("Controls").FindChild("IntervalInputField").GetComponent<InputField>();

    }

    private RectTransform _rect;
    public RectTransform rectTransform
    {
        get
        {
            return _rect;
        }
        set
        {
            _rect = value;
        }
    }

    private Transform _tr;
    public Transform transform
    {
        get
        {
            return _tr;
        }
        set
        {
            _tr = value;
        }
    }

    public void Init()
    {
        _savesPath.text = ObjectManager.GetOptions().SavePath;
        _savesInterval.text = ObjectManager.GetOptions().DataInterval.ToString();
    }

    public void Hide()
    {
        _tr.gameObject.SetActive(false);
    }

    public void Show()
    {
        _tr.gameObject.SetActive(true);
    }

    public void Apply()
    {
        //Save
        ObjectManager.GetOptions().SavePath = _savesPath.text;
        ObjectManager.GetOptions().DataInterval = float.Parse(_savesInterval.text);
        ObjectManager.GetOptions().Save();

        Hide();
    }

    public void Cancel()
    {
        Hide();
    }
}