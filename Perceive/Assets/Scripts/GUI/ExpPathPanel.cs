﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.IO;
using System;

public class ExpPathPanel : IPanel
{
    private Transform _panelRTr;
    private Transform _panelExperiment;

    private PathPlanner _planner;

    private Toggle _toggleLights;
    private Toggle _togglePlanner;
    private Toggle _toggleRail;

    private Dropdown _cbxBlockedType;

    //Intersection panel
    private Sprite _intersection3;
    private Sprite _intersection4;
    private InputField _inputDelay;
    private InputField _inputRToG;
    private InputField _inputYToR;
    private Combobox _comboGInitial;
    private Image _imageInter;
    private GameObject _panelLights;
    private GameObject _panelPartState;

    //Experiment panel
    private Button _buttonEdit;

    #region Properties
    private RectTransform _rect;
    public RectTransform rectTransform
    {
        get
        {
            return _rect;
        }
        set
        {
            _rect = value;
        }
    }

    private Transform _tr;
    public Transform transform
    {
        get
        {
            return _tr;
        }
        set
        {
            _tr = value;
        }
    }
    #endregion

    public ExpPathPanel(RectTransform rect, Transform tr, Transform panelRTr, Transform panelLightsTr, Transform panelPartState, Transform panelExperiment, PathPlanner planner)
    {
        _tr = tr;
        _rect = rect;
        _panelRTr = panelRTr;
        _panelLights = panelLightsTr.gameObject;
        _panelPartState = panelPartState.gameObject;
        _panelExperiment = panelExperiment;
        _planner = planner;


        _toggleLights = _tr.FindChild("Controls").FindChild("ToggleLights").GetComponent<Toggle>();
        _togglePlanner = _tr.FindChild("Controls").FindChild("TogglePlanner").GetComponent<Toggle>();
        _toggleRail = _tr.FindChild("Controls").FindChild("ToggleRail").GetComponent<Toggle>();

        _cbxBlockedType = panelPartState.FindChild("Dropdown").GetComponent<Dropdown>();
        
        //Intersection panel components:
        _inputDelay = panelLightsTr.FindChild("InputDelay").GetComponent<InputField>();
        _inputRToG = panelLightsTr.FindChild("InputRToG").GetComponent<InputField>();
        _inputYToR = panelLightsTr.FindChild("InputYToR").GetComponent<InputField>();
        _imageInter = panelLightsTr.FindChild("Image").GetComponent<Image>();
        _comboGInitial = panelLightsTr.FindChild("Combobox").GetComponent<Combobox>();

        //Experiment panel components:
        _buttonEdit = panelExperiment.FindChild("ButtonEdit").GetComponent<Button>();
    
    }

    public void InitPathPanel(Sprite Inter3, Sprite Inter4)
    {
        _planner.NewSelectionEvent += new NewSelection(this._planner_NewSelectionEvent);

        string[] blockedType = Enum.GetNames(typeof(Enums.PlannerBlockedType));
        foreach (var str in blockedType)
            _cbxBlockedType.options.Add(new UnityEngine.UI.Dropdown.OptionData(str));
        _cbxBlockedType.onValueChanged.AddListener(this._cbxBlockedType_ValueChanged);

        //Intersection Panel:
        _intersection3 = Inter3;
        _intersection4 = Inter4;
    }


    public void Init()
    {
        _toggleLights.isOn = ObjectManager.GetOptions().isLightsRandom;
        _toggleRail.isOn = ObjectManager.GetOptions().isRail;
        _togglePlanner.isOn = ObjectManager.GetOptions().isPlanner;
    }

    public void Hide()
    {
        _tr.gameObject.SetActive(false);
    }

    public void Show()
    {
        _tr.gameObject.SetActive(true);
        setDropbox();
    }

    internal void setDropbox()
    {
        var files = Directory.GetFiles(string.Format("{0}/RoadPath", Application.dataPath));
        Dropdown gText = _panelRTr.FindChild("Liste Route").GetComponent<Dropdown>();
        gText.options.Clear();
        if (files.Length != 0)
            foreach (string st in files)
            {
                string nom = st;
                try
                {
                    nom = st.Substring(0, st.LastIndexOf('.'));
                }
                catch { }
                try
                {
                    nom = nom.Substring(nom.LastIndexOf('\\') + 1);
                }
                catch { }
                if (!nom.Contains("_EXPERIMENTS") && !nom.Contains("_LIGHTS") && !nom.Contains("xml"))
                    gText.options.Add(new UnityEngine.UI.Dropdown.OptionData(nom));
            }
        try
        {
            gText.transform.FindChild("Label").GetComponent<Text>().text = gText.options[gText.value].text;
        }
        catch
        {
            gText.transform.FindChild("Label").GetComponent<Text>().text = "";
        }
    }

    public void Apply()
    {
        ObjectManager.GetOptions().isLightsRandom = _toggleLights.isOn;
        ObjectManager.GetOptions().isPlanner = _togglePlanner.isOn;
        ObjectManager.GetOptions().isRail = _toggleRail.isOn;
        ObjectManager.GetOptions().Save();

        Hide();
    }

    public void ToggleRailStatus()
    {
        if (_togglePlanner.isOn)
        {
            _toggleRail.gameObject.SetActive(true);
        }
        else
        {
            _toggleRail.gameObject.SetActive(false);
        }
    }

    public void Cancel()
    {
        Hide();
    }

    public void UpdateLightPanelValues()
    {
        _planner.UpdateLightController(float.Parse(_inputDelay.text), float.Parse(_inputRToG.text), float.Parse(_inputYToR.text), _comboGInitial.SelectedItem);
    }

    internal void setNom(string p)
    {
        _panelRTr.FindChild("InfoPanel").FindChild("Nom Route").GetComponent<InputField>().text = p;
    }

    public void UpdateInfos(PlannerPartInfo infos)
    {
        /*if (infos.State == Enums.PlannerState.BlockedEvent)
            ComboboxTypeState(true);
        else
            ComboboxTypeState(false);
        _infos = infos;
        _txtName.text = string.Format("Nom: {0}", infos.Name);
        _txtRoad.text = string.Format("Route: {0}", infos.Road);
        _txtType.text = string.Format("Type: {0}", infos.Type);
        _txtState.text = string.Format("Etat: {0}", infos.State);
        _cbxBlockedType.SelectedItem = infos.BlockedType.ToString();

        string str = "";
        if (infos.Adjactents.Length > 0)
        {
            for (int cnt = 0; cnt < infos.Adjactents.Length; cnt++)
            {
                if (cnt >= 0)
                    str = string.Format("{0}/{1}", str, infos.Adjactents[cnt].ToString());
                else
                    str = infos.Adjactents[cnt].ToString();
            }
        }
        else
            str = "Aucunes";
        _txtAdjacent.text = string.Format("Routes annexes: {0}", str);*/

        //IntersectionInfos:
        if (infos.Instance.Type == Enums.PlannerPart.Intersection)
        {
            TrafficLightGroupController light = infos.Instance.LightController;
            if (light != null)
            {
                string[] cbxContent = new string[2];
                cbxContent[0] = "01-03";
                switch (light.Type)
                {
                    case Enums.IntersectionLightType.IntersectionT:
                        _imageInter.sprite = _intersection3;
                        cbxContent[1] = "02";
                        break;
                    case Enums.IntersectionLightType.Carrefour:
                        _imageInter.sprite = _intersection4;
                        cbxContent[1] = "02-04";
                        break;
                }
                _inputDelay.text = light.Delay.ToString();
                _inputRToG.text = light.RedToGreenDelay.ToString();
                _inputYToR.text = light.YellowDelay.ToString();
                _panelLights.SetActive(true);
                _panelPartState.SetActive(false);
                _comboGInitial.InitCombobox(cbxContent);
                _comboGInitial.SelectedItem = light.DefaultGreen;
            }
            else
                _panelLights.SetActive(false);
        }
        else
        {
            /*if (infos.State != Enums.PlannerState.Allowed)
            {
                _panelPartState.SetActive(true);
                //_toggleStart.isOn = infos.isStart;
                //_toggleEnd.isOn = infos.isEnd;
                _cbxBlockedType.value = (int) infos.BlockedType;
            }
            else
                _panelPartState.SetActive(false);*/
            _panelLights.SetActive(false);
        }
    }

    private void _planner_NewSelectionEvent(object sender, bool isRight, PlannerPartInfo infos)
    {
        if (isRight)
        {
            UpdateInfos(infos);
        }
    }

    private void _cbxBlockedType_ValueChanged(int id)
    {
        _planner.UpdateBlockedType((Enums.PlannerBlockedType)id);
    }

    public void StartEditExperiments()
    {
        _planner.OnDrawExperiments = !_planner.OnDrawExperiments;
        if (_planner.OnDrawExperiments)
        {
            ColorBlock block = _buttonEdit.colors;
            block.normalColor = Color.green;
            block.highlightedColor = new Color32(255, 113, 133, 255);
            block.pressedColor = Color.red;
            _buttonEdit.colors = block;
        }
        else
        {
            ColorBlock block = _buttonEdit.colors;
            block.normalColor = Color.red;
            block.highlightedColor = new Color32(161, 251, 161, 255);
            block.pressedColor = new Color32(103, 225, 103, 255);
            _buttonEdit.colors = block;
        }
    }
}
