﻿using UnityEngine.UI;
using UnityEngine;
using System.Collections;

public class OptionsPauseMenu : MonoBehaviour
{
    private Button[] _panelButtons;
    private IPanel[] _panels;

    public void Init()
    {
        _panelButtons = new Button[1] { transform.FindChild("Panel_tabs").FindChild("Graphics").GetComponent<Button>()};
        _panels = new IPanel[1] { transform.FindChild("Panel_graphics").GetComponent<OptionsGraphicsPauseMenu>().GetNewPanelReference() };
    }


    public void ChangeOptionTab(int tabIndex)
    {
        SetSelectedButton(tabIndex);
        ShowPanel(tabIndex);
    }

    private void SetSelectedButton(int index)
    {
        ColorBlock colors;
        for (int cnt = 0; cnt < _panelButtons.Length; cnt++)
        {
            colors = _panelButtons[cnt].colors;
            if (cnt != index)
                colors.normalColor = new Color32(255, 251, 255, 255);
            else
                colors.normalColor = new Color32(103, 225, 103, 255);
            _panelButtons[cnt].colors = colors;
        }
    }

    private void ShowPanel(int index)
    {
        for (int cnt = 0; cnt < _panels.Length; cnt++)
        {
            if (cnt != index)
                _panels[cnt].Hide();
            else
            {
                _panels[cnt].Show();
                _panels[cnt].Init();
            }
        }
    }
}
