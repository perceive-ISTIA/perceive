﻿using UnityEngine.UI;
using UnityEngine;
using System.Collections;

public class OptionsCar : MonoBehaviour
{
    private CarPanel _panel;
    public CarPanel panel
    {
        get
        {
            return _panel;
        }
    }

    public CarPanel GetNewPanelReference()
    {
        _panel = new CarPanel(transform.GetComponent<RectTransform>(), transform);
        return _panel;
    }

    public void Apply()
    {
        _panel.Apply();
    }

    public void Cancel()
    {
        _panel.Cancel();
    }

    public void Hide()
    {
        _panel.Hide();
    }

    public void Show()
    {
        _panel.Show();
    }
}

public class CarPanel : IPanel
{
    private InputField _inputSpeed;
    private InputField _inputAcceleration;
    private InputField _inputDeceleration;
    public CarPanel(RectTransform rect, Transform tr)
    {
        _rect = rect;
        _tr = tr;

        _inputSpeed = _tr.FindChild("Controls").FindChild("InputSpeed").GetComponent<InputField>();
        _inputAcceleration = _tr.FindChild("Controls").FindChild("InputAcceleration").GetComponent<InputField>();
        _inputDeceleration = _tr.FindChild("Controls").FindChild("InputDeceleration").GetComponent<InputField>();
    }

    private RectTransform _rect;
    public RectTransform rectTransform
    {
        get
        {
            return _rect;
        }
        set
        {
            _rect = value;
        }
    }

    private Transform _tr;
    public Transform transform
    {
        get
        {
            return _tr;
        }
        set
        {
            _tr = value;
        }
    }

    public void Init()
    {
        _inputSpeed.text = ObjectManager.GetOptions().PlayerMaxSpeed.ToString();
        _inputAcceleration.text = ObjectManager.GetOptions().PlayerAcceleration.ToString();
        _inputDeceleration.text = ObjectManager.GetOptions().PlayerDeceleration.ToString();
    }

    public void Hide()
    {
        _tr.gameObject.SetActive(false);
    }

    public void Show()
    {
        _tr.gameObject.SetActive(true);
    }

    public void Apply()
    {
        //Save
        ObjectManager.GetOptions().PlayerMaxSpeed = float.Parse(_inputSpeed.text);
        ObjectManager.GetOptions().PlayerAcceleration = float.Parse(_inputAcceleration.text);
        ObjectManager.GetOptions().PlayerDeceleration = float.Parse(_inputDeceleration.text);
        ObjectManager.GetOptions().Save();

        Hide();
    }

    public void Cancel()
    {
        Hide();
    }
}