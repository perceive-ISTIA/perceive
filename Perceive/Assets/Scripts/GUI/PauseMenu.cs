﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PauseMenu : MonoBehaviour
{
    public BehavioralData Data;
    public OptionsPauseMenu PanelOptions;
    public FinishedMenu FinishedMenu;

    public bool OnFinished { get; set; }

    private RectTransform _resume;
    private RectTransform _options;
    private RectTransform _finish;
    private RectTransform _exit;
    private RectTransform[] _btns;
    private Button[] _btnsButton;
    private Image[] _btnsImage;

    private AutoMove _autoMove;
    public AutoMove AutoMoveScript
    {
        get
        {
            return _autoMove;
        }
        set
        {
            _autoMove = value;
        }
    }

    private Vector2 _highlightedSize;
    private Vector2 _initialSize;
    private Transform _thisTransform;

    private float _lastSimulationTime;
    public float LastSimulationTime
    {
        get
        {
            return _lastSimulationTime;
        }
    }

    private string _lastSaveName;
    public string LastSaveName
    {
        get
        {
            return _lastSaveName;
        }
    }
    void Awake()
    {
        _thisTransform = transform;
        OnFinished = false;
        _highlightedSize = new Vector2(983.5f, 58);

        _resume = _thisTransform.FindChild("Panel").FindChild("BTN_Resume").GetComponent<RectTransform>();

        _initialSize = _resume.sizeDelta;
        _options = _thisTransform.FindChild("Panel").FindChild("BTN_Options").GetComponent<RectTransform>();
        _finish = _thisTransform.FindChild("Panel").FindChild("BTN_Finish").GetComponent<RectTransform>();
        _exit = _thisTransform.FindChild("Panel").FindChild("BTN_Exit").GetComponent<RectTransform>();

        _btns = new RectTransform[4] { _resume, _options, _finish, _exit };
        _btnsButton = new Button[4] { _resume.GetComponent<Button>(), _options.GetComponent<Button>(), _finish.GetComponent<Button>(), _exit.GetComponent<Button>() };
        _btnsImage = new Image[4] { _resume.GetComponent<Image>(), _options.GetComponent<Image>(), _finish.GetComponent<Image>(), _exit.GetComponent<Image>() };
    }

    void Start()
    {
        PanelOptions.Init();
    }

    public void Show()
    {
        _thisTransform.gameObject.SetActive(true);
    }
    public void Hide()
    {
        _thisTransform.gameObject.SetActive(false);
        _autoMove.UnFreezeMovement();
    }


    public void OnBTNOptions()
    {
        if (!PanelOptions.gameObject.activeInHierarchy)
        {
            PanelOptions.gameObject.SetActive(true);
            FreezeFocusOnButton(2);

        }
        else
        {
            PanelOptions.gameObject.SetActive(false);

            ReleaseFocusOnButton(2);
        }
    }

    public void OnBTNResume()
    {
        Hide();
    }

    public void OnBTNExit()
    {
        Application.Quit();
    }

    public void OnBTNFinish()
    {
        _lastSimulationTime = Data.StopRecording();
        _lastSaveName = Data.LastSaveName;
        gameObject.SetActive(false);
        OnFinished = true;
    }

    public void FreezeFocusOnButton(int id)
    {
        for (int cnt = 0; cnt < _btns.Length; cnt++)
        {
            if (cnt == id)
            {
                _btnsButton[cnt].transition = Selectable.Transition.None;
                _btnsImage[cnt].color = new Color32(161, 251, 161, 255);

                _btns[cnt].sizeDelta = _highlightedSize;
            }
            else
            {
                _btnsButton[cnt].transition = Selectable.Transition.Animation;

                _btnsImage[cnt].color = new Color32(255, 255, 255, 255);
                _btns[cnt].sizeDelta = _initialSize;
            }
        }
    }
    public void ReleaseFocusOnButton(int id)
    {
        _btns[id].GetComponent<Button>().transition = Selectable.Transition.Animation;
        _btns[id].GetComponent<Image>().color = new Color32(255, 255, 255, 255);

        _btns[id].sizeDelta = _initialSize;

    }
}
