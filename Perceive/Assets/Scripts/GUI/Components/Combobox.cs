﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.EventSystems;

public delegate void ComboboxEventHandler(object sender, EventArgs e);
public class Combobox : MonoBehaviour
{
    public event ComboboxEventHandler ValueChanged;
    protected virtual void OnValueChanged(EventArgs e)
    {
        if(ValueChanged != null)
            ValueChanged(this, e);
    }
    
    private RectTransform _dropList;
    private RectTransform _labeledDropBtn;
    private RectTransform _dropArrow;
    private Transform _controls;
    private Transform _topControl;
    private Transform _thisTransform;
    private RectTransform _thisTransformRect;

    private string[] _items;
    private bool _expand = false;
    private bool _mouseIn = false;
    public string[] Items
    {
        get
        {
            return _items;
        }
        set
        {
            _items = new string[value.Length];
            value.CopyTo(_items, 0);
        }
    }
    private int _selectedItemIndex = 0;
    public int SelectedItemIndex
    {
        get
        {
            return _selectedItemIndex;
        }
        set
        {
            if (_items != null && value >= 0 && value < _items.Length)
            {
                _selectedItemIndex = value;
                _labeledDropBtn.transform.FindChild("Text").GetComponent<Text>().text = _items[value]; //Set the selected index
            }
            else
            {
                if (_items == null)
                    throw new Exception("The combobox does not have Items");
                if (value < 0)
                    throw new Exception("Out of range exception, the value is < 0");
                if (value >= _items.Length)
                    throw new Exception("Out of range exception, the value is over the length");
            }
        }
    }

    public string SelectedItem
    {
        get
        {
            return _items[_selectedItemIndex];
        }
        set
        {
            for(int i = 0; i < _items.Length; i ++)
            {
                if(_items[i] == value)
                {
                    SelectedItemIndex = i;
                    return;
                }
            }
        }
    }

    private float _topPadding = 5.0f;
    private float _bottomPadding = 5.0f;
    private float _leftPadding = 5.0f;
    private float _interPadding = 2.0f;

    private float _itemHeight = 30.0f;


    private bool _dropclick = false;

    void Awake()
    {
        _thisTransform = transform;
        _thisTransformRect = _thisTransform.GetComponent<RectTransform>();
        _dropList = _thisTransform.FindChild("drop_list").GetComponent<RectTransform>();
        _labeledDropBtn = _thisTransform.FindChild("drop").GetComponent<RectTransform>();
        _dropArrow = _thisTransform.FindChild("drop_arrow").GetComponent<RectTransform>();
        _controls = _thisTransform.parent;

        int count = 0;
        foreach(Transform tr in _controls)
        {
            if (tr.GetComponent<Combobox>() != null)
                count++;
        }
        if (count > 1)
            _topControl = transform.parent.parent.FindChild("TopControl");
        else
            _topControl = null;
    }

    void Update()
    {
        if (((Input.GetMouseButtonDown(0) && !_mouseIn) || Input.GetKeyDown(KeyCode.Escape)) && _expand )
            OnDropClick();
    }

    /// <summary>
    /// Initialize the combobox by creating the buttons and events
    /// </summary>
    /// <param name="items">Array of items</param>
    /// <param name="defaultIndex">Item selected by default</param>
    public void InitCombobox(string[] items, int defaultIndex = 0)
    {
        //Cleaning the existing buttons:
        for (int cnt = 0; cnt < _dropList.childCount; cnt++)
        {
            GameObject.Destroy(_dropList.GetChild(cnt).gameObject);
        }

        Items = new string[items.Length];
        _items = new string[items.Length];
        items.CopyTo(_items, 0);
        if (items.Length > 0)
            _labeledDropBtn.transform.FindChild("Text").GetComponent<Text>().text = _items[defaultIndex]; //Set the selected index
        else
            _labeledDropBtn.transform.FindChild("Text").GetComponent<Text>().text = "";


        //Set the item height scalled on the labeled btn
        _itemHeight = _labeledDropBtn.GetHeight();

        //Draw the list of items
        float size = _topPadding + _bottomPadding + (float)_items.Length * (_itemHeight + _interPadding) - _interPadding;
        _dropList.localPosition = new Vector3(-16.5f, -(size / 2f + _thisTransformRect.GetHeight() / 2f), 0);
        RectTransformExtensions.SetHeight(_dropList, size);

        //Create the buttons:
        int id = 0;
        foreach (string str in _items)
        {
            GameObject btn = (GameObject)GameObject.Instantiate(Resources.Load("GUI/GenericButton"));
            btn.transform.SetParent(_dropList.transform, false);
            btn.name = string.Format("btn_{0}", id);
            btn.transform.FindChild("Text").GetComponent<Text>().text = str; //Set the selected index
            btn.GetComponent<RectTransform>().anchoredPosition = new Vector3(-_leftPadding, (size/ 2f - 15 - _topPadding) - (_itemHeight + _interPadding) * id, 0);
            RectTransformExtensions.SetHeight(btn.GetComponent<RectTransform>(), _itemHeight);
            btn.GetComponent<Button>().onClick.AddListener(() => OnBtnListClick(btn));
            id++;
        }

        #region Events
        //Mouse event:
        EventTrigger trigger = _dropList.gameObject.AddComponent<EventTrigger>();
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.PointerEnter;
        entry.callback.AddListener((eventData) => { OnMouseEnter(); });
        trigger.triggers = new System.Collections.Generic.List<EventTrigger.Entry>();
        trigger.triggers.Add(entry);
        entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.PointerExit;
        entry.callback.AddListener((eventData) => { OnMouseExit(); });
        trigger.triggers.Add(entry);

        trigger = _dropArrow.gameObject.AddComponent<EventTrigger>();
        entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.PointerEnter;
        entry.callback.AddListener((eventData) => { OnMouseEnter(); });
        trigger.triggers = new System.Collections.Generic.List<EventTrigger.Entry>();
        trigger.triggers.Add(entry);
        entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.PointerExit;
        entry.callback.AddListener((eventData) => { OnMouseExit(); });
        trigger.triggers.Add(entry);

        trigger = _labeledDropBtn.gameObject.AddComponent<EventTrigger>();
        entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.PointerEnter;
        entry.callback.AddListener((eventData) => { OnMouseEnter(); });
        trigger.triggers = new System.Collections.Generic.List<EventTrigger.Entry>();
        trigger.triggers.Add(entry);
        entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.PointerExit;
        entry.callback.AddListener((eventData) => { OnMouseExit(); });
        trigger.triggers.Add(entry);
        #endregion
    }

    //EVENTS
    public void OnDropClick()
    {
        _dropclick = !_dropclick;
        if (_dropclick)
        {
            //Set onTop
            SetOnTop(true);
            _expand = true;
            //Rotate the drop arrow:
            _dropArrow.transform.Rotate(Vector3.forward, 180);
            _dropList.gameObject.SetActive(true);
        }
        else
        {
            SetOnTop(false);
            _expand = false;
            _dropArrow.transform.Rotate(Vector3.forward, -180);
            _dropList.gameObject.SetActive(false);
        }
    }

    public void OnBtnListClick(GameObject e)
    {
        //Validate the click and update the text
        _selectedItemIndex = int.Parse(e.name.Replace("btn_", ""));
        OnValueChanged(new EventArgs());
        _labeledDropBtn.transform.FindChild("Text").GetComponent<Text>().text = _items[_selectedItemIndex]; //Set the selected index
        _dropclick = false;
        _dropList.gameObject.SetActive(false);
    }

    public void OnMouseEnter()
    {
        _mouseIn = true;
    }

    public void OnMouseExit()
    {
        _mouseIn = false;
    }



    public void SetOnTop(bool state)
    {
        if (_topControl != null)
        {
            if (state)
            {
                transform.SetParent(_topControl, false);
            }
            else
            {
                transform.SetParent(_controls, false);
            }
        }
    }

    public string GetItemAt(int index)
    {
        if (_items != null && index >= 0 && index < _items.Length)
            return _items[index];
        else
            return null;
    }

    /// <summary>
    /// Add a new item at the end of the existing items
    /// </summary>
    /// <param name="str">Item text</param>
    public void AddItem(string str)
    {
        string[] tmp = new string[_items.Length];
        _items.CopyTo(tmp, 0);

        int newLength = tmp.Length + 1;

        Items = new string[newLength];
        _items = new string[newLength];
        for(int cnt = 0 ; cnt < tmp.Length; cnt ++)
        {
            _items[cnt] = tmp[cnt];
        }
        _items[newLength - 1] = str;

        //Resize the drop panel:
        float size = 2 * _topPadding + _items.Length * (_itemHeight + _interPadding) - _interPadding;
        _dropList.localPosition = new Vector3(-16.5f, -(size / 2f + 20), 0);
        RectTransformExtensions.SetHeight(_dropList, size);

        //Add button:
        GameObject btn = (GameObject)GameObject.Instantiate(Resources.Load("GUI/GenericButton"));
        btn.transform.SetParent(_dropList.transform, false);
        btn.name = string.Format("btn_{0}", _items.Length - 1);
        btn.transform.FindChild("Text").GetComponent<Text>().text = str; //Set the selected index
        btn.GetComponent<RectTransform>().anchoredPosition = new Vector3(-_leftPadding, (size / 2f - 15 - _topPadding) - (30 + _interPadding) * (newLength - 1), 0);
        RectTransformExtensions.SetHeight(btn.GetComponent<RectTransform>(), 25);
        btn.GetComponent<Button>().onClick.AddListener(() => OnBtnListClick(btn));
    }

    public void Refresh()
    {
        for(int cnt = 0; cnt < _dropList.childCount; cnt++)
        {
            Destroy(_dropList.GetChild(cnt).gameObject);
        }

        _labeledDropBtn.transform.FindChild("Text").GetComponent<Text>().text = _items[_selectedItemIndex]; //Set the selected index

        //Draw the list of items
        float size = 2 * _topPadding + _items.Length * (_itemHeight + _interPadding) - _interPadding;
        _dropList.localPosition = new Vector3(-16.5f, -(size / 2f + 20), 0);
        RectTransformExtensions.SetHeight(_dropList, size);

        //Create the buttons:
        int id = 0;
        foreach (string str in _items)
        {
            GameObject btn = (GameObject)GameObject.Instantiate(Resources.Load("GUI/GenericButton"));
            btn.transform.SetParent(_dropList.transform, false);
            btn.name = string.Format("btn_{0}", id);
            btn.transform.FindChild("Text").GetComponent<Text>().text = str; //Set the selected index
            btn.GetComponent<RectTransform>().anchoredPosition = new Vector3(-_leftPadding, (size / 2f - 15 - _topPadding) - (30 + _interPadding) * id, 0);
            RectTransformExtensions.SetHeight(btn.GetComponent<RectTransform>(), 25);
            btn.GetComponent<Button>().onClick.AddListener(() => OnBtnListClick(btn));
            id++;
        }
    }

    public void Show()
    {
        _thisTransform.gameObject.SetActive(true);
    }

    public void Hide()
    {
        _thisTransform.gameObject.SetActive(false);
    }

}


public static class RectTransformExtensions
{
    public static void SetDefaultScale(this RectTransform trans)
    {
        trans.localScale = new Vector3(1, 1, 1);
    }
    public static void SetPivotAndAnchors(this RectTransform trans, Vector2 aVec)
    {
        trans.pivot = aVec;
        trans.anchorMin = aVec;
        trans.anchorMax = aVec;
    }

    public static Vector2 GetSize(this RectTransform trans)
    {
        return trans.rect.size;
    }
    public static float GetWidth(this RectTransform trans)
    {
        return trans.rect.width;
    }
    public static float GetHeight(this RectTransform trans)
    {
        return trans.rect.height;
    }

    public static void SetPositionOfPivot(this RectTransform trans, Vector2 newPos)
    {
        trans.localPosition = new Vector3(newPos.x, newPos.y, trans.localPosition.z);
    }

    public static void SetLeftBottomPosition(this RectTransform trans, Vector2 newPos)
    {
        trans.localPosition = new Vector3(newPos.x + (trans.pivot.x * trans.rect.width), newPos.y + (trans.pivot.y * trans.rect.height), trans.localPosition.z);
    }
    public static void SetLeftTopPosition(this RectTransform trans, Vector2 newPos)
    {
        trans.localPosition = new Vector3(newPos.x + (trans.pivot.x * trans.rect.width), newPos.y - ((1f - trans.pivot.y) * trans.rect.height), trans.localPosition.z);
    }
    public static void SetRightBottomPosition(this RectTransform trans, Vector2 newPos)
    {
        trans.localPosition = new Vector3(newPos.x - ((1f - trans.pivot.x) * trans.rect.width), newPos.y + (trans.pivot.y * trans.rect.height), trans.localPosition.z);
    }
    public static void SetRightTopPosition(this RectTransform trans, Vector2 newPos)
    {
        trans.localPosition = new Vector3(newPos.x - ((1f - trans.pivot.x) * trans.rect.width), newPos.y - ((1f - trans.pivot.y) * trans.rect.height), trans.localPosition.z);
    }

    public static void SetSize(this RectTransform trans, Vector2 newSize)
    {
        Vector2 oldSize = trans.rect.size;
        Vector2 deltaSize = newSize - oldSize;
        trans.offsetMin = trans.offsetMin - new Vector2(deltaSize.x * trans.pivot.x, deltaSize.y * trans.pivot.y);
        trans.offsetMax = trans.offsetMax + new Vector2(deltaSize.x * (1f - trans.pivot.x), deltaSize.y * (1f - trans.pivot.y));
    }
    public static void SetWidth(this RectTransform trans, float newSize)
    {
        SetSize(trans, new Vector2(newSize, trans.rect.size.y));
    }
    public static void SetHeight(this RectTransform trans, float newSize)
    {
        SetSize(trans, new Vector2(trans.rect.size.x, newSize));
    }
}
