﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MessageBox : MonoBehaviour
{
    public static void ShowMessageBox(string text, float width = 500, float height = 250)
    {
        GameObject tmp = GameObject.Instantiate(Resources.Load("GUI/MessageBox")) as GameObject;

        //Set the text and size:
        tmp.transform.GetChild(0).GetComponent<RectTransform>().SetSize(new Vector2(width, height));
        tmp.transform.GetChild(0).GetChild(0).GetComponent<Text>().text = text;
    }

    public void OnClickValidate()
    {
        Destroy(gameObject);
    }
}
