﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;

public class ControlList: MonoBehaviour
{
    public GameObject ModelItem;

    private RectTransform _rect;

    private string[] _items;
    public string[] Items
    {
        get
        {
            return _items;
        }
        set
        {
            _items = new string[value.Length];
            value.CopyTo(_items, 0);
        }
    }
    private string[] _selectedItems;
    public string[] SelectedItems
    {
        get
        {
            return _selectedItems;
        }
        set
        {
            _selectedItems = value;
        }
    }

    private float _topPadding = 5.0f;
    private float _leftPadding = 5.0f;
    private float _interPadding = 2.0f;

    private float _itemHeight = 20.0f;


    void Awake()
    {
        _rect = transform.FindChild("rect").FindChild("Controls").GetComponent<RectTransform>();
    }

    public void Init(float[] MaxSpeed, float[] StopDuration, string[] Names)
    {
        float width = _rect.GetWidth();
        float height = _rect.GetHeight();

        float itemHeight;
        GameObject obj;
        for (int cnt = 0; cnt < MaxSpeed.Length; cnt++)
        {
            obj = (GameObject)GameObject.Instantiate(ModelItem);
            obj.transform.SetParent(_rect.transform, false);
            itemHeight = obj.GetComponent<RectTransform>().GetHeight();
            obj.GetComponent<RectTransform>().anchoredPosition = new Vector3(0, ((height - itemHeight) / 2f) - _topPadding - cnt * (_topPadding + itemHeight), 0f);
            obj.GetComponent<RectTransform>().SetWidth(width - 2f * _leftPadding);

            //Set values:
            obj.transform.FindChild("Label").GetComponent<Text>().text = Names[cnt];
            obj.transform.FindChild("InputSpeed").GetComponent<InputField>().text = MaxSpeed[cnt].ToString();
            obj.transform.FindChild("InputStopTime").GetComponent<InputField>().text = StopDuration[cnt].ToString();
        }
    }

    public string GetItemAt(int index)
    {
        if (_items != null && index >= 0 && index < _items.Length)
            return _items[index];
        else
            return null;
    }

    /// <summary>
    /// Add a new item at the end of the existing items
    /// </summary>
    /// <param name="str">Item text</param>
    public void AddItem(string str, float maxSpeed, float stopDuration)
    {
        float width = _rect.GetWidth();
        float height = _rect.GetHeight();

        float itemHeight;
        GameObject obj = (GameObject)GameObject.Instantiate(ModelItem);
        obj.transform.SetParent(_rect.transform, false);
        itemHeight = obj.GetComponent<RectTransform>().GetHeight();
        obj.GetComponent<RectTransform>().anchoredPosition = new Vector3(0, ((height - itemHeight) / 2f) - _topPadding - (_rect.childCount - 1) * (_topPadding + itemHeight), 0f);
        obj.GetComponent<RectTransform>().SetWidth(width - 2f * _leftPadding);

        //Set values:
        obj.transform.FindChild("Label").GetComponent<Text>().text = str;
        obj.transform.FindChild("InputSpeed").GetComponent<InputField>().text = maxSpeed.ToString();
        obj.transform.FindChild("InputStop").GetComponent<InputField>().text = stopDuration.ToString();
    }

    public void Refresh()
    {
        for(int cnt = 0; cnt < _rect.childCount; cnt++)
        {
            Destroy(_rect.GetChild(cnt).gameObject);
        }

        //Draw the list of items
        float size = 2 * _topPadding + _items.Length * (_itemHeight + _interPadding) - _interPadding;
        if (size > _rect.GetHeight())
            RectTransformExtensions.SetHeight(_rect, size);
        else
            size = _rect.GetHeight();

        //Create the buttons:
        int id = 0;
        foreach (string str in _items)
        {
            GameObject tog = (GameObject)GameObject.Instantiate(Resources.Load("GUI/GenericToggle"));
            tog.transform.SetParent(_rect.transform, false);
            tog.name = string.Format("tog_{0}", id);
            tog.transform.FindChild("Panel").GetComponent<Text>().text = str; //Set the selected index
            tog.GetComponent<RectTransform>().anchoredPosition = new Vector3(_leftPadding, (size / 2f - 15 - _topPadding) - ((_itemHeight + 5) + _interPadding) * id, 0);
            tog.GetComponent<RectTransform>().sizeDelta = new Vector2(0, 0);
            RectTransformExtensions.SetHeight(tog.GetComponent<RectTransform>(), _itemHeight);
            RectTransformExtensions.SetWidth(tog.GetComponent<RectTransform>(), _rect.GetWidth() - 5f);
            id++;
        }

    }

}
