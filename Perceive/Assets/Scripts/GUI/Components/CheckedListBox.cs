﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;

public class CheckedListBox: MonoBehaviour
{
    private RectTransform _scrollList;
    private GameObject[] _togs;


    private string[] _items;
    private bool _expand = false;
    private bool _mouseIn = false;
    public string[] Items
    {
        get
        {
            return _items;
        }
        set
        {
            _items = new string[value.Length];
            value.CopyTo(_items, 0);
        }
    }
    private string[] _selectedItems;
    public string[] SelectedItems
    {
        get
        {
            List<string> tmpstr = new List<string>();
            foreach (GameObject tog in _togs)
            {
                if (tog.GetComponent<Toggle>().isOn)
                    tmpstr.Add(tog.transform.FindChild("Label").GetComponent<Text>().text);
            }
            _selectedItems = new string[tmpstr.Count];
            _selectedItems = tmpstr.ToArray();
            return _selectedItems;
        }
        set
        {
            if (_items != null && value.Length > 0)
            {
                _selectedItems = new string[value.Length];
                value.CopyTo(_selectedItems, 0);
                Refresh(); //Refresh the component
            }
            else
            {
                if (_items == null)
                    throw new Exception("The listbox does not have Items");
            }
        }
    }

    private float _topPadding = 5.0f;
    private float _bottomPadding = 5.0f;
    private float _leftPadding = 5.0f;
    private float _rightPadding = 5.0f;
    private float _interPadding = 2.0f;

    private float _itemHeight = 20.0f;
    private float _itemWidth = 276.0f;

    private bool _dropclick = false;

    void Awake()
    {
        _scrollList = transform.FindChild("rect").FindChild("Controls").GetComponent<RectTransform>();
    }

    public void InitListBox(string[] items, string[] selectedItems = null)
    {
        //Cleaning the existing toggles:
        for (int cnt = 0; cnt < _scrollList.childCount; cnt++ )
        {
            GameObject.Destroy(_scrollList.GetChild(cnt).gameObject);
        }
        Items = new string[items.Length];
        _items = new string[items.Length];
        items.CopyTo(_items, 0);

        //Draw the list of items
        float size = 2 * _topPadding + _items.Length * (_itemHeight + _interPadding) - _interPadding;
        Debug.Log("GUI Rect size: " + size);
        if (size > _scrollList.GetHeight())
            RectTransformExtensions.SetHeight(_scrollList, size);
        else
            size = _scrollList.GetHeight();

        _togs = new GameObject[items.Length];

        if (selectedItems != null)
        {
            _selectedItems = new string[selectedItems.Length];
            selectedItems.CopyTo(_selectedItems, 0);
        }
        //Create the toggles:
        int id = 0;
        foreach (string str in _items)
        {
            GameObject tog = (GameObject)GameObject.Instantiate(Resources.Load("GUI/GenericToggle"));
            tog.transform.SetParent(_scrollList.transform, false);
            tog.name = string.Format("tog_{0}", id);
            tog.transform.FindChild("Label").GetComponent<Text>().text = str;
            if(selectedItems != null)
            {
                foreach(string s in selectedItems)
                {
                    if (s == str)
                    {
                        tog.GetComponent<Toggle>().isOn = true;
                        break;
                    }
                }
            }
            tog.GetComponent<RectTransform>().anchoredPosition = new Vector3(_leftPadding, (size / 2f - 15 - _topPadding) - ((_itemHeight + 2) + _interPadding) * id, 0);
            tog.GetComponent<RectTransform>().sizeDelta = new Vector2(0, 0);
            RectTransformExtensions.SetHeight(tog.GetComponent<RectTransform>(), _itemHeight);
            RectTransformExtensions.SetWidth(tog.GetComponent<RectTransform>(), _scrollList.GetWidth() - 5f);

            _togs[id] = tog;

            id++;
        }
    }

    public string GetItemAt(int index)
    {
        if (_items != null && index >= 0 && index < _items.Length)
            return _items[index];
        else
            return null;
    }

    /// <summary>
    /// Add a new item at the end of the existing items
    /// </summary>
    /// <param name="str">Item text</param>
    public void AddItem(string str)
    {
        string[] tmp = new string[_items.Length];
        _items.CopyTo(tmp, 0);

        int newLength = tmp.Length + 1;

        Items = new string[newLength];
        _items = new string[newLength];
        for(int cnt = 0 ; cnt < tmp.Length; cnt ++)
        {
            _items[cnt] = tmp[cnt];
        }
        _items[newLength - 1] = str;

        //Resize the drop panel:
        float size = 2 * _topPadding + _items.Length * (_itemHeight + _interPadding) - _interPadding;
        if (size > _scrollList.GetHeight())
            RectTransformExtensions.SetHeight(_scrollList, size);
        else
            size = _scrollList.GetHeight();

        //Add button:
        GameObject tog = (GameObject)GameObject.Instantiate(Resources.Load("GUI/GenericToggle"));
        tog.transform.SetParent(_scrollList.transform, false);
        tog.name = string.Format("tog_{0}", newLength - 1);
        tog.transform.FindChild("Label").GetComponent<Text>().text = str;
        tog.GetComponent<RectTransform>().anchoredPosition = new Vector3(_leftPadding, (size / 2f - 15 - _topPadding) - ((_itemHeight + 2) + _interPadding) * (newLength - 1), 0);
        tog.GetComponent<RectTransform>().sizeDelta = new Vector2(0, 0);
        RectTransformExtensions.SetHeight(tog.GetComponent<RectTransform>(), _itemHeight);
        RectTransformExtensions.SetWidth(tog.GetComponent<RectTransform>(), _scrollList.GetWidth() - 5f);
    }

    public void Refresh()
    {
        for(int cnt = 0; cnt < _scrollList.childCount; cnt++)
        {
            Destroy(_scrollList.GetChild(cnt).gameObject);
        }

        //Draw the list of items
        float size = 2 * _topPadding + _items.Length * (_itemHeight + _interPadding) - _interPadding;
        if (size > _scrollList.GetHeight())
            RectTransformExtensions.SetHeight(_scrollList, size);
        else
            size = _scrollList.GetHeight();

        //Create the buttons:
        int id = 0;
        foreach (string str in _items)
        {
            GameObject tog = (GameObject)GameObject.Instantiate(Resources.Load("GUI/GenericToggle"));
            tog.transform.SetParent(_scrollList.transform, false);
            tog.name = string.Format("tog_{0}", id);
            tog.transform.FindChild("Panel").GetComponent<Text>().text = str; //Set the selected index
            tog.GetComponent<RectTransform>().anchoredPosition = new Vector3(_leftPadding, (size / 2f - 15 - _topPadding) - ((_itemHeight + 5) + _interPadding) * id, 0);
            tog.GetComponent<RectTransform>().sizeDelta = new Vector2(0, 0);
            RectTransformExtensions.SetHeight(tog.GetComponent<RectTransform>(), _itemHeight);
            RectTransformExtensions.SetWidth(tog.GetComponent<RectTransform>(), _scrollList.GetWidth() - 5f);
            id++;
        }

    }

}