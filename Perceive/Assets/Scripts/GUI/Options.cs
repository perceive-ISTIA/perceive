﻿using UnityEngine.UI;
using UnityEngine;
using System.Collections;

public class Options : MonoBehaviour
{
    private Button[] _panelButtons;
    private IPanel[] _panels;

    public void Init()
    {
        _panelButtons = new Button[3] { transform.FindChild("Panel_tabs").FindChild("Graphics").GetComponent<Button>(), transform.FindChild("Panel_tabs").FindChild("Save").GetComponent<Button>(), transform.FindChild("Panel_tabs").FindChild("Car").GetComponent<Button>() };
        _panels = new IPanel[3] { transform.FindChild("Panel_graphics").GetComponent<OptionsGraphics>().GetNewPanelReference(), transform.FindChild("Panel_saves").GetComponent<OptionsSaves>().GetNewPanelReference(), transform.FindChild("Panel_car").GetComponent<OptionsCar>().GetNewPanelReference() };
    }


    public void ChangeOptionTab(int tabIndex)
    {
        SetSelectedButton(tabIndex);
        ShowPanel(tabIndex);
    }

    private void SetSelectedButton(int index)
    {
        ColorBlock colors;
        for (int cnt = 0; cnt < _panelButtons.Length; cnt++)
        {
            colors = _panelButtons[cnt].colors;
            if (cnt != index)
                colors.normalColor = new Color32(255, 251, 255, 255);
            else
                colors.normalColor = new Color32(103, 225, 103, 255);
            _panelButtons[cnt].colors = colors;
        }
    }

    private void ShowPanel(int index)
    {
        for (int cnt = 0; cnt < _panels.Length; cnt++)
        {
            if (cnt != index)
                _panels[cnt].Hide();
            else
            {
                _panels[cnt].Show();
                _panels[cnt].Init();
            }
        }
    }
}
