﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Experimentation : MonoBehaviour
{
    private Button[] _panelButtons;
    private IPanel[] _panels;
    private Transform _thisTransform;
    public void Init()
    {
        _thisTransform = transform;
        Transform panelTabs = _thisTransform.FindChild("Panel_tabs");
        _panelButtons = new Button[3] { panelTabs.FindChild("AI").GetComponent<Button>(), panelTabs.FindChild("PlannedPath").GetComponent<Button>(), panelTabs.FindChild("Exp01").GetComponent<Button>() };
        _panels = new IPanel[3];
        _panels[0] = _thisTransform.FindChild("Panel_ai").GetComponent<ExpAI>().GetNewPanelReference();
        _panels[1] = _thisTransform.FindChild("Panel_path").GetComponent<ExpPath>().GetNewPanelReference();
        _panels[2] = _thisTransform.FindChild("Panel_experiment01").GetComponent<ExpExp01>().GetNewPanelReference();
    }

    public void ChangeOptionTab(int tabIndex)
    {
        SetSelectedButton(tabIndex);
        ShowPanel(tabIndex);
    }

    private void SetSelectedButton(int index)
    {
        ColorBlock colors;
        for (int cnt = 0; cnt < _panelButtons.Length; cnt++ )
        {
            colors = _panelButtons[cnt].colors;
            if (cnt != index)
                colors.normalColor = new Color32(255, 251, 255, 255);
            else
                colors.normalColor = new Color32(103, 225, 103, 255);
            _panelButtons[cnt].colors = colors;
        }
    }

    private void ShowPanel(int index)
    {
        for(int cnt = 0; cnt < _panels.Length; cnt ++)
        {
            if (cnt != index)
                _panels[cnt].Hide();
            else
            {
                _panels[cnt].Show();
                _panels[cnt].Init();
            }
        }
    }
}
