﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ExpAI : MonoBehaviour
{
    private ExpAIPanel _panel;
    public ExpAIPanel panel
    {
        get
        {
            return _panel;
        }
    }

    public ExpAIPanel GetNewPanelReference()
    {
        _panel = new ExpAIPanel(transform.GetComponent<RectTransform>(), transform);
        return _panel;
    }

    public void UpdateCarSliderText()
    {
        _panel.UpdateCarSliderText();
    }

    public void UpdatePedestrianSliderText()
    {
        _panel.UpdatePedestrianSliderText();
    }

    public void Apply()
    {
        _panel.Apply();
    }

    public void Cancel()
    {
        _panel.Cancel();
    }
}

public class ExpAIPanel : IPanel
{
    //UI ELEMENTS
    private CheckedListBox _carList;
    private Slider _carSlider;
    private Text _carSliderText;
    private ControlList _carDetails;

    private CheckedListBox _pedList;
    private Slider _pedSlider;
    private Text _pedSliderText;
    private ControlList _pedDetails;

    public ExpAIPanel(RectTransform rect, Transform tr)
    {
        _rect = rect;
        _tr = tr;

        _carSlider = _tr.FindChild("Controls").FindChild("Controls").FindChild("CarSlider").GetComponent<Slider>();
        _carList = _tr.FindChild("Controls").FindChild("Controls").FindChild("CarList").GetComponent<CheckedListBox>();
        _carSliderText = _carSlider.transform.FindChild("Text").GetComponent<Text>();
        _carDetails = _tr.FindChild("Controls").FindChild("Controls").FindChild("CarDetails").GetComponent<ControlList>();

        _pedSlider = _tr.FindChild("Controls").FindChild("Controls").FindChild("PedestrianSlider").GetComponent<Slider>();
        _pedList = _tr.FindChild("Controls").FindChild("Controls").FindChild("PedestrianList").GetComponent<CheckedListBox>();
        _pedSliderText = _pedSlider.transform.FindChild("Text").GetComponent<Text>();
        _pedDetails = _tr.FindChild("Controls").FindChild("Controls").FindChild("PedestrianDetails").GetComponent<ControlList>();
    }


    public void UpdateCarSliderText()
    {
        _carSliderText.text = string.Format("Nombre de véhicules: {0}", (int)_carSlider.value);
    }

    public void UpdatePedestrianSliderText()
    {
        _pedSliderText.text = string.Format("Nombre de piétons: {0}", (int)_pedSlider.value);
    }


    private RectTransform _rect;
    public RectTransform rectTransform
    {
        get
        {
            return _rect;
        }
        set
        {
            _rect = value;
        }
    }

    private Transform _tr;
    public Transform transform
    {
        get
        {
            return _tr;
        }
        set
        {
            _tr = value;
        }
    }

    public void Init()
    {
        _carList.InitListBox(ObjectManager.GetOptions().CarModels, ObjectManager.GetOptions().SelectedCars);

        _carDetails.Init(ObjectManager.GetOptions().CarMaxSpeed, ObjectManager.GetOptions().CarStopDuration, ObjectManager.GetOptions().CarModels);

        _carSlider.value = ObjectManager.GetOptions().CarCount;

        //Pedestrians
        _pedList.InitListBox(ObjectManager.GetOptions().PedestrianModels, ObjectManager.GetOptions().SelectedPedestrians);

        //_pedDetails.Init(options.CarMaxSpeed, options.CarStopDuration, options.PedestrianModels);

        _pedSlider.value = ObjectManager.GetOptions().PedestrianCount;



        UpdateCarSliderText();
    }

    public void Hide()
    {
        _tr.gameObject.SetActive(false);
    }

    public void Show()
    {
        _tr.gameObject.SetActive(true);
    }
    
    public void Apply()
    {
        ObjectManager.GetOptions().SelectedCars = _carList.SelectedItems;
        ObjectManager.GetOptions().CarCount = (int)_carSlider.value;

        ObjectManager.GetOptions().SelectedPedestrians = _pedList.SelectedItems;
        ObjectManager.GetOptions().PedestrianCount = (int)_pedSlider.value;

        ObjectManager.GetOptions().Save();

        Hide();
    }

    public void Cancel()
    {
        Hide();
    }
}
