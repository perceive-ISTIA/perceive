﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Xml.Serialization;
using System.IO;

public class StartExperimentation : MonoBehaviour
{
    public AutoMove autoMoveScript;
    public AIController aiController;
    public BehavioralData Data;
    public ExperimentController Experiment;
    public MainMenu Main;

    private StartExperimentationPanel _panel;
    public StartExperimentationPanel panel
    {
        get
        {
            return _panel;
        }
    }

    public StartExperimentationPanel GetNewPanelReference()
    {
        _panel = new StartExperimentationPanel(transform.GetComponent<RectTransform>(), transform, aiController, Experiment, autoMoveScript, Data, Main);
        return _panel;
    }

    public void Apply()
    {
        _panel.Apply();
    }

    public void Cancel()
    {
        _panel.Cancel();
    }

    public void Show()
    {
        _panel.Show();
    }
}

public class StartExperimentationPanel : IPanel
{
    //UI ELEMENTS
    private Text _textPlanner;
    private Text _textRail;
    private Text _textEnv;
    private Text _textAI;
    private Text _textExperiments;
    private Text _textTrafficLights;

    private InputField _inputName;


    private AutoMove _autoMoveScript;
    private AIController _aiController;
    private BehavioralData _data;
    private ExperimentController _experiment;
    private MainMenu _mainMenu;

    private RectTransform _rect;
    public RectTransform rectTransform
    {
        get
        {
            return _rect;
        }
        set
        {
            _rect = value;
        }
    }

    private Transform _thisTransform;
    public Transform transform
    {
        get
        {
            return _thisTransform;
        }
        set
        {
            _thisTransform = value;
        }
    }

    public StartExperimentationPanel(RectTransform rect, Transform tr, AIController aiController, ExperimentController experimentController, AutoMove autoMove, BehavioralData data, MainMenu mainMenu)
    {
        _rect = rect;
        _thisTransform = tr;

        _autoMoveScript = autoMove;
        _aiController = aiController;
        _data = data;
        _mainMenu = mainMenu;
        _experiment = experimentController;

        Transform controls = _thisTransform.FindChild("Controls");

        _textPlanner = controls.FindChild("TextPlanner").GetComponent<Text>();
        _textRail = controls.FindChild("TextRail").GetComponent<Text>();
        _textEnv = controls.FindChild("TextEnv").GetComponent<Text>();
        _textAI = controls.FindChild("TextAI").GetComponent<Text>();
        _textExperiments = controls.FindChild("TextExperiments").GetComponent<Text>();
        _textTrafficLights = controls.FindChild("TextTrafficLights").GetComponent<Text>();

        _inputName = controls.FindChild("InputSubject").GetComponent<InputField>();
    }

    public void Init()
    {
        string str = "";
        _textPlanner.text = string.Format("Utilisation d\'un chemin prédéfini: {0}", ObjectManager.GetOptions().isPlanner);
        _textRail.text = string.Format("Utilisation du rail: {0}", ObjectManager.GetOptions().isRail);
        _textEnv.text = string.Format("Environnement; Centre-Ville");
        str = "";
        if (ObjectManager.GetOptions().CarCount > 0)
            str = string.Format("Voitures (x{0})", ObjectManager.GetOptions().CarCount);
        if (ObjectManager.GetOptions().PedestrianCount > 0)
        {
            if(str != "")
                str = string.Format("{0}/Piétons (x{1})", str, ObjectManager.GetOptions().PedestrianCount);
            else
                str = string.Format("Piétons (x{0})", ObjectManager.GetOptions().PedestrianCount);
        }
        _textAI.text = string.Format("IA: {0}", str);

        try
        {
            ExperimentXML experimentObject = new ExperimentXML();
            XmlSerializer serializer = new XmlSerializer(typeof (ExperimentXML));
            using (
                StreamReader stream = new StreamReader(ObjectManager.GetOptions().PlannedPathExperimentFile,
                    System.Text.Encoding.GetEncoding("UTF-8")))
            {
                experimentObject = serializer.Deserialize(stream) as ExperimentXML;
                stream.Close();

                str = "";
                foreach (Enums.ExperimentType type in experimentObject.Type)
                {
                    if (!str.Contains(type.ToString()))
                    {
                        if (str == "")
                            str = type.ToString();
                        else
                            str = string.Format("{0} / {1}", str, type.ToString());
                    }
                }
            }
        }
        catch (Exception e)
        {
            str = "Fichier de route introuvable";
        }
        _textExperiments.text = string.Format("Expérimentation: {0}", str);
        _textTrafficLights.text = string.Format("Feux tricolores: {0}", ObjectManager.GetOptions().isLightsRandom);
    }

    public void Hide()
    {
        _thisTransform.gameObject.SetActive(false);
    }

    public void Show()
    {
        Init();
        _thisTransform.gameObject.SetActive(true);
    }

    public void Apply()
    {
        //Get the subject name
        string subjectName = _inputName.text;
        if (subjectName == "" || subjectName.IndexOfAny(Path.GetInvalidFileNameChars()) >= 0 || subjectName.Contains("_"))
        {
            Debug.Log(subjectName.IndexOfAny(Path.GetInvalidFileNameChars()));
            return;
        }

        Debug.Log("Start");
        //Set AI:
        if (ObjectManager.GetOptions().CarCount > 0)
        {
            _aiController.Cars = ObjectManager.GetOptions().CarCount;
            _aiController.CarsObjects = ObjectManager.GetOptions().CarModels;
            _aiController.CarSpeed = ObjectManager.GetOptions().CarMaxSpeed;
            _aiController.CarStopDuration = ObjectManager.GetOptions().CarStopDuration;

            _aiController.StartAI();
        }

        //Experiment objects:
        _experiment.LoadExperiments(ObjectManager.GetOptions().PlannedPathExperimentFile);

        //Path planner:
        if (ObjectManager.GetOptions().isPlanner)
        {
            PathPlanner.LoadDataFromFile(ObjectManager.GetOptions().PlannedPathFile, ObjectManager.GetOptions().isLightsRandom);
            if (ObjectManager.GetOptions().isRail)
                _autoMoveScript.StartAutoMove(ObjectManager.GetOptions().PlannedPathFile);
        }
        else
        {
            ObjectManager.GetOptions().PlannedPathFile = "";
        }

        if (ObjectManager.GetOptions().isLightsRandom)
        {
            //Start traffic lights:
            foreach (TrafficLightGroupController trafficLight in ObjectManager.TrafficLights)
            {
                trafficLight.isRandom = true;
                trafficLight.InitLights();
            }
        }

        //Start the save:
        if (ObjectManager.GetOptions().CarCount > 0 || ObjectManager.GetOptions().PedestrianCount > 0)
            _data.StartRecording(
                ObjectManager.GetOptions().DataInterval,
                true,
                ObjectManager.GetOptions().isRail,
                subjectName,
                ObjectManager.GetOptions().PlannedPathFile,
                ObjectManager.GetOptions().isLightsRandom,
                ObjectManager.GetOptions().isRail);
        else
            _data.StartRecording(ObjectManager.GetOptions().DataInterval,
                false,
                ObjectManager.GetOptions().isRail,
                subjectName,
                ObjectManager.GetOptions().PlannedPathFile,
                ObjectManager.GetOptions().isLightsRandom,
                ObjectManager.GetOptions().isRail);

        //Start traffic lights:
        foreach (TrafficLightGroupController trafficLight in ObjectManager.TrafficLights)
        {
            trafficLight.StartLights();
        }

        Hide();

        _mainMenu.gameObject.SetActive(false);
    }

    public void Cancel()
    {
        Hide();
    }
}
