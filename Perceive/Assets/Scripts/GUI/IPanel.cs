﻿using UnityEngine;
using System.Collections;

public interface IPanel
{
    RectTransform rectTransform { get; set; }

    Transform transform { get; set; }

    void Init();

    void Hide();

    void Show();

    void Apply();

    void Cancel();
}
