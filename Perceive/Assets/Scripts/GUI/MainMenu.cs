﻿using UnityEngine.UI;
using UnityEngine;
using System.Collections;

public class MainMenu : MonoBehaviour
{
    /*
     * Options
     */
    public Options PanelOptions;
    public Experimentation PanelExperimentation;
    public Replay PanelReplay;
    public StartExperimentation PanelStartExperimentation;
    /*public AutoMove autoMoveScript;
    public AIController aiController;
    public BehavioralData Data;
    public ExperimentController Experiment;*/

    private RectTransform _start;
    private RectTransform _experimentation;
    private RectTransform _options;
    private RectTransform _replay;
    private RectTransform _exit;
    private RectTransform[] _btns;
    private Button[] _btnsButton;
    private Image[] _btnsImage;
    public GameObject PausePanel;

    private Vector2 _highlightedSize;
    private Vector2 _initialSize;

    private Transform _thisTransform;

    private bool _clickedState = false;

    void Awake()
    {
        _thisTransform = transform;
        _highlightedSize = new Vector2(983.5f, 58);

        _start = _thisTransform.FindChild("Panel").FindChild("BTN_Start").GetComponent<RectTransform>();

        _initialSize = _start.sizeDelta;
        _experimentation = _thisTransform.FindChild("Panel").FindChild("BTN_Experimentation").GetComponent<RectTransform>();
        _options = _thisTransform.FindChild("Panel").FindChild("BTN_Options").GetComponent<RectTransform>();
        _replay = _thisTransform.FindChild("Panel").FindChild("BTN_Replay").GetComponent<RectTransform>();
        _exit = _thisTransform.FindChild("Panel").FindChild("BTN_Exit").GetComponent<RectTransform>();

        _btns = new RectTransform[5] { _start, _experimentation, _options, _replay, _exit };
        _btnsButton = new Button[5] { _start.GetComponent<Button>(), _experimentation.GetComponent<Button>(), _options.GetComponent<Button>(), _replay.GetComponent<Button>(), _exit.GetComponent<Button>() };
        _btnsImage = new Image[5] { _start.GetComponent<Image>(), _experimentation.GetComponent<Image>(), _options.GetComponent<Image>(), _replay.GetComponent<Image>(), _exit.GetComponent<Image>() };
    }

	// Use this for initialization
	void Start ()
    {
        PanelOptions.Init();
        PanelExperimentation.Init();
	}


    public void OnBTNExit()
    {
        FreezeFocusOnButton(3);
        Application.Quit();
    }

    public void OnBTNOptions()
    {
        if (!PanelOptions.gameObject.activeInHierarchy)
        {
            PanelOptions.gameObject.SetActive(true);
            PanelExperimentation.gameObject.SetActive(false);
            PanelStartExperimentation.gameObject.SetActive(false);
            PanelReplay.ReplayMenuGUI.gameObject.SetActive(false);
            FreezeFocusOnButton(2);

        }
        else
        {
            PanelOptions.gameObject.SetActive(false);

            ReleaseFocusOnButton(2);
        }
    }

    public void OnBTNStart()
    {
        if (!PanelStartExperimentation.gameObject.activeInHierarchy)
        {
            PanelStartExperimentation.GetNewPanelReference();
            PanelStartExperimentation.Show();
            PanelExperimentation.gameObject.SetActive(false);
            PanelOptions.gameObject.SetActive(false);
            PanelReplay.ReplayMenuGUI.gameObject.SetActive(false);
            FreezeFocusOnButton(0);

        }
        else
        {
            PanelStartExperimentation.gameObject.SetActive(false);

            ReleaseFocusOnButton(0);
        }
    }

    public void OnBTNExperimentation()
    {
        if (!PanelExperimentation.gameObject.activeInHierarchy)
        {
            PanelExperimentation.gameObject.SetActive(true);
            PanelOptions.gameObject.SetActive(false);
            PanelStartExperimentation.gameObject.SetActive(false);
            PanelReplay.ReplayMenuGUI.gameObject.SetActive(false);
            FreezeFocusOnButton(1);

        }
        else
        {
            PanelExperimentation.gameObject.SetActive(false);

            ReleaseFocusOnButton(1);
        }
    }


    public void OnBTNReplay()
    {
        if (!PanelReplay.ReplayMenuGUI.gameObject.activeInHierarchy)
        {
            PanelReplay.ReplayMenuGUI.gameObject.SetActive(true);
            PanelExperimentation.gameObject.SetActive(false);
            PanelStartExperimentation.gameObject.SetActive(false);
            PanelOptions.gameObject.SetActive(false);
            PanelReplay.Init();
            FreezeFocusOnButton(4);
        }
        else
        {
            PanelExperimentation.gameObject.SetActive(false);
            ReleaseFocusOnButton(4);
        }
    }


    public void FreezeFocusOnButton(int id)
    {
        for(int cnt = 0; cnt < _btns.Length; cnt++)
        {
            if(cnt == id)
            {
                _btnsButton[cnt].transition = Selectable.Transition.None;
                _btnsImage[cnt].color = new Color32(161, 251, 161, 255);

                _btns[cnt].sizeDelta = _highlightedSize;
            }
            else
            {
                _btnsButton[cnt].transition = Selectable.Transition.Animation;

                _btnsImage[cnt].color = new Color32(255, 255, 255, 255);
                _btns[cnt].sizeDelta = _initialSize;
            }
        }
    }
    public void ReleaseFocusOnButton(int id)
    {
        _btnsButton[id].transition = Selectable.Transition.Animation;
        _btnsImage[id].color = new Color32(255, 255, 255, 255);

        _btns[id].sizeDelta = _initialSize;

    }
}
