﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class OptionsGraphicsPauseMenu : MonoBehaviour
{
    private GraphicsPanelPauseMenu _panel;
    public GraphicsPanelPauseMenu panel
    {
        get
        {
            return _panel;
        }
    }

    public GraphicsPanelPauseMenu GetNewPanelReference()
    {
        _panel = new GraphicsPanelPauseMenu(transform.GetComponent<RectTransform>(), transform);
        return _panel;
    }

    public void UpdateVegetationSliderText()
    {
        _panel.UpdateVegetationSliderText();
    }

    public void Apply()
    {
        _panel.Apply();
    }

    public void Cancel()
    {
        _panel.Cancel();
    }

    public void Hide()
    {
        _panel.Hide();
    }

    public void Show()
    {
        _panel.Show();
    }
}

public class GraphicsPanelPauseMenu : IPanel
{
    private Combobox _resolution;
    private Combobox _quality;
    private Slider _vegetationSlider;
    private Text _vegetationText;

    public GraphicsPanelPauseMenu(RectTransform rect, Transform tr)
    {
        _rect = rect;
        _tr = tr;

        _resolution = _tr.FindChild("Controls").FindChild("ComboboxResolution").GetComponent<Combobox>();
        _resolution.ValueChanged += new ComboboxEventHandler(this._resolution_ValueChanged);
        _quality = _tr.FindChild("Controls").FindChild("ComboboxQuality").GetComponent<Combobox>();
        _quality.ValueChanged += new ComboboxEventHandler(this._quality_ValueChanged);
        _vegetationSlider = _tr.FindChild("Controls").FindChild("VegetationSlider").GetComponent<Slider>();
        _vegetationText = _vegetationSlider.transform.FindChild("Text").GetComponent<Text>();

    }

    private RectTransform _rect;
    public RectTransform rectTransform
    {
        get
        {
            return _rect;
        }
        set
        {
            _rect = value;
        }
    }

    private Transform _tr;
    public Transform transform
    {
        get
        {
            return _tr;
        }
        set
        {
            _tr = value;
        }
    }

    public void Init()
    {
        _resolution.InitCombobox(ObjectManager.Resolutions, ObjectManager.GetOptions().Resolution);

        _quality.InitCombobox(ObjectManager.Quality, ObjectManager.GetOptions().Quality);

        _vegetationSlider.value = ObjectManager.GetOptions().Vegetation;
    }

    public void UpdateVegetationSliderText()
    {
        int value = (int)_vegetationSlider.value;
        if (_vegetationSlider.value != (float)value)
            _vegetationSlider.value = (float)value;
        switch(value)
        {
            case 0:
                _vegetationText.text = "Aucune";
                break;
            case 1:
                _vegetationText.text = "Buissons";
                break;
            case 2:
                _vegetationText.text = "Arbres";
                break;
            case 3:
                _vegetationText.text = "Toute la végétation";
                break;
        }
    }

    public void Hide()
    {
        _tr.gameObject.SetActive(false);
    }

    public void Show()
    {
        _tr.gameObject.SetActive(true);
    }

    public void Apply()
    {
        //Save
        ObjectManager.GetOptions().Quality = _quality.SelectedItemIndex;
        ObjectManager.GetOptions().Resolution = _resolution.SelectedItemIndex;
        ObjectManager.GetOptions().Vegetation = (int)_vegetationSlider.value;

        ObjectManager.GetOptions().Save();

        QualitySettings.SetQualityLevel(_quality.SelectedItemIndex);
        

        Hide();
    }

    public void Cancel()
    {
        //Reset resolution:
        if (_resolution.SelectedItemIndex != ObjectManager.GetOptions().Resolution)
        {
            string[] split = ObjectManager.Resolutions[ObjectManager.GetOptions().Resolution].Split('x');
            int width = int.Parse(split[0]);
            int height = int.Parse(split[1]);
            Screen.SetResolution(width, height, true);
        }
        if (_quality.SelectedItemIndex != ObjectManager.GetOptions().Quality)
            QualitySettings.SetQualityLevel(ObjectManager.GetOptions().Quality);
        Hide();
    }


    private void _resolution_ValueChanged(object sender, System.EventArgs args)
    {
        string[] split = _resolution.SelectedItem.Split('x');
        int width = int.Parse(split[0]);
        int height = int.Parse(split[1]);
        Screen.SetResolution(width, height, true);
    }
    private void _quality_ValueChanged(object sender, System.EventArgs args)
    {
        QualitySettings.SetQualityLevel(_quality.SelectedItemIndex);
    }
}
