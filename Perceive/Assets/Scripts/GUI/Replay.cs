﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
using System.IO;
using System.Xml.Serialization;

public class Replay : MonoBehaviour 
{
    public Transform ReplayMenuGUI;
    public AIController AIcontroller;
    public GameObject FrontCamera;
    public GameObject PlayerModel;
    public GameObject TopCamera;
    public ExperimentController ExpController;
    private ListBox _list;
    private Transform _thisTransform;
    public bool OnPlay { get; set; }
    private Transform _player;
    private int _currentIndex = 0;
    private LineRenderer _lineRenderer;

    private float _tick;
    private float _maxTime;
    private int _dataSize = 0;
    private int _aiDataSize = 0;
    private int _expSize = 0;
    private int _aiSize = 0;

    //Panel infos
    private Text _infoTextFile;
    private Text _infoTextDate;
    private Text _infoTextSessionTime;
    private Text _infoTextExperimentations;
    private Text _infoTextCars;
    private Text _infoTextPedestrians;
    private Text _infoTextPlanner;
    private Text _infoTextRail;
    private Text _infoTextTrafficLights;



    //Form components:
    private Button _buttonPlayPause;
    private Text _textPlayPause;
    private bool _onPause = false;
    private int _increment = 1;
    private Slider _sliderTime;
    private Text _textTime;
    private Text _textFile;
    private Transform _controls;

    private Text _textPosition;
    private Text _textRotation;
    private Text _textSpeed;
    private Text _textExpType;
    private Text _textExpResult;

    private string _currentExperiment = "";

    //Temp/converted arrays
    private Vector3[] _pos;
    private string[] _experiments;
    private string[] _experimentsResults;
    private Enums.ExperimentType[] _experimentsType;
    private Quaternion[] _rot;
    private float[] _speed;
    private string[] _dataExp;

    //AI
    private bool _isAI;
    private Vector3[][] _AIpos;
    private Quaternion[][] _AIrot;
    private Transform[] _AItr;
    private TrafficLightsDetailsStateXML[] _lights;

    //Lights
    private byte[][] _rebuiledLights;

    void Awake()
    {
        _thisTransform = transform;
        _list = ReplayMenuGUI.FindChild("ListBox").GetComponent<ListBox>();
        _player = GameObject.FindGameObjectWithTag("Player").transform;
        _isAI = false;

        _lineRenderer = _thisTransform.GetComponent<LineRenderer>();
        //Panel infos:
        Transform infoControls = ReplayMenuGUI.FindChild("Panel_infos").FindChild("Controls");
        _infoTextFile = infoControls.FindChild("SelectedFileText").GetComponent<Text>();
        _infoTextDate = infoControls.FindChild("Date").GetComponent<Text>();
        _infoTextSessionTime = infoControls.FindChild("SessionTime").GetComponent<Text>();
        _infoTextExperimentations = infoControls.FindChild("Experimentation").GetComponent<Text>();
        _infoTextCars = infoControls.FindChild("CarsCount").GetComponent<Text>();
        _infoTextPedestrians = infoControls.FindChild("PedestriansCount").GetComponent<Text>();
        _infoTextPlanner = infoControls.FindChild("PlannerType").GetComponent<Text>();
        _infoTextRail = infoControls.FindChild("Rail").GetComponent<Text>();
        _infoTextTrafficLights = infoControls.FindChild("TrafficLights").GetComponent<Text>();

        //Form components:
        _controls = _thisTransform.FindChild("Controls");
        Transform panelInfos = _controls.FindChild("PanelInfos");
        _buttonPlayPause = _controls.FindChild("ButtonPlayPause").GetComponent<Button>();
        _textPlayPause = _buttonPlayPause.transform.FindChild("Text").GetComponent<Text>();
        _sliderTime = _controls.FindChild("SliderTime").GetComponent<Slider>();
        _textFile = _controls.FindChild("TextCurrent").GetComponent<Text>();
        _textTime = _controls.FindChild("TextTime").GetComponent<Text>();

        _textPosition = panelInfos.FindChild("TextPosition").GetComponent<Text>();
        _textRotation = panelInfos.FindChild("TextRotation").GetComponent<Text>();
        _textSpeed = panelInfos.FindChild("TextSpeed").GetComponent<Text>();
        _textExpType = panelInfos.FindChild("TextExpType").GetComponent<Text>();
        _textExpResult = panelInfos.FindChild("TextExpResult").GetComponent<Text>();
    }

    public void RefreshList()
    {
        string[] files;
        int count = 0;
        foreach (string file in Directory.GetFiles(Application.dataPath + "\\Saves"))
        {
            if (file.Contains(".xml") && !file.Contains(".meta") && file.Contains("_SAVE"))
            {
                count++;
            }
        }

        files = new string[count];

        count = 0;
        foreach (string file in Directory.GetFiles(Application.dataPath + "\\Saves"))
        {
            if (file.Contains(".xml") && !file.Contains(".meta") && file.Contains("_SAVE"))
            {
                files[count] = file.Substring(file.LastIndexOf(@"\") + 1);
                count++;
            }
        }

        _list.InitListBox(files);
    }


    public void Init()
    {
        string[] files;
        int count = 0;
        foreach(string file in Directory.GetFiles(Application.dataPath + "\\Saves"))
        {
            if (file.Contains(".xml") && !file.Contains(".meta") && file.Contains("_SAVE"))
            {
                count++;
            }
        }

        files = new string[count];

        count = 0;
        foreach (string file in Directory.GetFiles(Application.dataPath + "\\Saves"))
        {
            if (file.Contains(".xml") && !file.Contains(".meta") && file.Contains("_SAVE"))
            {
                files[count] = file.Substring(file.LastIndexOf(@"\") + 1);
                count++;
            }
        }

        _list.InitListBox(files);
        _list.ItemChanged += new ListboxItemChanged(this._list_ItemChanged);
    }
	
    private void _list_ItemChanged(object sender, EventArgs e)
    {
        Debug.Log("Item changed");
        //Open the selected file:
        SavedGame savedData = new SavedGame();
        XmlSerializer serializer = new XmlSerializer(typeof(SavedGame));
        using (StreamReader stream = new StreamReader(string.Format("{0}\\Saves\\{1}", Application.dataPath, _list.SelectedItem), System.Text.Encoding.GetEncoding("UTF-8")))
        {
            savedData = serializer.Deserialize(stream) as SavedGame;
            stream.Close();
        }

        
        _infoTextFile.text = string.Format("Fichier de sauvegarde: {0}\\Saves\\{1}", Application.dataPath, _list.SelectedItem);
        _infoTextDate.text = string.Format("Date: {0}", savedData.Date.ToString());
        _infoTextSessionTime.text = string.Format("Durée de la session: {0}", savedData.Tick * savedData.Data.Count);
        #region Experimentations
        if (File.Exists(string.Format("{0}\\Saves\\{1}_RESULTS.xml", Application.dataPath, _list.SelectedItem.Replace("_SAVE.xml", ""))))
        {
            ExperimentResultsXML savedResultsData = new ExperimentResultsXML();
            serializer = new XmlSerializer(typeof(ExperimentResultsXML));
            using (StreamReader stream = new StreamReader(string.Format("{0}\\Saves\\{1}_RESULTS.xml", Application.dataPath, _list.SelectedItem.Replace("_SAVE.xml", "")), System.Text.Encoding.GetEncoding("UTF-8")))
            {
                savedResultsData = serializer.Deserialize(stream) as ExperimentResultsXML;
                stream.Close();
            }

            bool[] expTypes = new bool[1] { false };
            int[] expCount = new int[1] { 0 };
            foreach(Enums.ExperimentType type in savedResultsData.Type)
            {
                switch(type)
                {
                    case Enums.ExperimentType.Experiment01:
                        expTypes[0] = true;
                        expCount[0]++;
                        break;
                }
            }

            string expstr = "";
            for(int cnt = 0; cnt < expTypes.Length; cnt++)
            {
                switch(cnt)
                {
                    case 0:
                        if(expCount[0] > 0)
                            expstr = string.Format("{0}Tâche1 [x{1}]/", expstr, expCount[0]);
                        break;
                }
            }
            if (expstr != "")
                _infoTextExperimentations.text = string.Format("Tâche(s): {0}", expstr);
            else
                _infoTextExperimentations.text = "Aucunes tâches";
        }
        #endregion
        #region AI
        if (File.Exists(string.Format("{0}\\Saves\\{1}_AI.xml", Application.dataPath, _list.SelectedItem.Replace("_SAVE.xml", ""))))
        {
            SavedAI savedAIData = new SavedAI();
            serializer = new XmlSerializer(typeof(SavedAI));
            using (StreamReader stream = new StreamReader(string.Format("{0}\\Saves\\{1}_AI.xml", Application.dataPath, _list.SelectedItem.Replace("_SAVE.xml", "")), System.Text.Encoding.GetEncoding("UTF-8")))
            {
                savedAIData = serializer.Deserialize(stream) as SavedAI;
                stream.Close();
            }
            string Carstr = "";
            int CarCount = 0;
            string Pedestrianstr = "";
            int PedestrianCount = 0;
            foreach(AIRow row in savedAIData.Data[0].ActiveAI)
            {
                if (row.Type == Enums.AIType.Vehicle)
                {
                    if (!Carstr.Contains(row.Name.Remove(row.Name.IndexOf("("))))
                        Carstr = string.Format("{0}{1}/", Carstr, row.Name.Remove(row.Name.IndexOf("(")));
                    CarCount++;
                }
                else
                {
                    if (!Pedestrianstr.Contains(row.Name.Remove(row.Name.IndexOf("("))))
                        Pedestrianstr = string.Format("{0}{1}/", Pedestrianstr, row.Name);
                    PedestrianCount++;
                }
            }
            if(CarCount > 0)
                _infoTextCars.text = string.Format("Véhicules[{0}]: {1}", CarCount, Carstr);
            else
                _infoTextCars.text = "Véhicules[0]: Aucuns";
            if(PedestrianCount > 0)
                _infoTextPedestrians.text = string.Format("Piétons[{0}]: {1}", PedestrianCount, Pedestrianstr);
            else
                _infoTextPedestrians.text = "Piétons[0]: Aucuns";
        }
        else
        {
            _infoTextCars.text = "Véhicules[0]: Aucuns";
            _infoTextPedestrians.text = "Piétons[0]: Aucuns";
        }
        #endregion
        if (savedData.PlannerFile != "null")
            _infoTextPlanner.text = string.Format("Fichier de trajet: {0}", savedData.PlannerFile);
        else
            _infoTextPlanner.text = "Fichier de trajet: Pas de trajet prédéfini";
        _infoTextRail.text = string.Format("Utilisation du rail: {0}", savedData.isRail);
        _infoTextTrafficLights.text = string.Format("Feux aléatoires: {0}", savedData.isLightRandom);
    }

    public void Play()
    {
        ExpController.DeleteExperiments();
        AIcontroller.DestroyExistingAI();
        if (_list.SelectedItem != null)
        {
            //Get positions
            SavedGame savedData = new SavedGame();
            XmlSerializer serializer = new XmlSerializer(typeof(SavedGame));
            using (StreamReader stream = new StreamReader(string.Format("{0}\\Saves\\{1}", Application.dataPath, _list.SelectedItem), System.Text.Encoding.GetEncoding("UTF-8")))
            {
                savedData = serializer.Deserialize(stream) as SavedGame;
                stream.Close();
            }
            
            //Looking for the experiment results:
            ExperimentResultsXML experimentsData = null;
            if (File.Exists(string.Format("{0}\\Saves\\{1}RESULTS.xml", Application.dataPath, _list.SelectedItem.Replace("SAVE.xml", ""))))
            {
                experimentsData = new ExperimentResultsXML();
                serializer = new XmlSerializer(typeof(ExperimentResultsXML));
                using (StreamReader stream = new StreamReader(string.Format("{0}\\Saves\\{1}RESULTS.xml", Application.dataPath, _list.SelectedItem.Replace("SAVE.xml", "")), System.Text.Encoding.GetEncoding("UTF-8")))
                {
                    experimentsData = serializer.Deserialize(stream) as ExperimentResultsXML;
                    stream.Close();
                }
            }
            else
            {
                _textExpType.text = "";
                _textExpResult.text = "";
            }
            #region Load AI file
            SavedAI savedAIData = null;
            _isAI = false;
            if (File.Exists(string.Format("{0}\\Saves\\{1}AI.xml", Application.dataPath, _list.SelectedItem.Replace("SAVE.xml", ""))))
            {
                _isAI = true;
                savedAIData = new SavedAI();
                serializer = new XmlSerializer(typeof(SavedAI));
                using (StreamReader stream = new StreamReader(string.Format("{0}\\Saves\\{1}AI.xml", Application.dataPath, _list.SelectedItem.Replace("SAVE.xml", "")), System.Text.Encoding.GetEncoding("UTF-8")))
                {
                    savedAIData = serializer.Deserialize(stream) as SavedAI;
                    stream.Close();
                }
                AIcontroller.enabled = false;
            }
            #endregion


            //Set local arrays
            _dataSize = savedData.Data.Count;
            _expSize = experimentsData.Size;
            _pos = new Vector3[_dataSize];
            _rot = new Quaternion[_dataSize];
            _dataExp = new string[_dataSize];
            _speed = new float[_dataSize];
            _experimentsResults = new string[_expSize];
            _experiments = new string[_expSize];
            _experimentsType = new Enums.ExperimentType[_expSize];

            #region AI
            if (savedAIData != null)
            {
                _aiDataSize = savedAIData.Data[0].ActiveAI.Length;
                _AIpos = new Vector3[_dataSize][];
                _AIrot = new Quaternion[_dataSize][];
                _AItr = new Transform[_aiDataSize];
                //Spawn vehicles:
                // Get the fist row and spawn the vehicles at the first pos:
                GameObject tempGO;
                for(int cnt = 0; cnt < _aiDataSize; cnt ++)
                {
                    Debug.Log(savedAIData.Data[0].ActiveAI[cnt].Name.Remove(savedAIData.Data[0].ActiveAI[cnt].Name.IndexOf("(")));
                    if(savedAIData.Data[0].ActiveAI[cnt].Type == Enums.AIType.Vehicle)
                        tempGO = (GameObject)GameObject.Instantiate(Resources.Load("Cars_replay/" + savedAIData.Data[0].ActiveAI[cnt].Name.Remove(savedAIData.Data[0].ActiveAI[cnt].Name.IndexOf("("))));
                    else
                        tempGO = (GameObject)GameObject.Instantiate(Resources.Load("Pedestrians_replay/" + savedAIData.Data[0].ActiveAI[cnt].Name.Remove(savedAIData.Data[0].ActiveAI[cnt].Name.IndexOf("("))));
                    tempGO.name = savedAIData.Data[0].ActiveAI[cnt].Name;
                    _AItr[cnt] = tempGO.transform;
                    if (savedAIData.Data[0].ActiveAI[cnt].Type == Enums.AIType.Vehicle)
                        _AItr[cnt].SetParent(AIcontroller.transform.FindChild("Cars"));
                    else
                        _AItr[cnt].SetParent(AIcontroller.transform.FindChild("Pedestrians"));
                    _AItr[cnt].position = Tools.ParseStringToVector3(savedAIData.Data[0].ActiveAI[cnt].Position);
                    _AItr[cnt].rotation = Tools.ParseStringToQuaternion(savedAIData.Data[0].ActiveAI[cnt].Rotation);
                }
            }
            #endregion

            #region Lights
            TrafficLightsDetailsXML savedLights = new TrafficLightsDetailsXML();
            serializer = new XmlSerializer(typeof(TrafficLightsDetailsXML));
            using (StreamReader stream = new StreamReader(string.Format("{0}\\Saves\\{1}LIGHTSDETAILS.xml", Application.dataPath, _list.SelectedItem.Replace("SAVE.xml", "")), System.Text.Encoding.GetEncoding("UTF-8")))
            {
                savedLights = serializer.Deserialize(stream) as TrafficLightsDetailsXML;
                stream.Close();
            }

            _lights = new TrafficLightsDetailsStateXML[_dataSize];
            int lightsCount = savedLights.Data[0].State01.Length;

            for (int cnt = 0; cnt < _dataSize; cnt++ )
            {
                _lights[cnt] = new TrafficLightsDetailsStateXML();
                _lights[cnt].State01 = new Enums.LightState[lightsCount];
                _lights[cnt].State02 = new Enums.LightState[lightsCount];
                _lights[cnt].State03 = new Enums.LightState[lightsCount];
                _lights[cnt].State04 = new Enums.LightState[lightsCount];
                for(int i = 0; i < lightsCount; i++)
                {
                    _lights[cnt].State01[i] = savedLights.Data[cnt].State01[i];
                    _lights[cnt].State02[i] = savedLights.Data[cnt].State02[i];
                    _lights[cnt].State03[i] = savedLights.Data[cnt].State03[i];
                    _lights[cnt].State04[i] = savedLights.Data[cnt].State04[i];
                }
            }
            #endregion

                _lineRenderer.SetVertexCount(_dataSize);
            for (int i = 0; i < _dataSize; i++)
            {
                _pos[i] = Tools.ParseStringToVector3(savedData.Data[i].PlayerPosition);
                _rot[i] = Tools.ParseStringToQuaternion(savedData.Data[i].PlayerRotation);
                _speed[i] = savedData.Data[i].PlayerSpeed;
                _dataExp[i] = savedData.Data[i].CurrentExperiment;
                _lineRenderer.SetPosition(i, Tools.ParseStringToVector3(savedData.Data[i].PlayerPosition));
                #region AI
                if (savedAIData != null)
                {
                    _AIpos[i] = new Vector3[_aiDataSize];
                    _AIrot[i] = new Quaternion[_aiDataSize];
                    for (int x = 0; x < _aiDataSize; x++)
                    {
                        _AIpos[i][x] = Tools.ParseStringToVector3(savedAIData.Data[i].ActiveAI[x].Position);
                        _AIrot[i][x] = Tools.ParseStringToQuaternion(savedAIData.Data[i].ActiveAI[x].Rotation);
                    }
                }
                #endregion
            }
            for(int i = 0; i < _expSize; i++)
            {
                _experiments[i] = experimentsData.Position[i];
                _experimentsResults[i] = experimentsData.Result[i];
                _experimentsType[i] = experimentsData.Type[i];
            }

            //Load road path file:
            if(savedData.PlannerFile != "")
            {
                PathPlanner.LoadDataFromFile(savedData.PlannerFile, savedData.isLightRandom);
            }

            //Set the vehicle position:
            _player.GetComponent<Rigidbody>().isKinematic = true;
            _currentIndex = 0;
            _player.position = _pos[_currentIndex];
            _player.rotation = _rot[_currentIndex];

            //Init form values:
            _sliderTime.maxValue = _dataSize - 1;
            _sliderTime.value = 0;
            _tick = savedData.Tick;
            _maxTime = _tick * _dataSize;
            _textTime.text = string.Format("0/{0}sec", _maxTime);
            _textFile.text = _list.SelectedItem;

            _controls.gameObject.SetActive(true);

            _onPause = false;


            //Start traffic lights + Init
            TrafficLightGroupController.LoadLightsFromFile(string.Format("{0}\\Saves\\{1}LIGHTS.xml", Application.dataPath, _list.SelectedItem.Replace("SAVE.xml", "")), savedData.isLightRandom);

            OnPlay = true;
            InvokeRepeating("PlaySavedSession", 1.0f, savedData.Tick);
        }
    }

    public void ShowPath()
    {
        if (_list.SelectedItem != null)
        {
            //Get positions
            SavedGame savedData = new SavedGame();
            XmlSerializer serializer = new XmlSerializer(typeof(SavedGame));
            using (StreamReader stream = new StreamReader(string.Format("{0}\\Saves\\{1}", Application.dataPath, _list.SelectedItem), System.Text.Encoding.GetEncoding("UTF-8")))
            {
                savedData = serializer.Deserialize(stream) as SavedGame;
                stream.Close();
            }

            //Set local arrays
            _pos = new Vector3[savedData.Data.Count];
            _lineRenderer.SetVertexCount(savedData.Data.Count);
            for (int i = 0; i < savedData.Data.Count; i++)
            {
                _lineRenderer.SetPosition(i, Tools.ParseStringToVector3(savedData.Data[i].PlayerPosition));
            }

            OnPlay = true;
        }
    }

    public void PlayPauseBTN()
    {
        _increment = 1;
        if(_onPause)
        {
            //Unset the pause
            _textPlayPause.text = "Pause";
            TrafficLightGroupController.PlayAllLights();
            _onPause = false;
        }
        else
        {
            //Set the pause:
            _textPlayPause.text = "Play";
            TrafficLightGroupController.PauseAllLights();
            _onPause = true;
        }
    }

    public void ExitForm()
    {
        CancelInvoke("PlaySavedSession");
        AIcontroller.enabled = true;
        #region AI
        //Destroy AI objects
        if(_isAI)
        {
            for (int cnt = 0; cnt < _AItr.Length; cnt++ )
            {
                GameObject.Destroy(_AItr[cnt].gameObject);
            }
            _AIpos = null;
            _AIrot = null;
            _AItr = null;
        }
        #endregion
        _lineRenderer.SetVertexCount(0);
        _controls.gameObject.SetActive(false);
        OnPlay = false;
    }

    public void SwitchCamera()
    {
        FrontCamera.SetActive(!FrontCamera.activeInHierarchy);
        TopCamera.SetActive(!TopCamera.activeInHierarchy);
        PlayerModel.SetActive(TopCamera.activeInHierarchy);
    }
    public void StopPlaying()
    {
        _increment = 1;
        _onPause = true;

        _currentIndex = 0;
        _textTime.text = string.Format("0/{0}sec", _maxTime);
        _textPlayPause.text = "Play";
        _sliderTime.value = 0;
    }

    public void SpeedX2()
    {
        if (_increment != 2)
            _increment = 2;
        else
            _increment = 1;
    }

    public void SpeedX4()
    {
        if (_increment != 4)
            _increment = 4;
        else
            _increment = 1;
    }

    public void OnSliderTimeValueChanged()
    {
        _currentIndex = (int)_sliderTime.value;
        _textTime.text = string.Format("{0}/{1}sec", _currentIndex * _tick, _maxTime);

        //Update infos:
        UpdateInfos();

        _player.position = _pos[_currentIndex];
        _player.rotation = _rot[_currentIndex];

        #region Lights
        for (int cnt = 0; cnt < ObjectManager.TrafficLightsLength; cnt++ )
        {
            ObjectManager.TrafficLights[cnt].SetLightsStatus(0, _lights[_currentIndex].State01[cnt]);
            ObjectManager.TrafficLights[cnt].SetLightsStatus(1, _lights[_currentIndex].State02[cnt]);
            ObjectManager.TrafficLights[cnt].SetLightsStatus(2, _lights[_currentIndex].State03[cnt]);
            ObjectManager.TrafficLights[cnt].SetLightsStatus(3, _lights[_currentIndex].State04[cnt]);
        }
        #endregion

        #region AI
            if (_isAI)
            {
                for (int cnt = 0; cnt < _aiDataSize; cnt++)
                {
                    _AItr[cnt].position = _AIpos[_currentIndex][cnt];
                    _AItr[cnt].rotation = _AIrot[_currentIndex][cnt];
                }
            }
        #endregion
    }

    private void UpdateInfos()
    {
        _textPosition.text = string.Format("Position: {0}", _pos[_currentIndex].ToString());
        _textRotation.text = string.Format("Rotation: {0}", _rot[_currentIndex].eulerAngles.ToString());
        _textSpeed.text = string.Format("Vitesse: {0}", _speed[_currentIndex].ToString());

        if (_experiments != null)
        {
            //Check if experiment in progress
            if (_dataExp[_currentIndex] != "null")
            {
                if (_dataExp[_currentIndex] != _currentExperiment)
                {
                    //Looking for the correct position:
                    for (int cnt = 0; cnt < _expSize; cnt++)
                    {
                        if (_dataExp[_currentIndex] == _experiments[cnt])
                        {
                            _currentExperiment = _dataExp[_currentIndex];
                            _textExpType.text = string.Format("Type de tâche: {0}", _experimentsType[cnt].ToString());
                            _textExpResult.text = string.Format("Résultat: {0}", _experimentsResults[cnt]);
                        }
                    }
                }
            }
            else
            {
                //Reset text:
                _currentExperiment = "";
                _textExpType.text = "";
                _textExpResult.text = "";
            }
        }
    }

    private void PlaySavedSession()
    {
        if (_onPause)
            return;
        if (_currentIndex < _pos.Length)
        {
            _sliderTime.value = _currentIndex;
            _currentIndex = _currentIndex + _increment;
        }
    }

    void Update()
    {
        if(OnPlay)
        {
            if (_currentIndex >= _pos.Length)
            {
                _player.GetComponent<Rigidbody>().isKinematic = false;
                OnPlay = false;
                CancelInvoke("PlaySavedSession");
            }
        }
    }
}
