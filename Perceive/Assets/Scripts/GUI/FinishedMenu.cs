﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class FinishedMenu : MonoBehaviour
{
    private Text _sessionTime;
    private Text _saveName;
    private Text _simulationType;

    private Transform _thisTransform;


    void Awake()
    {
        _thisTransform = transform;

        _sessionTime = _thisTransform.FindChild("TextSessionTime").GetComponent<Text>();
        _saveName = _thisTransform.FindChild("TextSaveName").GetComponent<Text>();
        _simulationType = _thisTransform.FindChild("TextSessionType").GetComponent<Text>();

    }

    public void Show()
    {
        _thisTransform.gameObject.SetActive(true);
    }
    public void Hide()
    {
        _thisTransform.gameObject.SetActive(false);
    }

    public void InitFinishedPanel(float sessionTime, string saveName)
    {
        _saveName.text = string.Format("Fichier de sauvegarde: {0}", saveName);
        _sessionTime.text = string.Format("Temps de session: {0} secondes", sessionTime);
        _simulationType.text = string.Format("Type de session:\r\nUtilisation du rail {0}\r\nFichier de chemin {1}\r\nFeux aléatoires {2}", ObjectManager.GetOptions().isRail, ObjectManager.GetOptions().PlannedPathFile, ObjectManager.GetOptions().isLightsRandom);
    }


    public void OnBTNClose()
    {
        Hide();
    }
}
