﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public delegate void ListboxItemChanged(object sender, EventArgs e);
public class ListBox : MonoBehaviour
{
    public event ListboxItemChanged ItemChanged;
    protected virtual void OnItemChanged(EventArgs e)
    {
        if (ItemChanged != null)
            ItemChanged(this, e);
    }


    private RectTransform _controls;
    private Transform _thisTransform;

    public float XPadding = 2.0f;
    public float YPadding = 2.0f;
    public float InterPadding = 1.0f;
    public float ItemHeight = 30.0f;

    public Color HighlightedColor;
    public Color NormalColor;
    public Color ClickedColor;

    private bool _clicked = false;
    private Image _lastClicked;



    #region Properties
    private string[] _items;
    public string[] Items
    {
        get
        {
            return _items;
        }
        set
        {
            _items = value;
        }
    }

    private int _selectedItemIndex = 0;
    public int SelectedItemIndex
    {
        get
        {
            return _selectedItemIndex;
        }
        set
        {
            _selectedItemIndex = value;
        }
    }

    private string _selectedItem;
    public string SelectedItem
    {
        get
        {
            return _selectedItem;
        }
        set
        {
            _selectedItem = value;
        }
    }
    #endregion


    void Awake()
    {
        _thisTransform = transform;
        _controls = _thisTransform.FindChild("Controls").GetComponent<RectTransform>();
    }

    public void InitListBox(string[] items, int defaultSelected = 0)
    {
        if (items == null)
            return;

        _items = new string[items.Length];
        items.CopyTo(_items, 0);



        //Cleaning the existing buttons:
        for (int cnt = 0; cnt < _controls.childCount; cnt++)
        {
            GameObject.Destroy(_controls.GetChild(cnt).gameObject);
        }


        //Draw the list of items
        float size = 2 * YPadding + _items.Length * (ItemHeight + InterPadding) - InterPadding;
        //_controls.localPosition = new Vector3(0f, 0f, 0);
        if (size > _controls.GetHeight())
            RectTransformExtensions.SetHeight(_controls, size);
        else
            size = _controls.GetHeight();

        //Create the buttons:
        int id = 0;
        foreach (string str in _items)
        {
            GameObject item = (GameObject)GameObject.Instantiate(Resources.Load("GUI/ListBoxItem"));
            item.transform.SetParent(_controls.transform, false);
            item.name = string.Format("item_{0}", id);
            item.transform.FindChild("Text").GetComponent<Text>().text = str; //Set the selected index
            item.GetComponent<RectTransform>().anchoredPosition = new Vector3(-XPadding, (size / 2f - 15 - YPadding) - (25 + InterPadding) * id, 0);
            RectTransformExtensions.SetHeight(item.GetComponent<RectTransform>(), 25);
            
            #region Event
            EventTrigger trigger = item.AddComponent<EventTrigger>();
            EventTrigger.Entry entry = new EventTrigger.Entry();
            entry.eventID = EventTriggerType.PointerEnter;
            entry.callback.AddListener((eventData) => { OnMouseEnter(item.GetComponent<Image>()); });
            trigger.triggers = new System.Collections.Generic.List<EventTrigger.Entry>();
            trigger.triggers.Add(entry);

            entry = new EventTrigger.Entry();
            entry.eventID = EventTriggerType.PointerExit;
            entry.callback.AddListener((eventData) => { OnMouseExit(item.GetComponent<Image>()); });
            trigger.triggers.Add(entry);

            entry = new EventTrigger.Entry();
            entry.eventID = EventTriggerType.PointerClick;
            entry.callback.AddListener((eventData) => { OnMouseClick(item.GetComponent<Image>()); });
            trigger.triggers.Add(entry);

            #endregion
            id++;
        }
    }

    public void OnMouseClick(Image e)
    {
        if (_lastClicked != e)
        {
            e.color = ClickedColor;
            if(_lastClicked != null)
                _lastClicked.color = NormalColor;
            _clicked = true;
            _lastClicked = e;

            //Set selected indexes
            string text = e.transform.GetChild(0).GetComponent<Text>().text;
            _selectedItem = text;
            for(int cnt = 0; cnt < _items.Length; cnt++)
            {
                if (_items[cnt] == text)
                    _selectedItemIndex = cnt;
            }
            OnItemChanged(new EventArgs());
        }
    }

    public void OnMouseEnter(Image e)
    {
        if(e != _lastClicked)
            e.color = HighlightedColor;
    }
    public void OnMouseExit(Image e)
    {
        if(e != _lastClicked)
            e.color = NormalColor;
    }

}
