﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using System.IO;
using System;

public delegate void NewSelection(object sender, bool isRight, PlannerPartInfo infos);
public class PathPlanner : MonoBehaviour
{

    public event NewSelection NewSelectionEvent;
    protected virtual void OnNewSelection(bool isRight, PlannerPartInfo infos)
    {
        //NewSelectionEvent(this, isRight, infos);
    }

    public static bool IsEditingExperiments { get; set; }

    public ExperimentController ExpController;
    private Camera _exp01ExampleCamera;
    public Transform Experiment01Example;


    //Cache Objects
    private GameObject[] _meshesObjects;
    private GameObject[] _plannerObjects;
    private Transform[] _parts;
    private PlannerPart[] _partsScript;
    private PlannerPart[] _intersectionParts;
    public PlannerPart[] PartsScript { get { return _partsScript; } }
    private int _size = 0;
    private Ray _ray;

    public Camera Cam;
    private RaycastHit _hit;
    private Info _infoDelegate;

    private Material _allowedMaterial;
    private Material _blockedMaterial;
    private Material _blockedEventMaterial;
    private Material _outlinedMaterial;
    private Material _selectedMaterial;

    private PlannerPartInfo _infos;
    private PlannerPart _selectedPart;

    private Transform _targetTr;
    private Material _targetMaterial;

    private Transform _experimentParent;
    private bool _rot = false;
    private bool _onDrawExperiments = false;
    private bool _dragExperimentObject = false;
    private Transform _draggedTransform;
    public bool OnDrawExperiments
    {
        get
        {
            return _onDrawExperiments;
        }
        set
        {
            _onDrawExperiments = value;
            IsEditingExperiments = value;
            if (value)
                _exp01ExampleCamera.enabled = true;
            else
                _exp01ExampleCamera.enabled = false;
            if (!value && Experiment01Example != null)
                Experiment01Example.position = new Vector3(0, 0, 0);
        }
    }

    void Awake()
    {
        _meshesObjects = GameObject.FindGameObjectsWithTag("RoadMesh");
        _plannerObjects = GameObject.FindGameObjectsWithTag("RoadPlanner");
        List<Transform> tmp = new List<Transform>();
        List<PlannerPart> tmpScript = new List<PlannerPart>();
        int count = 0;
        _exp01ExampleCamera = Experiment01Example.FindChild("Camera").GetComponent<Camera>();

        ObjectManager manager = GameObject.FindGameObjectWithTag("ObjectManager").GetComponent<ObjectManager>();
        _allowedMaterial = manager.PathPlannerAllowedMaterial;
        _blockedMaterial = manager.PathPlannerBlockedMaterial;
        _blockedEventMaterial = manager.PathPlannerEventMaterial;
        _outlinedMaterial = manager.PathPlannerOutlinedMaterial;
        _selectedMaterial = manager.PathPlannerSelectedMaterial;

        _onDrawExperiments = false;

        foreach (GameObject go in _plannerObjects)
        {
            foreach (Transform tr in go.transform)
            {
                tmp.Add(tr);
                tmpScript.Add(tr.GetComponent<PlannerPart>());
                tmpScript[tmpScript.Count - 1].SetMaterials(_allowedMaterial, _blockedMaterial, _blockedEventMaterial, _outlinedMaterial, _selectedMaterial);
                tmpScript[tmpScript.Count - 1].State = Enums.PlannerState.Blocked;
                if (tmpScript[tmpScript.Count - 1].Type == Enums.PlannerPart.Intersection || tmpScript[tmpScript.Count - 1].Type == Enums.PlannerPart.SimpleCrossing)
                    count++;
            }
        }
        _size = tmp.Count;
        _parts = new Transform[_size];
        tmp.CopyTo(_parts, 0);

        _partsScript = new PlannerPart[_size];
        tmpScript.CopyTo(_partsScript, 0);

        _intersectionParts = new PlannerPart[count];
        count = 0;
        foreach (PlannerPart part in _partsScript)
        {
            if (part.Type == Enums.PlannerPart.Intersection || part.Type == Enums.PlannerPart.SimpleCrossing)
            {
                _intersectionParts[count] = part;
                count++;
            }
        }

        ShowRoadMesh = true;

        _experimentParent = GameObject.Find("Experiments").transform;

    }

    // Update is called once per frame
    void Update()
    {
        #region Path
        if (!_onDrawExperiments)
        {
            if (Input.GetMouseButtonDown(1))
            {
                _ray = Cam.ScreenPointToRay(Input.mousePosition);
                #region ALL
                if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
                {
                    if (Physics.Raycast(_ray, out _hit))
                    {
                        _targetTr = _hit.transform;
                        if (_targetTr.parent != null && _targetTr.parent.tag == "RoadPlanner")
                        {
                            for (int cnt = 0; cnt < _size; cnt++)
                            {
                                if (_parts[cnt] == _targetTr)
                                {
                                    //Found

                                    //Brose all GO of the selected road
                                    _partsScript[cnt].SwitchState();
                                    Enums.PlannerState current = _partsScript[cnt].State;
                                    foreach (Transform tr in _parts[cnt].parent)
                                    {
                                        tr.GetComponent<PlannerPart>().State = current;
                                        CheckIntersections(tr.GetComponent<PlannerPart>());
                                    }
                                    ExpPath.OnSelected -= _infoDelegate;
                                    _infoDelegate = null;
                                    CheckIntersectionsGlobal();
                                    OnNewSelection(false, _partsScript[cnt].GetInfo()); //Event
                                    return;
                                }
                            }
                        }
                    }
                }
                #endregion
                #region SINGLE
                else
                {
                    if (Physics.Raycast(_ray, out _hit))
                    {
                        _targetTr = _hit.transform;
                        if (_targetTr.parent != null && _targetTr.parent.tag == "RoadPlanner")
                        {
                            for (int cnt = 0; cnt < _size; cnt++)
                            {
                                if (_parts[cnt] == _targetTr)
                                {
                                    ExpPath.OnSelected -= _infoDelegate;
                                    _infoDelegate = null;
                                    _partsScript[cnt].SwitchState();
                                    CheckIntersections(_partsScript[cnt]);
                                    CheckIntersectionsGlobal();
                                    OnNewSelection(false, _partsScript[cnt].GetInfo()); //Event
                                    return;
                                }
                            }
                        }
                    }
                }
                #endregion
            }
            #region Selection
            else if (Input.GetMouseButtonDown(0))
            {
                _ray = Cam.ScreenPointToRay(Input.mousePosition);

                if (Physics.Raycast(_ray, out _hit))
                {
                    _targetTr = _hit.transform;
                    if (_selectedPart != null && _targetTr == _selectedPart.transform)
                    {
                        ExpPath.OnSelected -= _infoDelegate;
                        _selectedPart.isSelected = false;
                        _selectedPart = null;
                        return;
                    }
                    if (_targetTr.parent != null && _targetTr.parent.tag == "RoadPlanner")
                    {
                        for (int cnt = 0; cnt < _size; cnt++)
                        {
                            if (_parts[cnt] == _targetTr)
                            {
                                if (_selectedPart != null)
                                {
                                    _selectedPart.isSelected = false;
                                }
                                _selectedPart = _partsScript[cnt];
                                _selectedPart.isSelected = true;
                                _selectedPart.SetSelectedMaterial();
                                if (_infoDelegate != null)
                                    ExpPath.OnSelected -= _infoDelegate;
                                _infos = _partsScript[cnt].GetInfo();
                                _infoDelegate = new Info(_partsScript[cnt].GetInfo);
                                ExpPath.OnSelected += _infoDelegate;
                                OnNewSelection(true, _partsScript[cnt].GetInfo()); //Event
                                return;
                            }
                        }
                    }
                }
            }

            #endregion
        }
        #endregion
        #region Experiment
        //Showing target and allow rotation:
        if (_onDrawExperiments)
        {
            if (Input.GetKeyDown(KeyCode.LeftShift) || Input.GetKeyDown(KeyCode.RightShift))
                _exp01ExampleCamera.transform.localEulerAngles = new Vector3(0, _exp01ExampleCamera.transform.localEulerAngles.y + 180, 0);
            _ray = Cam.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(_ray, out _hit))
            {
                if (_hit.transform.parent != null && (_hit.transform.parent.tag == "RoadPlanner" || _hit.transform.parent.name == "Experiments"))
                {
                    //Showing example:
                    Experiment01Example.position = (_hit.point + new Vector3(0, 1, 0));
                    if (Input.GetKeyDown(KeyCode.LeftControl) || Input.GetKeyDown(KeyCode.RightControl))
                    {
                        if (!_rot)
                        {
                            Experiment01Example.localEulerAngles = new Vector3(0, 90, 0);
                            _rot = true;
                        }
                        else
                        {
                            Experiment01Example.localEulerAngles = new Vector3(0, 0, 0);
                            _rot = false;
                        }
                    }


                    //Drag
                    if (_dragExperimentObject)
                    {
                        if (Input.GetMouseButton(0))
                        {
                            _draggedTransform.position = Experiment01Example.transform.position;
                            _draggedTransform.rotation = Experiment01Example.transform.rotation;
                        }
                        else
                        {
                            _dragExperimentObject = false;
                            //Disable collider:
                            _draggedTransform.GetComponent<BoxCollider>().enabled = true;
                        }
                    }

                    if (Input.GetMouseButtonDown(1))
                    {
                        GameObject exp = (GameObject)GameObject.Instantiate(Resources.Load("experiment01"));
                        exp.transform.SetParent(_experimentParent, false);
                        exp.transform.position = _hit.point + new Vector3(0, 1, 0);
                        if (_rot)
                            exp.transform.localEulerAngles = new Vector3(0, 90, 0);
                        else
                            exp.transform.localEulerAngles = new Vector3(0, 0, 0);
                    }
                    if (Input.GetMouseButtonDown(0)) //Move the hoovering part
                    {
                        if (_hit.transform.GetComponent<Experiment01>() != null) //If the mouse is over an experiment control
                        {
                            _dragExperimentObject = true;
                            _draggedTransform = _hit.transform;
                            _draggedTransform.position = Experiment01Example.transform.position;
                            _draggedTransform.rotation = Experiment01Example.transform.rotation;
                            //Disable collider:
                            _draggedTransform.GetComponent<BoxCollider>().enabled = false;
                        }
                    }
                    if (Input.GetMouseButtonDown(2))
                    {
                        if (_hit.transform.GetComponent<Experiment01>() != null) //If the mouse is over an experiment control
                        {
                            GameObject.Destroy(_hit.transform.gameObject);
                        }
                    }
                }
                else if (_dragExperimentObject)
                {
                    _dragExperimentObject = false;
                    //Disable collider:
                    _draggedTransform.GetComponent<BoxCollider>().enabled = true;
                }
            }
        }
        #endregion
    }

    public void UpdateBlockedType(Enums.PlannerBlockedType newType)
    {
        _infos.Instance.BlockedType = newType;
        _infos.Instance.PopRoadBlock();
    }

    public void UpdateLightController(float delay, float rtog, float ytor, string firstGreen)
    {
        _infos.Instance.LightController.Delay = delay;
        _infos.Instance.LightController.RedToGreenDelay = rtog;
        _infos.Instance.LightController.YellowDelay = ytor;
        _infos.Instance.LightController.DefaultGreen = firstGreen;
    }

    public void UpdateStartEndValues(bool start, bool end, int side)
    {
        _infos.Instance.isStart = start;
        _infos.Instance.isEnd = end;
        _infos.Instance.Side = side;
    }

    private void CheckIntersections(PlannerPart part)
    {
            if (part.State == Enums.PlannerState.Allowed)
            {
                for (int cnt = 0; cnt < 4; cnt++)
                {
                    if (part.Adjacents.Length > cnt && part.Adjacents[cnt] != null && part.Adjacents[cnt].State == Enums.PlannerState.Blocked)
                    {
                        part.Adjacents[cnt].State = Enums.PlannerState.BlockedEvent;
                        part.Adjacents[cnt].BlockedType = Enums.PlannerBlockedType.RoadBlockConcrete; // Default block
                        part.Adjacents[cnt].PopRoadBlock(); //Move blockers
                    }
                }
            }
            if(part.State == Enums.PlannerState.Blocked)
            {
                for (int cnt = 0; cnt < 4; cnt++)
                {
                    if (part.Adjacents.Length > cnt && part.Adjacents[cnt] != null && part.Adjacents[cnt].State == Enums.PlannerState.BlockedEvent)
                    {
                        part.Adjacents[cnt].State = Enums.PlannerState.Blocked;
                        part.Adjacents[cnt].BlockedType = Enums.PlannerBlockedType.None; // Default block
                        part.Adjacents[cnt].PopRoadBlock(); //Move blockers
                    }
                }
            }

            part.PopRoadBlock();
    }
    private void CheckIntersectionsGlobal()
    {
        foreach (PlannerPart part in _intersectionParts)
        {
            if (part.State == Enums.PlannerState.Allowed)
            {
                for (int cnt = 0; cnt < 4; cnt++)
                {
                    if (part.Adjacents.Length > cnt && part.Adjacents[cnt] != null && part.Adjacents[cnt].State == Enums.PlannerState.Blocked)
                    {
                        part.Adjacents[cnt].State = Enums.PlannerState.BlockedEvent;
                        part.Adjacents[cnt].BlockedType = Enums.PlannerBlockedType.RoadBlockConcrete; // Default block
                        part.Adjacents[cnt].PopRoadBlock(); //Move blockers
                    }
                }
            }
        }
    }

    private bool _roadmesh;
    public bool ShowRoadMesh
    {
        get
        {
            return _roadmesh;
        }
        set
        {
            /*foreach (GameObject obj in _meshesObjects)
            {
                obj.SetActive(value);
            }*/
            foreach (GameObject obj in _plannerObjects)
            {
                obj.SetActive(!value);
            }
            _roadmesh = value;
        }
    }

    public void Save(string filename)
    {
        RoadPlannerXML _data = new RoadPlannerXML();
        //TrafficLightsXML _trafficData = new TrafficLightsXML();
        List<PlannerPart> usefullParts = new List<PlannerPart>();
        List<TrafficLightGroupController> lights = new List<TrafficLightGroupController>();
        int allowedCount = 0;
        int adjacentCount = 0;
        int trafficCount = 0;

        #region ItemsCounting
        bool isStart = false;
        bool isEnd = false;
        foreach (PlannerPart part in _partsScript)
        {
            if (part.isStart)
                isStart = true;
            if (part.isEnd)
                isEnd = true;
            if (part.State == Enums.PlannerState.BlockedEvent)
            {
                usefullParts.Add(part);
                adjacentCount++;
            }
            if (part.State == Enums.PlannerState.Allowed)
            {
                usefullParts.Add(part);
                allowedCount++;
            }
            if (part.LightController != null)
            {
                trafficCount++;
                lights.Add(part.LightController);
            }
        }

        if(!isStart || !isEnd)
        {
            MessageBox.ShowMessageBox("Impossible de sauvegarder le trajet actuel: La portion de départ et/ou celle d'arrivée n'est pas renseignée !");
            return;
        }

        _data.AllowedPoints = new string[allowedCount];
        _data.AdjacentPoints = new RoadPlannerAdjacentXML[adjacentCount];
        #endregion

        #region SetData
        allowedCount = 0;
        adjacentCount = 0;
        foreach (PlannerPart part in usefullParts)
        {
            if (part.State == Enums.PlannerState.Allowed)
            {
                _data.AllowedPoints[allowedCount] = Tools.ParseVector3ToString(part.transform.position);
                allowedCount++;
                if (part.isStart)
                {
                    _data.StartPosition = Tools.ParseVector3ToString(part.transform.position);
                    _data.Side = part.Side;
                }
                if (part.isEnd)
                    _data.EndPosition = Tools.ParseVector3ToString(part.transform.position);
            }
            if (part.State == Enums.PlannerState.BlockedEvent)
            {
                _data.AdjacentPoints[adjacentCount] = new RoadPlannerAdjacentXML();
                _data.AdjacentPoints[adjacentCount].BlockedType = part.BlockedType;
                _data.AdjacentPoints[adjacentCount].Position = Tools.ParseVector3ToString(part.transform.position);
                adjacentCount++;
            }
        }

        /*_trafficData.Position = new string[trafficCount];
        _trafficData.Delay = new float[trafficCount];
        _trafficData.RToG = new float[trafficCount];
        _trafficData.YToR = new float[trafficCount];
        _trafficData.DefaultGreen = new string[trafficCount];
        for (int cnt = 0; cnt < trafficCount; cnt++ )
        {
            _trafficData.Position[cnt] = Tools.ParseVector3ToString(lights[cnt].transform.position);
            _trafficData.Delay[cnt] = lights[cnt].Delay;
            _trafficData.RToG[cnt] = lights[cnt].RedToGreenDelay;
            _trafficData.YToR[cnt] = lights[cnt].YellowDelay;
            _trafficData.DefaultGreen[cnt] = lights[cnt].DefaultGreen;
        }*/
        #endregion



        if (!Directory.Exists(Application.dataPath + "\\RoadPath"))
            Directory.CreateDirectory(Application.dataPath + "\\RoadPath");

        XmlSerializer xml = new XmlSerializer(typeof(RoadPlannerXML));
        if (File.Exists(string.Format("{0}\\RoadPath\\{1}.xml", Application.dataPath, filename)))
            File.Delete(string.Format("{0}\\RoadPath\\{1}.xml", Application.dataPath, filename));
        using (StreamWriter stream = new StreamWriter(string.Format("{0}\\RoadPath\\{1}.xml", Application.dataPath, filename), false, System.Text.Encoding.GetEncoding("UTF-8")))
        {
            xml.Serialize(stream, _data);
            stream.Close();
        }

        //xml = new XmlSerializer(typeof(TrafficLightsXML));
        if (File.Exists(string.Format("{0}\\RoadPath\\{1}_LIGHTS.xml", Application.dataPath, filename)))
            File.Delete(string.Format("{0}\\RoadPath\\{1}_LIGHTS.xml", Application.dataPath, filename));
        TrafficLightGroupController.SaveLightsToFile(string.Format("{0}\\RoadPath\\{1}_LIGHTS.xml", Application.dataPath, filename));
        /*using (StreamWriter stream = new StreamWriter(string.Format("{0}\\RoadPath\\{1}_LIGHTS.xml", Application.dataPath, filename), false, System.Text.Encoding.GetEncoding("UTF-8")))
        {
            xml.Serialize(stream, _trafficData);
            stream.Close();
        }*/

        ExperimentXML experimentsObject = new ExperimentXML();
        int size = _experimentParent.childCount;
        experimentsObject.Name = new string[size];
        experimentsObject.Position = new string[size];
        experimentsObject.Rotation = new string[size];
        experimentsObject.Scale = new string[size];
        experimentsObject.Size = size;
        experimentsObject.Type = new Enums.ExperimentType[size];
        //Experiments data
        Transform trTmp;
        for(int cnt = 0; cnt < size; cnt++)
        {
            trTmp = _experimentParent.GetChild(cnt);
            experimentsObject.Name[cnt] = trTmp.name;
            experimentsObject.Position[cnt] = Tools.ParseVector3ToString(trTmp.position);
            experimentsObject.Rotation[cnt] = Tools.ParseQuaternionToString(trTmp.rotation);
            experimentsObject.Scale[cnt] = Tools.ParseVector3ToString(trTmp.localScale);

            //Switching all experiments:
            if(trTmp.GetComponent<Experiment01>() != null)
                experimentsObject.Type[cnt] = trTmp.GetComponent<Experiment01>().IExpInstance.Type;
        }
        ExperimentController.SaveExperiments(string.Format("{0}\\RoadPath\\{1}_EXPERIMENTS.xml", Application.dataPath, filename), experimentsObject);



        //Save the file as the default file
        ObjectManager.GetOptions().PlannedPathFile = string.Format("{0}\\RoadPath\\{1}.xml", Application.dataPath, filename);
        ObjectManager.GetOptions().PlannedPathExperimentFile = string.Format("{0}\\RoadPath\\{1}_EXPERIMENTS.xml", Application.dataPath, filename);
        ObjectManager.GetOptions().Save();
    }

    public void Load(string file)
    {
        Debug.Log("Loading " + file);
        if (File.Exists(file))
        {
            RoadPlannerXML roadPlannerObject = new RoadPlannerXML();
            XmlSerializer serializer = new XmlSerializer(typeof(RoadPlannerXML));
            using (StreamReader stream = new StreamReader(file, System.Text.Encoding.GetEncoding("UTF-8")))
            {
                roadPlannerObject = serializer.Deserialize(stream) as RoadPlannerXML;
                stream.Close();
            }
            BlockAll();

            string startPos = roadPlannerObject.StartPosition;
            bool startOK = false;
            int side = roadPlannerObject.Side;
            string endPos = roadPlannerObject.EndPosition;
            bool endOK = false;

            for (int cnt = 0; cnt < roadPlannerObject.AllowedPoints.Length; cnt++)
            {
                for (int i = 0; i < _parts.Length; i++)
                {
                    if (Tools.ParseVector3ToString(_parts[i].position) == roadPlannerObject.AllowedPoints[cnt])
                    {
                        _partsScript[i].State = Enums.PlannerState.Allowed;

                        //Check if start or end
                        if(!startOK)
                        {
                            if (Tools.ParseVector3ToString(_parts[i].position) == startPos)
                            {
                                _partsScript[i].isStart = true;
                                _partsScript[i].Side = side;
                                startOK = true;
                            }
                        }
                        if (!endOK)
                        {
                            if (Tools.ParseVector3ToString(_parts[i].position) == endPos)
                            {
                                _partsScript[i].isEnd = true;
                                endOK = true;
                            }
                        }
                        break;
                    }
                }
            }

            try
            {
                //Adjacent points
                for (int x = 0; x < roadPlannerObject.AdjacentPoints.Length; x++)
                {
                    for (int j = 0; j < _parts.Length; j++)
                    {
                        if (Tools.ParseVector3ToString(_parts[j].position) == roadPlannerObject.AdjacentPoints[x].Position)
                        {
                            _partsScript[j].State = Enums.PlannerState.BlockedEvent;
                            _partsScript[j].BlockedType = roadPlannerObject.AdjacentPoints[x].BlockedType;
                            break;
                        }
                    }
                }
            }
            catch(Exception e)
            {}
        }

        #region Traffic lights
        //Load lights
        string lightsFileName = string.Format("{0}_LIGHTS.xml", file.Replace(".xml", "")); //Get the correct ligths XML file
        if (File.Exists(lightsFileName)) //If the light xml file exists
        {
            /*TrafficLightsXML trafficLightsObject = new TrafficLightsXML();
            XmlSerializer serializer = new XmlSerializer(typeof(TrafficLightsXML));
            using (StreamReader stream = new StreamReader(lightsFileName, System.Text.Encoding.GetEncoding("UTF-8")))
            {
                trafficLightsObject = serializer.Deserialize(stream) as TrafficLightsXML;
                stream.Close();
            }
            if (!ObjectManager.GetOptions().isLightsRandom)
            {
                TrafficLightGroupController light;

                //Get only the intersections
                PlannerPart[] intersectionParts = new PlannerPart[_parts.Length];
                int cnt = 0;
                foreach (PlannerPart part in _partsScript)
                {
                    if (part.Type == Enums.PlannerPart.Intersection || part.Type == Enums.PlannerPart.SimpleCrossing)
                    {
                        intersectionParts[cnt] = part;
                        cnt++;
                    }
                }


                for (int x = 0; x < trafficLightsObject.Position.Length; x++)
                {
                    for (int j = 0; j < intersectionParts.Length; j++)
                    {
                        if (intersectionParts[j] != null)
                        {
                            if (Tools.ParseVector3ToString(intersectionParts[j].transform.position) == trafficLightsObject.Position[x])
                            {
                                light = intersectionParts[j].LightController;
                                light.isRandom = false;
                                light.Delay = trafficLightsObject.Delay[x];
                                light.RedToGreenDelay = trafficLightsObject.RToG[x];
                                light.YellowDelay = trafficLightsObject.YToR[x];
                                light.DefaultGreen = trafficLightsObject.DefaultGreen[x];
                                break;
                            }
                        }
                    }
                }
            }*/
            TrafficLightGroupController.LoadLightsFromFile(lightsFileName, ObjectManager.GetOptions().isLightsRandom);
        }
        #endregion

        #region Experiments
        //Load Experiment objects:
        string expFileName = string.Format("{0}_EXPERIMENTS.xml", file.Replace(".xml", ""));
        if (File.Exists(expFileName))
            ExpController.LoadExperiments(expFileName);
        #endregion
    }

    private void BlockAll()
    {
        foreach(PlannerPart part in _partsScript)
        {
            part.State = Enums.PlannerState.Blocked;
        }
    }



    //STATIC
    public static void LoadDataFromFile(string path, bool isLightRandom)
    {
        if (File.Exists(path))
        {
            RoadPlannerXML roadPlannerObject = new RoadPlannerXML();
            XmlSerializer serializer = new XmlSerializer(typeof (RoadPlannerXML));
            using (StreamReader stream = new StreamReader(path, System.Text.Encoding.GetEncoding("UTF-8")))
            {
                roadPlannerObject = serializer.Deserialize(stream) as RoadPlannerXML;
                stream.Close();
            }
            /*path = string.Format("{0}_LIGHTS.xml", path.Replace(".xml", ""));
        TrafficLightsXML trafficLightsObject = new TrafficLightsXML();
        serializer = new XmlSerializer(typeof(TrafficLightsXML));
        using (StreamReader stream = new StreamReader(path, System.Text.Encoding.GetEncoding("UTF-8")))
        {
            trafficLightsObject = serializer.Deserialize(stream) as TrafficLightsXML;
            stream.Close();
        }*/

            //Set arrays
            GameObject[] plannerObjects = GameObject.FindGameObjectsWithTag("RoadPlanner");
            List<PlannerPart> tmpScript = new List<PlannerPart>();
            int count = 0;
            foreach (GameObject go in plannerObjects)
            {
                foreach (Transform tr in go.transform)
                {
                    tmpScript.Add(tr.GetComponent<PlannerPart>());
                    if (tmpScript[tmpScript.Count - 1].Type == Enums.PlannerPart.Intersection ||
                        tmpScript[tmpScript.Count - 1].Type == Enums.PlannerPart.SimpleCrossing)
                        count++;
                }
            }
            int size = tmpScript.Count;

            PlannerPart[] partsScript = new PlannerPart[size];
            tmpScript.CopyTo(partsScript, 0);

            PlannerPart[] intersectionParts = new PlannerPart[count];
            count = 0;
            foreach (PlannerPart part in partsScript)
            {
                if (part.Type == Enums.PlannerPart.Intersection || part.Type == Enums.PlannerPart.SimpleCrossing)
                {
                    intersectionParts[count] = part;
                    count++;
                }
            }

            //ShowRoadMesh = true;

            //BlockAll();

            try
            {
                //Set Road blocks
                for (int x = 0; x < roadPlannerObject.AdjacentPoints.Length; x++) //Looping all adjacent objects
                {
                    for (int j = 0; j < intersectionParts.Length; j++) //Looking for the right object in scene
                    {
                        if (intersectionParts[j].BlockedType != Enums.PlannerBlockedType.None)
                        {
                            for (int i = 0; i < intersectionParts[j].Adjacents.Length; i++)
                            {
                                if (Tools.ParseVector3ToString(intersectionParts[j].Adjacents[i].transform.position) ==
                                    roadPlannerObject.AdjacentPoints[x].Position) //Correct part
                                {
                                    //Pop the obstacle
                                    switch (roadPlannerObject.AdjacentPoints[x].BlockedType)
                                    {
                                        case Enums.PlannerBlockedType.RoadBlockBarricade:
                                            intersectionParts[j].Adjacents[i].PopRoadBlock();
                                            break;
                                        case Enums.PlannerBlockedType.RoadBlockConcrete:
                                            intersectionParts[j].Adjacents[i].PopRoadBlock();
                                            break;
                                    }
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
            }

            #region Traffic Light

            TrafficLightGroupController.LoadLightsFromFile(string.Format("{0}_LIGHTS.xml", path.Replace(".xml", "")),
                isLightRandom);

            #endregion
        }
    }
}
