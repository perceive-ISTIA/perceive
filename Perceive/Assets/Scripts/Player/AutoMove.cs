﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using System.IO;
using System.Linq;
using System;

public delegate void AutoMoveHandler(object sender, EventArgs e);
public class AutoMove : MonoBehaviour
{
    public event AutoMoveHandler AutoMoveFinished;
    protected virtual void OnAutoMoveFinished(EventArgs e)
    {
        if (AutoMoveFinished != null)
            AutoMoveFinished(this, e);
    }

    #region GPS
    public AudioClip _audioTurnLeft;
    public AudioClip _audioTurnRight;
    public AudioClip _audioTurnFront;
    #endregion

    #region Materials
    private Material _highlightedMat;
    private Material _allowedMat;
    #endregion
    public PathPlanner Planner;

    private Transform _player;
    private PlannerPart[] _pathParts;
    private Vector3[] _waypointsTr;
    private Waypoint[] _waypoints;
    private bool _onMove = false;

    private int _currentWaypoint = 0;

    private List<float> _accelerationCurve;
    private float _currentSpeed = 0.0f;
    public float CurrentSpeed
    {
        get
        {
            return _currentSpeed;
        }
    }
    private float _accelerationTick;
    private float _pressedTime = 0f;
    private float _decelerationTick;

    //Bezier
    private bool _isBezier = false;
    private int _bezierIndex = 0;
    private Vector3[] _bezierPoints;

    private bool _freeze = false;

    public float MaxSpeed
    {
        get
        {
            return ObjectManager.GetOptions().PlayerMaxSpeed;
        }
    }

    void Awake()
    {
        _player = GameObject.FindGameObjectWithTag("Player").transform;
        _accelerationTick = ObjectManager.GetOptions().PlayerAcceleration;
        _decelerationTick = ObjectManager.GetOptions().PlayerAcceleration;
        _accelerationCurve = Tools.CalculateSpeedCurve(ObjectManager.GetOptions().PlayerMaxSpeed);

        ObjectManager manager = GameObject.FindGameObjectWithTag("ObjectManager").GetComponent<ObjectManager>();
        _highlightedMat = manager.PathPlannerOutlinedMaterial;
        _allowedMat = manager.PathPlannerAllowedMaterial;
    }

    void Update()
    {
        if (_freeze)
            return;
        if (_onMove)
            Move();
    }

    public void FreezeMovement()
    {
        _freeze = true;
    }

    public void UnFreezeMovement()
    {
        _freeze = false;
    }

    public void Experiment01Move()
    {
        int count = (int)(Mathf.Abs(ObjectManager.GetOptions().Exp01Offset) * 100);
        for(int cnt = 0; cnt < count ; cnt++)
        {
            Move();
        }
    }
    public void StartAutoMove(string pathFile)
    {
        Planner.ShowRoadMesh = false;
        LoadDataFromFile(pathFile);
        Planner.ShowRoadMesh = true;


        _currentWaypoint = 0;

        //Set the pop position
        _player.position = _waypointsTr[_currentWaypoint];

        if ((_waypointsTr[_currentWaypoint] - _player.position).magnitude != 0f)
            _player.rotation = Quaternion.Slerp(Quaternion.LookRotation(_waypointsTr[_currentWaypoint] - _player.position), _player.rotation, Time.deltaTime * 2.0f);



        _onMove = true;
        _player.GetComponent<Rigidbody>().isKinematic = true;
    }

    int ind = 0;
    private void Visualize()
    {
        _pathParts[ind].GetComponent<Renderer>().material = _highlightedMat;
        if (ind != 0)
            _pathParts[ind - 1].GetComponent<Renderer>().material = _allowedMat;
        else
            _pathParts[_pathParts.Length - 1].GetComponent<Renderer>().material = _allowedMat;
        if (ind == _pathParts.Length - 1)
            ind = 0;
        else
            ind++;
    }

    private void Move()
    {
        if (_currentWaypoint < _waypointsTr.Length)
        {
            if (_waypoints[_currentWaypoint].Intersection != Enums.Intersection.None && !_isBezier)
            {
                if (_currentWaypoint + 1 < _waypoints.Length)
                {
                    for (int cnt = 0; cnt < _waypoints[_currentWaypoint].IntersectionPoints.Length; cnt++)
                    {
                        if (_waypoints[_currentWaypoint].IntersectionPoints[cnt] != null && _waypoints[_currentWaypoint].IntersectionPoints[cnt] == _waypoints[_currentWaypoint + 1].gameObject)
                        {
                            _bezierPoints = new Vector3[_waypoints[_currentWaypoint].IntersectBezierPoints[cnt].Length];
                            _waypoints[_currentWaypoint].IntersectBezierPoints[cnt].CopyTo(_bezierPoints, 0);
                            _bezierIndex = 0;
                            _isBezier = true;
                        }
                    }
                }
            }

            if (_isBezier)
            {
                if ((_bezierPoints[_bezierIndex] - _player.position).magnitude != 0f)
                    _player.rotation = Quaternion.Slerp(Quaternion.LookRotation(_bezierPoints[_bezierIndex] - _player.position), _player.rotation, Time.deltaTime * 2.0f);

                _currentSpeed = GetSpeed(Input.GetAxis("Vertical"));
                _player.position = Vector3.MoveTowards(_player.position, _bezierPoints[_bezierIndex], _currentSpeed);

                if (Vector3.Distance(_player.position, _bezierPoints[_bezierIndex]) < 2.0f)
                {
                    if (_bezierIndex + 1 < _bezierPoints.Length)
                        _bezierIndex++;
                    else
                    {
                        _isBezier = false;
                        _currentWaypoint++;
                        AudioGPS();
                    }
                }
            }

            else
            {
                if(_waypoints[_currentWaypoint].BezierPoints != null)
                {
                    _bezierPoints = new Vector3[_waypoints[_currentWaypoint].BezierPoints.Length];
                    _waypoints[_currentWaypoint].BezierPoints.CopyTo(_bezierPoints, 0);
                    _bezierIndex = 0;
                    _isBezier = true;
                    return;
                }
                //Set the rotation:
                if ((_waypointsTr[_currentWaypoint] - _player.position).magnitude != 0f)
                    _player.rotation = Quaternion.Slerp(Quaternion.LookRotation(_waypointsTr[_currentWaypoint] - _player.position), _player.rotation, Time.deltaTime * 2.0f);

                _currentSpeed = GetSpeed(Input.GetAxis("Vertical"));
                _player.position = Vector3.MoveTowards(_player.position, _waypointsTr[_currentWaypoint], _currentSpeed);

                if (Vector3.Distance(_player.position, _waypointsTr[_currentWaypoint]) < 2.0f)
                {
                    _currentWaypoint++;
                    AudioGPS();
                }
            }
        }
        else
        {
            _onMove = false;
            _player.GetComponent<Rigidbody>().isKinematic = false;
            OnAutoMoveFinished(new EventArgs());
        }
    }

    private void AudioGPS()
    {
        //Check if next point is intersection
        if (_currentWaypoint + 1 < _waypoints.Length)
        {
            if (_waypoints[_currentWaypoint].Intersection != Enums.Intersection.None)
            {
                for (int i = 0; i < _waypoints[_currentWaypoint].IntersectionPoints.Length; i++)
                {
                    if (_waypoints[_currentWaypoint].IntersectionPoints[i] != null)
                    {
                        if (_waypoints[_currentWaypoint].IntersectionPoints[i].transform == _waypoints[_currentWaypoint + 1].transform)
                        {
                            switch (i)
                            {
                                case 0:
                                    Debug.Log("Prochaine à gauche");
                                    break;
                                case 1:
                                    Debug.Log("Prochaine à droite");
                                    break;
                                case 2:
                                    Debug.Log("Tout droit");
                                    break;
                                case 3:
                                    Debug.Log("Derrière");
                                    break;
                            }
                            break;
                        }
                    }
                }
            }
        }
    }

    private float GetSpeed(float axis)
    {
        if (axis > 0f)
        {
            if(_pressedTime < _accelerationCurve.Count)
                _pressedTime += _accelerationTick;
            if ((int)_pressedTime < _accelerationCurve.Count)
                return _accelerationCurve[(int)_pressedTime];
            else
                return _accelerationCurve[_accelerationCurve.Count - 1];
        }
        else
        {
            if (_pressedTime > 0f)
            {
                if (axis < 0f)
                    _pressedTime -= _accelerationTick * _decelerationTick;
                else
                    _pressedTime -= _accelerationTick;
            }
            if ((int)_pressedTime > 0f)
                return _accelerationCurve[(int)_pressedTime];
            else
                return 0f;
        }
    }

    private void LoadDataFromFile(string path)
    {
        RoadPlannerXML roadPlannerObject = new RoadPlannerXML();
        XmlSerializer serializer = new XmlSerializer(typeof(RoadPlannerXML));
        using (StreamReader stream = new StreamReader(path, System.Text.Encoding.GetEncoding("UTF-8")))
        {
            roadPlannerObject = serializer.Deserialize(stream) as RoadPlannerXML;
            stream.Close();
        }

        //Set arrays
        GameObject[] plannerObjects = GameObject.FindGameObjectsWithTag("RoadPlanner");


        /*_pathParts = new PlannerPart[roadPlannerObject.AllowedPoints.Length];

        for (int cnt = 0; cnt < roadPlannerObject.AllowedPoints.Length; cnt++)
        {
            foreach (GameObject go in plannerObjects)
            {
                foreach (Transform tr in go.transform)
                {
                    if (Tools.ParseVector3ToString(tr.position) == roadPlannerObject.AllowedPoints[cnt])
                    {
                        _pathParts[cnt] = tr.GetComponent<PlannerPart>();
                        if (roadPlannerObject.AllowedPoints[cnt] == roadPlannerObject.StartPosition)
                        {
                            _pathParts[cnt].isStart = true;
                            _pathParts[cnt].Side = roadPlannerObject.Side;
                        }
                        if (roadPlannerObject.AllowedPoints[cnt] == roadPlannerObject.EndPosition)
                            _pathParts[cnt].isEnd = true;
                    }
                }
            }
        }

        SortParts();*/
        var listWp = new List<Waypoint>();
        var waypoints = new List<Waypoint>(GameObject.Find("Roads").GetComponentsInChildren<Waypoint>());

        foreach(var ap in roadPlannerObject.PlannedRoad)
        {
            foreach (Waypoint wp in waypoints)
            {
                if (Tools.ParseVector3ToString(wp.transform.position) == ap)
                {
                    listWp.Add(wp);
                    break;
                }
            }
        }

        SortWaypoints(listWp);
    }

    private void SortParts()
    {
        PlannerPart start = null;
        Vector3 startPosition = Vector3.zero;
        PlannerPart end = null;
        int size = _pathParts.Length;
        int cnt = 0;
        PlannerPart[] parts = new PlannerPart[size];
        foreach (PlannerPart part in _pathParts)
        {
            if (part.isStart)
            {
                start = part;
                startPosition = start.transform.position;
            }
            if (part.isEnd)
            {
                end = part;
            }
        }

        //Initialize tab:
        parts[0] = start;
        parts[size - 1] = end;
        cnt = 1;
        for (int i = 0; i < size; i++)
        {
            if (_pathParts[i] != start && _pathParts[i] != end)
            {
                parts[cnt] = _pathParts[i];
                cnt++;
            }
        }
        //ARRAY FORM:
        //[start],[?],...,[?],[end]

        //Bubble sort:
        int index = 0;
        Vector3 actualPos = startPosition;
        PlannerPart tempPart = null;
        int tempPartIndex = 0;

        float dist = 9999.0f;

        do
        {
            for (int j = (index + 1); j < size; j++)
            {
                if (Vector3.Distance(actualPos, parts[j].transform.position) < dist)
                {
                    dist = Vector3.Distance(actualPos, parts[j].transform.position);
                    tempPart = parts[j];
                    tempPartIndex = j;
                }
            }
            dist = 999.0f;
            index++;



            if (index < parts.Length)
            {
                parts[tempPartIndex] = parts[index];
                parts[index] = tempPart;
                actualPos = parts[index].transform.position;
            }

        } while (index < parts.Length);

        parts.CopyTo(_pathParts, 0);
    }

    private void SortWaypoints(int WaypointSide)
    {
        int side = WaypointSide;
        bool found = false;
        int size = 0;
        List<Waypoint> tmpWP = new List<Waypoint>();
        Waypoint last = null;
        
        for (int x = 0; x < _pathParts.Length; x++)
        {
            if (side == 0)
            {
                Debug.Log(_pathParts[x].WaypointsA.Length);
                if (_pathParts[x].WaypointsA.Length > 0)
                {
                    size = _pathParts[x].WaypointsA.Length;
                    for (int i = 0; i < size; i++)
                    {
                        tmpWP.Add(_pathParts[x].WaypointsA[i]);
                        last = _pathParts[x].WaypointsA[i];
                    }
                }
            }
            else
            {
                Debug.Log(_pathParts[x].WaypointsB.Length);
                if (_pathParts[x].WaypointsB.Length > 0)
                {
                    size = _pathParts[x].WaypointsB.Length;
                    for (int i = 0; i < size; i++)
                    {
                        tmpWP.Add(_pathParts[x].WaypointsB[i]);
                        last = _pathParts[x].WaypointsB[i];
                    }
                }
            }
            if (last.Intersection != Enums.Intersection.None && (x + 2) < _pathParts.Length)
            {
                float minDistance = float.MaxValue;
                Waypoint savePoint = null;
                //Matching the next point with the intersections in the current waypoint
                foreach (GameObject wpIntersection in last.IntersectionPoints)
                {

                    //Checking A side
                    foreach (Waypoint wpArray in _pathParts[x + 1].WaypointsA)
                    {
                        if (wpIntersection == wpArray.gameObject)
                        {
                            float distance = Vector3.Distance(wpIntersection.transform.position,wpArray.transform.position);
                            if (distance < minDistance)
                            {
                                minDistance = distance;
                                savePoint = wpIntersection.GetComponent<Waypoint>();
                                side = 0;
                            }
                        }
                    }
                    //Checking B side
                    foreach (Waypoint wpArray in _pathParts[x + 1].WaypointsB)
                    {
                        if (wpIntersection == wpArray.gameObject)
                        {
                            float distance = Vector3.Distance(wpIntersection.transform.position, wpArray.transform.position);
                            if (distance < minDistance)
                            {
                                minDistance = distance;
                                savePoint = wpIntersection.GetComponent<Waypoint>();
                                side = 1;
                            }
                        }
                    }
                }
                Debug.Log(minDistance);
                if (savePoint != null)
                {
                    tmpWP.Add(savePoint);
                }
            }
        }
        _waypointsTr = new Vector3[tmpWP.Count];
        _waypoints = new Waypoint[tmpWP.Count];
        for (int cnt = 0; cnt < tmpWP.Count; cnt++)
        {
            _waypoints[cnt] = tmpWP[cnt];
            _waypointsTr[cnt] = tmpWP[cnt].transform.position;
        }
    }

    private void SortWaypoints(List<Waypoint> PlannedRoad)
    {
        List<Waypoint> tmpWP = new List<Waypoint>();
        Waypoint last = null;

        foreach (var wp in PlannedRoad)
        {
            if (last == null)
            {
                tmpWP.Add(wp);
            }
            else
            {
                drawLine(last, wp, tmpWP);
            }
            last = wp;
        }

        _waypointsTr = new Vector3[tmpWP.Count];
        _waypoints = new Waypoint[tmpWP.Count];
        for (int cnt = 0; cnt < tmpWP.Count; cnt++)
        {
            _waypoints[cnt] = tmpWP[cnt];
            _waypointsTr[cnt] = tmpWP[cnt].transform.position;
        }
    }

    public void drawLine(Waypoint lastWaypoint, Waypoint currentWaypoint, List<Waypoint> tmpWP, bool start = false)
    {
        var waypointsList = lastWaypoint.transform.parent.gameObject.GetComponentsInChildren<Waypoint>();
        foreach (var waypoint in waypointsList)
        {
            //On cherche d'abord notre point de départ
            if (!start)
            {
                if (waypoint.name == lastWaypoint.name)
                    start = true;
            }
            //On commence à déssiner la route une fois la route trouver
            if (start)
            {
                tmpWP.Add(waypoint);
                if (waypoint.IntersectionPoints.Contains(currentWaypoint.gameObject))
                    break; //On s'arrete lorsqu'on trouve notre sortie de route
                if (waypoint.State == Enums.WaypointState.End)
                    if (waypoint.IntersectionPoints.Length > 0)
                    {
                        drawLine(waypoint.IntersectionPoints[0].GetComponent<Waypoint>(), currentWaypoint, tmpWP, true);
                    }
            }
        }
    }

    /*private void SortWaypoints(int WaypointSide)
    {
        int side = WaypointSide;
        bool found = false;
        int size = 0;

        //Set Waypoints
        List<Waypoint> tmpWP = new List<Waypoint>();

        for (int x = 0; x < _pathParts.Length; x++)
        
            #region SIDE A
            if (side == 0)
            {
                if(_pathParts[x].WaypointsA.Length > 0)
                {
                    size = _pathParts[x].WaypointsA.Length;
                    for (int i = 0; i < size; i++)
                    {
                        tmpWP.Add(_pathParts[x].WaypointsA[i]);
                    }
                    //Checking next point: [size - 1] waypoint of the current [x] point
                    if (_pathParts[x].WaypointsA[size - 1].Intersection != Enums.Intersection.None && (x + 2) < _pathParts.Length)
                    {
                        found = false;
                        //Matching the next point with the intersections in the current waypoint
                        foreach (GameObject wp in _pathParts[x].WaypointsA[size - 1].IntersectionPoints)
                        {
                            //Checking A side
                            for (int j = 0; j < _pathParts[x + 2].WaypointsA.Length; j++)
                            {
                                if (wp == _pathParts[x + 2].WaypointsA[j].gameObject)
                                {
                                    found = true;
                                    tmpWP.Add(wp.GetComponent<Waypoint>());
                                    side = 0;
                                    break;
                                }
                            }
                            if (!found)
                            {
                                //Checking B side
                                for (int j = 0; j < _pathParts[x + 1].WaypointsB.Length; j++)
                                {
                                    if (wp == _pathParts[x + 1].WaypointsB[j].gameObject)
                                    {
                                        found = true;
                                        tmpWP.Add(wp.GetComponent<Waypoint>());
                                        side = 1;
                                        break;
                                    }
                                }
                            }
                            if (found)
                                break;
                        }
                    }
                }
            }
            #endregion
            #region SIDE B
            else
            {
                if(_pathParts[x].WaypointsB.Length > 0)
                {
                    size = _pathParts[x].WaypointsB.Length;
                    for (int i = 0; i < size; i++)
                    {
                        tmpWP.Add(_pathParts[x].WaypointsB[i]);
                    }
                    //Checking next point:
                    if (_pathParts[x].WaypointsB[size - 1].Intersection != Enums.Intersection.None && (x + 2) < _pathParts.Length)
                    {
                        found = false;
                        //Matching the next point with the intersections in the current waypoint
                        foreach (GameObject wp in _pathParts[x].WaypointsB[size - 1].IntersectionPoints)
                        {
                            //Checking A side
                            for (int j = 0; j < _pathParts[x + 2].WaypointsA.Length; j++)
                            {
                                if (wp == _pathParts[x + 2].WaypointsA[j].gameObject)
                                {
                                    found = true;
                                    tmpWP.Add(wp.GetComponent<Waypoint>());
                                    side = 0;
                                    break;
                                }
                            }
                            if (!found)
                            {
                                //Checking B side
                                for (int j = 0; j < _pathParts[x + 1].WaypointsB.Length; j++)
                                {
                                    if (wp == _pathParts[x + 1].WaypointsB[j].gameObject)
                                    {
                                        found = true;
                                        tmpWP.Add(wp.GetComponent<Waypoint>());
                                        side = 1;
                                        break;
                                    }
                                }
                            }
                            if (found)
                                break;
                        }
                    }
                }
            }
            #endregion


        _waypointsTr = new Vector3[tmpWP.Count];
        _waypoints = new Waypoint[tmpWP.Count];
        for (int cnt = 0; cnt < tmpWP.Count; cnt++ )
        {
            _waypoints[cnt] = tmpWP[cnt];
            _waypointsTr[cnt] = tmpWP[cnt].transform.position;
        }
    }*/
}
