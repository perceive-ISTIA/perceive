﻿using UnityEngine;
using System.Collections;
using System;

public delegate void Experiment01EventHandler(object sender, EventArgs e);
public class Experiment01 : MonoBehaviour 
{
    public event Experiment01EventHandler EventHandler;
    protected virtual void OnExperiment01Event(EventArgs e)
    {
        if (EventHandler != null)
            EventHandler(this, e);
    }

    //private Exp _infoDelegate;
    private AutoMove _autoMove;
    private Transform _player;
    private GameObject _playerCamera;

    private Transform _thisTransform;
    private Renderer _thisRenderer;

    private Material _outlinedMat;
    private Material _normalMat;

    private Transform UI;
    private GameObject _panelBlack;
    private GameObject _panelAnswer;

    private float _globalWait = 2.0f;
    private float _blackWait = 2.0f;
    private float _offset = 0.0f;

    private IExperiment _iExperiment;
    public IExperiment IExpInstance
    {
        get
        {
            if (_iExperiment != null)
            {
                return _iExperiment;
            }
            else
            {
                _thisTransform = transform;
                _iExperiment = new Experiment01Class(_thisTransform);
                return _iExperiment;
            }
        }
    }

    void Awake()
    {
        _thisTransform = transform;
        _thisRenderer = _thisTransform.GetComponent<Renderer>();

        ObjectManager manager = GameObject.FindGameObjectWithTag("ObjectManager").GetComponent<ObjectManager>();
        _outlinedMat = manager.ExperimentOutlinedMaterial;
        _normalMat = manager.ExperimentNormalMaterial;

        _globalWait = ObjectManager.GetOptions().Exp01TimeGlobal;
        _blackWait = ObjectManager.GetOptions().Exp01TimeBlack;
        _offset = ObjectManager.GetOptions().Exp01Offset;

        _autoMove = GameObject.Find("AutoMoveController").GetComponent<AutoMove>();

        UI = GameObject.Find("Experiment01UI").transform;
        _panelBlack = UI.FindChild("PanelBlack").gameObject;
        _panelAnswer = UI.FindChild("PanelAnswer").gameObject;
        _iExperiment = new Experiment01Class(_thisTransform);
    }

	// Use this for initialization
	void Start () 
    {
        _thisRenderer.material = _normalMat;
	}

    void Update()
    {
        if (_player == null && GameObject.FindGameObjectWithTag("MainCamera") != null)
        {
            _playerCamera = GameObject.FindGameObjectWithTag("MainCamera");
            _player = _playerCamera.transform.parent;
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.name == "ColliderBottom")
        {
            _autoMove.FreezeMovement();
            TrafficLightGroupController.PauseAllLights();
            StartCoroutine(Wait(2.0f));
        }
    }

    public void DisableFreeze()
    {
        _autoMove.UnFreezeMovement();
        TrafficLightGroupController.PlayAllLights();
        _panelAnswer.SetActive(false);
    }

    IEnumerator Wait(float seconds)
    {
        OnExperiment01Event(new EventArgs()); //Event for handling the answers and store the answer
        yield return new WaitForSeconds(_globalWait);
        _panelBlack.SetActive(true);
        _panelAnswer.SetActive(false);
        yield return new WaitForSeconds(_blackWait);
        //Change if offset:
        if (ObjectManager.GetOptions().Exp01Offset != 0f)
            _autoMove.Experiment01Move();

        _panelBlack.SetActive(false);
        yield return new WaitForSeconds(_globalWait);
        _panelAnswer.SetActive(true);
    }

    void OnMouseEnter()
    {
        _thisRenderer.material = _outlinedMat;
        //ExpPath.OnExperimentHighlighted += _infoDelegate;
    }

    void OnMouseExit()
    {
        _thisRenderer.material = _normalMat;
        //ExpPath.OnExperimentHighlighted -= _infoDelegate;
    }
}

public class Experiment01Class : IExperiment
{
    public GameObject gameObject { get; set; }

    public Transform transform { get; set; }

    public Vector3 position
    {
        get
        {
            return transform.position;
        }
        set
        {
            transform.position = value;
        }
    }

    public Quaternion rotation
    {
        get
        {
            return transform.rotation;
        }
        set
        {
            transform.rotation = value;
        }
    }

    public Enums.ExperimentType Type { get; set; }

    public Experiment01Class(Transform tr)
    {
        transform = tr;
        gameObject = tr.gameObject;
        rotation = transform.rotation;
        position = transform.position;
        Type = Enums.ExperimentType.Experiment01;
    }


    public string Result { get; set; }
}

public class Experiment01Info : IExperimentInfo
{

    public Experiment01Info(Experiment01Class instance)
    {
        _instance = instance;
        
    }
    private Experiment01Class _instance;
    public IExperiment Instance
    {
        get
        {
            return _instance;
        }
        set
        {
            _instance = value as Experiment01Class;
        }
    }

    public override string ToString()
    {
        string str = string.Format("{0} ({1})", Name, Position);
        return str;
    }

    public string Type { get; set; }

    public string Name { get; set; }

    public string Position { get; set; }

}
