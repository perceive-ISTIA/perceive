﻿using UnityEngine;
using System.Collections;

public interface IExperimentInfo
{
    string Type { get; set; }
    string Name { get; set; }
    string Position { get; set; }
    IExperiment Instance { get; set; }
}

public interface IExperiment
{
    Transform transform { get; set; }
    Vector3 position { get; set; }
    Quaternion rotation { get; set; }
    Enums.ExperimentType Type { get; set; }

    string Result { get; set; }
}