﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Xml.Serialization;
using System.IO;
using System;

public class ExperimentController : MonoBehaviour
{
    #region Experiment01
    public Transform Experiment01AnswerPanel;

    private Toggle[] _01TogglesQ2;
    private Toggle[] _01TogglesQ1;
    #endregion

    private Transform _thisTransform;
    private Transform _trTmp;

    private IExperiment[] _experiments;
    private Experiment01 _exp01Tmp;
    private GameObject[] _experimentGameObjects;
    private int _currentExp = 0;
    private int _expSize;

    public string CurrentExperiment
    {
        get
        {
            if (_exp01Tmp == null)
                return "null";
            else
                return _exp01Tmp.transform.position.ToString();
        }
    }

    void Awake()
    {
        _thisTransform = transform;
    }

    public void InitAllAnswersPanels()
    {
        Transform Q1 = Experiment01AnswerPanel.FindChild("Q1");
        Transform Q2 = Experiment01AnswerPanel.FindChild("Q2");
        _01TogglesQ1 = new Toggle[3]
        {
            Q1.FindChild("ToggleA").GetComponent<Toggle>(),
            Q1.FindChild("ToggleB").GetComponent<Toggle>(),
            Q1.FindChild("ToggleC").GetComponent<Toggle>(),
        };
        _01TogglesQ2 = new Toggle[4]
        {
            Q2.FindChild("ToggleA").GetComponent<Toggle>(),
            Q2.FindChild("ToggleB").GetComponent<Toggle>(),
            Q2.FindChild("ToggleC").GetComponent<Toggle>(),
            Q2.FindChild("ToggleD").GetComponent<Toggle>()//,
            //Q2.FindChild("ToggleE").GetComponent<Toggle>()
        };
    }

    public void LoadExperiments(string path)
    {
        if (path == "")
            return;
        if (!File.Exists(path))
            return;

        //Deleting existing experiments:
        for (int cnt = 0; cnt < _thisTransform.childCount; cnt++)
        {
            GameObject.Destroy(_thisTransform.GetChild(cnt).gameObject);
        }


        ExperimentXML experimentObject = new ExperimentXML();
        XmlSerializer serializer = new XmlSerializer(typeof(ExperimentXML));
        using (StreamReader stream = new StreamReader(path, System.Text.Encoding.GetEncoding("UTF-8")))
        {
            experimentObject = serializer.Deserialize(stream) as ExperimentXML;
            stream.Close();

            _expSize = experimentObject.Size;
            _experimentGameObjects = new GameObject[_expSize];
            _experiments = new IExperiment[_expSize];
            for (int cnt = 0; cnt < _expSize; cnt++)
            {
                GameObject go = (GameObject)GameObject.Instantiate(Resources.Load(experimentObject.Type[cnt].ToString()));
                _trTmp = go.transform;
                _trTmp.SetParent(_thisTransform, false);
                _trTmp.name = experimentObject.Name[cnt];
                _trTmp.position = Tools.ParseStringToVector3(experimentObject.Position[cnt]);
                _trTmp.rotation = Tools.ParseStringToQuaternion(experimentObject.Rotation[cnt]);
                _trTmp.localScale = Tools.ParseStringToVector3(experimentObject.Scale[cnt]);
                _experimentGameObjects[cnt] = go;
                switch (experimentObject.Type[cnt])
                {
                    case Enums.ExperimentType.Experiment01:
                        _experiments[cnt] = go.GetComponent<Experiment01>().IExpInstance;
                        go.GetComponent<Experiment01>().EventHandler += new Experiment01EventHandler(this.experiment01_Event);
                        break;
                }
            }
        }

        InitAllAnswersPanels();
    }

    

    public static void SaveExperiments(string path, ExperimentXML data)
    {
        XmlSerializer serializer = new XmlSerializer(typeof(ExperimentXML));
        using (StreamWriter stream = new StreamWriter(path, false, System.Text.Encoding.GetEncoding("UTF-8")))
        {
            serializer.Serialize(stream, data);
            stream.Close();
        }
    }

    public void ValidateExperiment01AnswerQ1()
    {
        foreach (Toggle tg in _01TogglesQ1)
        {
            if (tg.isOn)
            {
                _exp01Tmp.IExpInstance.Result = string.Format("{0}", tg.transform.FindChild("Label").GetComponent<Text>().text);
            }
        }
        foreach(Toggle tg in _01TogglesQ2)
        {
            if(tg.isOn)
            {
                _exp01Tmp.IExpInstance.Result = string.Format("{0}=> {1}", _exp01Tmp.IExpInstance.Result, tg.transform.FindChild("Label").GetComponent<Text>().text);
            }
        }

        _exp01Tmp.DisableFreeze();
        _exp01Tmp = null;
    }

    public void SaveResults(string path)
    {
        if (_expSize == 0)
            return;
        ExperimentResultsXML resultsObject = new ExperimentResultsXML();

        resultsObject.Name = new string[_expSize];
        resultsObject.Position = new string[_expSize];
        resultsObject.Result = new string[_expSize];
        resultsObject.Rotation = new string[_expSize];
        resultsObject.Scale = new string[_expSize];
        resultsObject.Size = _expSize;
        resultsObject.Type = new Enums.ExperimentType[_expSize];

        for(int cnt = 0; cnt < _expSize; cnt++)
        {
            resultsObject.Name[cnt] = _experiments[cnt].transform.name;
            resultsObject.Position[cnt] = _experiments[cnt].position.ToString();
            if (_experiments[cnt].Result == null || _experiments[cnt].Result == "")
                resultsObject.Result[cnt] = "No result";
            else
                resultsObject.Result[cnt] = _experiments[cnt].Result;
            resultsObject.Rotation[cnt] = _experiments[cnt].rotation.ToString();
            resultsObject.Scale[cnt] = _experiments[cnt].transform.localScale.ToString();
            resultsObject.Type[cnt] = _experiments[cnt].Type;
        }

        XmlSerializer serializer = new XmlSerializer(typeof(ExperimentResultsXML));
        using (StreamWriter stream = new StreamWriter(path, false, System.Text.Encoding.GetEncoding("UTF-8")))
        {
            serializer.Serialize(stream, resultsObject);
            stream.Close();
        }
    }


    private void experiment01_Event(object sender, EventArgs e)
    {
        _exp01Tmp = (Experiment01)sender;

    }

    public void DeleteExperiments()
    {
        for(int cnt=0; cnt < _thisTransform.childCount; cnt++)
        {
            GameObject.Destroy(_thisTransform.GetChild(cnt).gameObject);
        }
    }
}
