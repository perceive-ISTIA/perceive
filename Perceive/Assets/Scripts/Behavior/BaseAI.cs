﻿using UnityEngine;
using System.Collections;

public interface BaseAI
{
    bool Wait { get; set; }
    Enums.AIState AIState { get; set; }
    void Move();
    /// <summary>
    /// Get the next destination
    /// </summary>
    /// <returns>Position of the destination</returns>
    Vector3 PickDestination();
    /// <summary>
    /// Get the movement' direction
    /// </summary>
    /// <returns>Vector3 of the direction</returns>
    Vector3 Direction();

    IEnumerator WaitSeconds(float seconds);

    /// <summary>
    /// Check if the desired destination is reached or not
    /// </summary>
    void CheckDestination();

    void Spawn();
}
