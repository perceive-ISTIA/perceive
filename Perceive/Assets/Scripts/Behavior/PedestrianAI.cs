﻿using UnityEngine;
using System.Collections;

public class PedestrianAI : BaseAI
{
    private Enums.AIType _type = Enums.AIType.Pedestrian;
    private GameObject _obj;
    private Animator _animator;
    private NavMeshAgent _agent;
    //private PathMaker[] _roads;
    public PedestrianAI(GameObject obj)
    {
        _obj = obj;
        _animator = _obj.GetComponent<Animator>();
        _agent = _obj.GetComponent<NavMeshAgent>();
    }


    public void Move()
    {
        if(!_wait)
        {
            _wait = true;
            _agent.SetDestination(PickDestination());
            _animator.SetBool("isWalk", true);
        }        
    }

    public Vector3 PickDestination()
    {
        NavMeshHit hit;

        NavMesh.SamplePosition((Random.insideUnitSphere * 10.0f) + _obj.transform.position, out hit, 200, 9);
        Debug.Log(hit.mask + "=> " + hit.position);
        return hit.position;
    }

    public IEnumerator WaitSeconds(float seconds)
    {
        yield return new WaitForSeconds(seconds);
    }

    public void WaitFormMove()
    {
        throw new System.NotImplementedException();
    }

    private bool _wait = false;
    public bool Wait
    {
        get
        {
            return _wait;
        }
        set
        {
            _wait = value;
        }
    }


    public void CheckDestination()
    {
        if (Vector3.Distance(_agent.destination, _obj.transform.position) < 2.0f)
        {
            _wait = false;
            _animator.SetBool("isWalk", false);
        }
    }


    public Vector3 Direction()
    {
        throw new System.NotImplementedException();
    }


    public bool NextIsIntersection
    {
        get
        {
            throw new System.NotImplementedException();
        }
        set
        {
            throw new System.NotImplementedException();
        }
    }

    private Enums.AIState _state;
    public Enums.AIState AIState
    {
        get
        {
            return _state;
        }
        set
        {
            _state = value;
        }
    }

    public void Spawn()
    {

    }
}
