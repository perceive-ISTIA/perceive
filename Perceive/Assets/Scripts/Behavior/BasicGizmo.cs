﻿using UnityEngine;
using System.Collections;

public class BasicGizmo : MonoBehaviour
{
    private Transform _thisTransform;
    private Vector3 _cube = new Vector3(2f, 2f, 2f);
    void Awake()
    {
        _thisTransform = transform;
    }
	void OnDrawGizmos()
    {
        if (_thisTransform == null)
            _thisTransform = transform;
        Gizmos.color = Color.magenta;
        Gizmos.DrawCube(_thisTransform.position, _cube);
    }
}
