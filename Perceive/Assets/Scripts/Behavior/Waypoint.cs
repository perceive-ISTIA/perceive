﻿using UnityEngine;
using System.Collections;
using System.Xml;
using System.Xml.Serialization;
using System.Collections.Generic;

[ExecuteInEditMode]
public class Waypoint : MonoBehaviour
{
    private int _id = -1;
    public bool DrawBezier = false;
    [Range(0.01f, 1.0f)]
    public float BezierPrecision = 0.07f;
    public Enums.Bezier BezierType;
    public Enums.WaypointState State;
    public Vector3[] BezierPoints { get; set; }
    public Vector3[][] IntersectBezierPoints { get; set; }
    private float[] _lastIntersectBezierTotal = new float[4];
    private bool _firstLaunch = true;
    private float _lastBezierTotal = 0;
    private GameObject[] iP1;
    private GameObject[] iP2;
    private GameObject P1;
    private GameObject P2;
    private bool _onLoad = false;
    private bool _onDisable = false;

    public Enums.SplitState SplitState = Enums.SplitState.None;

    private Transform _thistransform;
    //Cache values:
    private Vector3 _lastPos;
    private Vector3 _lastP1Pos;
    private Vector3 _lastP2Pos;
    private Vector3[] _lastIntersectPos = new Vector3[4];
    private Vector3 _lastNextPos;

    private CarAI[] _currentAI;
    public CarAI[] CarAIList
    {
        get
        {
            return _currentAI;
        }
    }
    public void InitCarList(int size)
    {
        _currentAI = new CarAI[size];
    }

    private bool _isStack = false;

    public uint _carCount = 0;
    public uint CarCount
    {
        get
        {
            return _carCount;
        }
    } 

    void Awake()
    {
        _thistransform = transform;
        _lastPos = _thistransform.position;
        _carCount = 0;
    }

    public void AddCarAI(CarAI ai)
    {
        _currentAI[_carCount] = ai;
        _carCount++;
    }
    public void RemoveCarAI(CarAI ai)
    {
        for (int cnt = 0; cnt < _carCount; cnt++ )
        {
            if(_currentAI[cnt] == ai)
            {
                _currentAI[cnt] = null;
                _carCount--;
            }
        }
    }
    public bool ContainsAI(CarAI ai)
    {
        for(int j = 0; j < _carCount; j++)
        {
            if(_currentAI[_carCount] == ai)
                return true;
        }
        return false;
    }

    public TrafficLightController IntersectionLightController;

    public int ID
    {
        get
        {
            return _id;
        }
        set
        {
            _id = value;
            if (SplitState == Enums.SplitState.None)
                _thistransform.name = string.Format("_waypoint{0}", value);
            if (_firstLaunch)
                _firstLaunch = false;
        }
    }

    private int _splitID = -1;
    public int SplitID
    {
        get
        {
            return _splitID;
        }
        set
        {
            _splitID = value;
            _thistransform.name = string.Format("_waypointSplit{0}_{1}", _id, _splitID);
        }
    }
    public int Total { get; set; }

    public Enums.Intersection Intersection = Enums.Intersection.None;

    public GameObject[] IntersectionPoints = new GameObject[4];

    public void InitWaypoint()
    {
        if (_thistransform.name.Contains("Split"))
        {
            string strname = _thistransform.name.Replace("_waypointSplit", "");
            ID = int.Parse(strname.Remove(1));
            SplitID = int.Parse(strname.Substring(2));
        }
        else
        {
            _splitID = -1;
            if (_thistransform.name.Contains("_waypoint"))
            {
                ID = int.Parse(_thistransform.name.Replace("_waypoint", ""));
            }
        }

        //Looking for P1 and P2
        if (_id < (Total - 1) && DrawBezier)
        {
            if (_thistransform.FindChild("_P1") == null)
            {
                P1 = new GameObject("_P1");
                P1.transform.SetParent(_thistransform);
                {
                    if (_splitID == -1)
                    {
                        Vector3 P = _thistransform.parent.FindChild(string.Format("_waypoint{0}", _id + 1)).position;
                        P1.transform.position = new Vector3(_thistransform.position.x, _thistransform.position.y, P.z);
                    }
                    else
                    {
                        Vector3 P = _thistransform.parent.FindChild(string.Format("_waypointSplit{0}_{1}", _id + 1, _splitID)).position;
                        P1.transform.position = new Vector3(_thistransform.position.x, _thistransform.position.y, P.z);
                    }
                    _lastP1Pos = P1.transform.position;
                }
                P1.AddComponent<BasicGizmo>();
            }
            else
                P1 = _thistransform.FindChild("_P1").gameObject;
            if (BezierType == Enums.Bezier.Cubic)
            {
                if (_thistransform.FindChild("_P2") == null)
                {
                    P2 = new GameObject("_P2");
                    P2.transform.SetParent(_thistransform);
                    P2.AddComponent<BasicGizmo>();
                    P2.transform.position = _thistransform.position;
                }
                else
                    P2 = _thistransform.FindChild("_P2").gameObject;
                _lastP2Pos = P2.transform.position;
            }
        }

        if (DrawBezier)
        {
            //Bezier array:
            float total = 1 / BezierPrecision; //Calculte the amount of points needed for Bezier
            BezierPoints = new Vector3[(int)total]; //Creating the new Array
            _lastBezierTotal = total;
        }
        bool ok = false;
        for (int cnt = 0; cnt < 4; cnt ++ )
        {
            if (IntersectionPoints[cnt] != null)
            {
                ok = true;
                _lastIntersectPos[cnt] = IntersectionPoints[cnt].transform.position;
            }
            else
            {
                _lastIntersectPos[cnt] = Vector3.zero;
            }
        }

        if (Intersection != Enums.Intersection.None && ok)
        {
            iP1 = new GameObject[4];
            iP2 = new GameObject[4];
            IntersectBezierPoints = new Vector3[4][];
        }

        _lastBezierTotal = -1;
        for(int cnt = 0; cnt< 4; cnt++)
        {
            _lastIntersectBezierTotal[cnt] = -1f;
        }
    }
    
    public void Refresh()
    {
        DrawBezier = false;
        BezierPoints = null;
        foreach (Transform tr in _thistransform)
        {
            GameObject.DestroyImmediate(tr.gameObject);
        }
    }

    public void SetStack(bool isStack)
    {
        _isStack = isStack;
    }

    private bool CheckForChanges(bool noUpdate, bool isBezier = false, bool isIntersection = false)
    {
        if (!noUpdate)
            return false;
        if(isBezier)
        {
            //Check the position of the next point:
            if(_lastNextPos != _thistransform.parent.FindChild(string.Format("_waypoint{0}", _id + 1)).position)
            {
                _lastNextPos = _thistransform.parent.FindChild(string.Format("_waypoint{0}", _id + 1)).position;
                return false;
            }
            //Check pos
            if (P1 != null)
            {
                if (_lastP1Pos != P1.transform.position)
                {
                    _lastP1Pos = P1.transform.position;
                    return false;
                }
            }
            if (BezierType == Enums.Bezier.Cubic)
            {
                if (P2 != null)
                {
                    if (_lastP2Pos != P2.transform.position)
                    {
                        _lastP2Pos = P2.transform.position;
                        return false;
                    }
                }
            }
        }
        if(isIntersection)
        {
            for(int cnt = 0; cnt < 4; cnt++)
            {
                if (IntersectionPoints[cnt] != null)
                {
                    if (_lastIntersectPos[cnt] != IntersectionPoints[cnt].transform.position)
                    {
                        _lastIntersectPos[cnt] = IntersectionPoints[cnt].transform.position;
                        return false;
                    }
                }
            }
        }
        if (_lastPos != _thistransform.position)
        {
            _lastPos = _thistransform.position;
            return false;
        }
        return true;
    }

    void OnDrawGizmos()
    {
        if (_thistransform == null)
            _thistransform = transform;
        if (_onLoad)
            return;
        #region Intersections
        bool ok = false;
        foreach (GameObject obj in IntersectionPoints)
        {
            if (obj != null)
            {
                ok = true;
                break;
            }
        }
        //Changing colors with intersections type
        switch (Intersection)
        {
            case Enums.Intersection.Lights:
                Gizmos.color = Color.green;
                //Bezier :
                if(ok)
                    DrawBezierIntersection();
                /*if (GameObject == null)
                    Debug.LogWarning("No intersection controller selected, please add one.");*/
                Gizmos.DrawSphere(_thistransform.position, 1.5f);
                break;
            case Enums.Intersection.None:
                Gizmos.color = Color.white;
                Gizmos.DrawSphere(_thistransform.position, 1.0f);
                break;
            case Enums.Intersection.Right:
                Gizmos.color = Color.yellow;
                //Bezier :
                if (ok)
                    DrawBezierIntersection();
                Gizmos.DrawSphere(_thistransform.position, 1.5f);
                break;
            case Enums.Intersection.Stop:
                Gizmos.color = Color.red;
                //Bezier :
                if (ok)
                    DrawBezierIntersection();
                //Set stack
                foreach(GameObject go in IntersectionPoints)
                {
                    if (go != null)
                        go.GetComponent<Waypoint>().SetStack(true);
                }
                Gizmos.DrawSphere(_thistransform.position, 1.5f);
                break;
            case Enums.Intersection.Split2:
                Gizmos.color = Color.grey;
                if (_splitID != -2)
                {
                    int splitID = -1;
                    Transform tmp = null;
                    foreach (Transform tr in _thistransform.parent)
                    {
                        if (tr.name.Contains(string.Format("Split{0}_", ID - 1)))
                        {
                            if (int.Parse(tr.name.Replace(string.Format("_waypointSplit{0}_", ID - 1), "")) > splitID)
                            {
                                splitID = int.Parse(tr.name.Replace(string.Format("_waypointSplit{0}_", ID - 1), ""));
                                tmp = tr;
                            }
                        }
                    }
                    if (_splitID > 0 && tmp.GetComponent<Waypoint>().SplitState != Enums.SplitState.End)
                    {
                        tmp.GetComponent<Waypoint>().SplitState = Enums.SplitState.End;
                        GameObject.DestroyImmediate(_thistransform.parent.FindChild(string.Format("_waypointSplit{0}_{1}", _id, _splitID)).gameObject);
                        _splitID = -2;
                    }
                }
                Gizmos.DrawSphere(_thistransform.position, 1.5f);
                break;
            case Enums.Intersection.SimpleCrossing:
                Gizmos.color = new Color32(204, 102, 0, 255);
                if (ok)
                    DrawBezierIntersection();
                Gizmos.DrawSphere(_thistransform.position, 1.5f);
                break;
        }
        #endregion

        #region DrawLineWithPrevious
        Gizmos.color = Color.cyan;
        if (_id > 0)
        {
            if(_isStack)
                Gizmos.color = Color.blue;

            if(SplitState != Enums.SplitState.End && SplitState != Enums.SplitState.Split)
                Gizmos.DrawLine(_thistransform.position, _thistransform.parent.FindChild(string.Format("_waypoint{0}", _id - 1)).position);
            else
            {
                //Get the previous split
                Gizmos.DrawLine(_thistransform.position, _thistransform.parent.FindChild(string.Format("_waypointSplit{0}_{1}", _id - 1, _splitID - 1)).position);
            }
            if (Intersection == Enums.Intersection.Split2)
            {
                Gizmos.DrawLine(_thistransform.position, _thistransform.parent.FindChild(string.Format("_waypoint{0}", _id - 1)).position);
                foreach (Transform tr in _thistransform.parent)
                {
                    if(tr.name.Contains("Split"))
                    {
                        if (tr.name.Replace("Split", "").Contains(string.Format("_waypoint{0}_", _id - 1)))
                        {
                            Gizmos.DrawLine(_thistransform.position, tr.position);
                            break;
                        }
                    }
                }
            }
            _isStack = false;
        }
        #endregion

        #region Bezier
        if (DrawBezier)
        {
            //Get the total amount of waypoint (safety purposes)
            Total = _thistransform.parent.childCount;
            //Check if the current waypoint isn't the last one
            if (_id < (Total - 1))
            {
                //Looking for P1 and P2
                if (_thistransform.FindChild("_P1") == null)
                {
                    P1 = new GameObject("_P1"); //Creating the new GameObject P1
                    P1.transform.SetParent(_thistransform); //Set the current waypoint as parent
                    Vector3 P = _thistransform.parent.FindChild(string.Format("_waypoint{0}", _id + 1)).position; //Looking for the next waypoint
                    P1.transform.position = new Vector3(_thistransform.position.x, _thistransform.position.y, P.z); //Set the default position of _P1
                    P1.AddComponent<BasicGizmo>(); //DrawGizmo
                }
                else
                    P1 = _thistransform.FindChild("_P1").gameObject; //Retreiving the _P1 GameObject


                float total = 1 / BezierPrecision; //Calculte the amount of points needed for Bezier
                bool noUpdate = true;
                //Instantiate the array only when the size changed
                if (total != _lastBezierTotal)
                {
                    BezierPoints = new Vector3[(int)total]; //Creating the new Array
                    _lastBezierTotal = total;
                    noUpdate = false;
                }

                float t = 0.0f; //Time ==> Precision

                Vector3 next; //Next point
                Vector3 origin = _thistransform.position; //Current point
                Gizmos.color = Color.magenta;

                if (!CheckForChanges(noUpdate, true))
                {
                    for (int i = 0; i < (int)total; i++)
                    {
                        if (BezierType == Enums.Bezier.Quadratic)
                        {
                            //Destroy _P2 if present (=>Quadratic need only 3 points)
                            if (_thistransform.FindChild("_P2") != null)
                            {
                                GameObject.DestroyImmediate(_thistransform.FindChild("_P2").gameObject);
                            }

                            //Get the next point coords
                            next = Tools.QuadraticBezier(_thistransform.position, P1.transform.position, _thistransform.parent.FindChild(string.Format("_waypoint{0}", _id + 1)).position, t);
                            //Add the point to the array :
                            BezierPoints[i] = next;

                            t += BezierPrecision; //Increasing the time

                            //Draw Gizmos
                            Gizmos.DrawSphere(next, .5f);
                            Gizmos.DrawLine(origin, next);

                            origin = next; //Saving the next Vector3
                        }
                        if (BezierType == Enums.Bezier.Cubic)
                        {
                            //Check if _P2 is present or missing
                            if (_thistransform.FindChild("_P2") == null)
                            {
                                //Creating a new _P2
                                P2 = new GameObject("_P2");
                                P2.transform.SetParent(_thistransform);
                                P2.AddComponent<BasicGizmo>();
                                P2.transform.position = _thistransform.position;
                            }
                            else
                                P2 = _thistransform.FindChild("_P2").gameObject; //Get the existing _P2

                            //Get tne next point coords
                            next = Tools.CubicBezier(_thistransform.position, P1.transform.position, P2.transform.position, _thistransform.parent.FindChild(string.Format("_waypoint{0}", _id + 1)).position, t);
                            //Add the point to the array :
                            BezierPoints[i] = next;

                            t += BezierPrecision; //Increasing the next Vector3

                            //Draw Gizmos
                            Gizmos.DrawSphere(next, .5f);
                            Gizmos.DrawLine(origin, next);

                            origin = next; //Saving the next Vector3
                        }
                    }
                }
                else
                {
                    for(int cnt = 0; cnt < BezierPoints.Length; cnt++)
                    {
                        Gizmos.DrawSphere(BezierPoints[cnt], .5f);
                        Gizmos.DrawLine(origin, BezierPoints[cnt]);
                        origin = BezierPoints[cnt];
                    }
                }
                Gizmos.DrawLine(origin, _thistransform.parent.FindChild(string.Format("_waypoint{0}", _id + 1)).position); //Draw la last line between the next waypoint and the last Bezier point
            }
            else
            {
                Debug.LogWarning("Unable to draw Bezier curve with the last waypoint !");
                DrawBezier = false;
            }


        }
        else
        {
            //Destroy all the childs ! yeah !
            if (_thistransform.childCount > 0 && Intersection == Enums.Intersection.None)
            {
                foreach (Transform tr in _thistransform)
                {
                    GameObject.DestroyImmediate(tr.gameObject);
                }
            }
        }
        #endregion
    }

    void OnDestroy()
    {
        if (_onDisable)
            return;
        #region Split
        //Check if the selected waypoint is associated with a split point:
        bool isSplit = false;
        GameObject associatedWP = null;
        if (_thistransform.name.Contains("Split"))
        {
            isSplit = true;
            if (_thistransform.parent.FindChild(string.Format("_waypoint{0}", _id)) != null)
                associatedWP = _thistransform.parent.FindChild(string.Format("_waypoint{0}", _id)).gameObject;
        }
        else
        {
            foreach (Transform tr in _thistransform.parent)
            {
                if (tr.name.Contains(string.Format("_waypointSplit{0}_", _id)))
                {
                    associatedWP = tr.gameObject;
                    break;
                }
            }
        }
        #endregion
        bool cont = true;
        foreach (Transform tr in _thistransform.parent)
        {
            Waypoint dg = tr.GetComponent<Waypoint>();
            if (dg.ID > _id)
            {
                if (isSplit)
                {

                    //Decrease the split of the selected split section
                    if (dg.SplitState != Enums.SplitState.None && cont)
                    {
                        dg.SplitID--;
                    }
                    else
                        cont = false;
                }
                else
                    dg.ID--;
            }
            if (!isSplit)
            {
                dg.Total--;
                if (dg.ID == (dg.Total - 1))
                    dg.State = Enums.WaypointState.End;
                if (dg.ID == 0)
                    dg.State = Enums.WaypointState.Begin;
            }

            if (associatedWP != null && Application.isEditor)
                GameObject.DestroyImmediate(associatedWP);
        }
        Debug.LogWarning(string.Format("Waypoint n°{0} destroyed !", ID));
    }
    void OnDisable()
    {
        _onDisable = true;
    }

    

    private void DrawBezierIntersection()
    {
        string strP1 = "_P1(LEFT)";
        string strP2 = "_P2(LEFT)";
        if (IntersectBezierPoints == null)
        {
            IntersectBezierPoints = new Vector3[4][];
            _lastIntersectBezierTotal = new float[4];
        }
        if (iP1 == null || iP1.Length != 4)
            iP1 = new GameObject[4];
        if (iP2 == null || iP2.Length != 4)
            iP2 = new GameObject[4];
        for(int cnt = 0; cnt <IntersectionPoints.Length; cnt++)
        {
            if (IntersectionPoints[cnt] != null)
            {
                //Looking for P1 and P2
                switch(cnt)
                {
                    case 1:
                        strP1 = "_P1(RIGHT)";
                        strP2 = "_P2(RIGHT)";
                        break;
                    case 2:
                        strP1 = "_P1(TOP)";
                        strP2 = "_P2(TOP)";
                        break;
                    case 3:
                        strP1 = "_P1(DOWN)";
                        strP2 = "_P2(DOWN)";
                        break;
                }
                if (_thistransform.FindChild(strP1) == null)
                {
                    //Debug.Log(iP1.Length + "  => " + transform.name);
                    iP1[cnt] = new GameObject(strP1); //Creating the new GameObject P1
                    iP1[cnt].transform.SetParent(_thistransform); //Set the current waypoint as parent
                    Vector3 P = IntersectionPoints[cnt].transform.position; //Looking for the next waypoint
                    iP1[cnt].transform.position = new Vector3(_thistransform.position.x, _thistransform.position.y, P.z); //Set the default position of _P1
                    iP1[cnt].AddComponent<BasicGizmo>(); //DrawGizmo
                }
                else
                    iP1[cnt] = _thistransform.FindChild(strP1).gameObject; //Retreiving the _P1 GameObject


                float total = 1 / BezierPrecision; //Calculte the amount of points needed for Bezier
                bool noUpdate = true;
                //Instantiate the array only when the size changed
                if (total != _lastIntersectBezierTotal[cnt])
                {
                    IntersectBezierPoints[cnt] = new Vector3[(int)total]; //Creating the new Array
                    _lastIntersectBezierTotal[cnt] = total;
                    noUpdate = false;
                }

                float t = 0.0f; //Time ==> Precision

                Vector3 next; //Next point
                Vector3 origin = _thistransform.position; //Current point

                if (!CheckForChanges(noUpdate, false, true))
                {
                    for (int i = 0; i < (int)total; i++)
                    {
                        if (BezierType == Enums.Bezier.Quadratic)
                        {
                            //Destroy _P2 if present (=>Quadratic need only 3 points)
                            if (_thistransform.FindChild(strP2) != null)
                            {
                                GameObject.DestroyImmediate(_thistransform.FindChild(strP2).gameObject);
                            }

                            //Get the next point coords
                            next = Tools.QuadraticBezier(_thistransform.position, iP1[cnt].transform.position, IntersectionPoints[cnt].transform.position, t);
                            //Add the point to the array :
                            IntersectBezierPoints[cnt][i] = next;

                            t += BezierPrecision; //Increasing the time

                            //Draw Gizmos
                            Gizmos.DrawSphere(next, .5f);
                            Gizmos.DrawLine(origin, next);

                            origin = next; //Saving the next Vector3
                        }
                        if (BezierType == Enums.Bezier.Cubic)
                        {
                            //Check if _P2 is present or missing
                            if (_thistransform.FindChild(strP2) == null)
                            {
                                //Creating a new _P2
                                iP2[cnt] = new GameObject(strP2);
                                iP2[cnt].transform.SetParent(_thistransform);
                                iP2[cnt].AddComponent<BasicGizmo>();
                                iP2[cnt].transform.position = _thistransform.position;
                            }
                            else
                                iP2[cnt] = _thistransform.FindChild(strP2).gameObject; //Get the existing _P2

                            //Get tne next point coords
                            next = Tools.CubicBezier(_thistransform.position, iP1[cnt].transform.position, iP2[cnt].transform.position, IntersectionPoints[cnt].transform.position, t);
                            //Add the point to the array :
                            IntersectBezierPoints[cnt][i] = next;

                            t += BezierPrecision; //Increasing the next Vector3

                            //Draw Gizmos
                            Gizmos.DrawSphere(next, .5f);
                            Gizmos.DrawLine(origin, next);

                            origin = next; //Saving the next Vector3
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < IntersectBezierPoints[cnt].Length; i++)
                    {
                        Gizmos.DrawSphere(IntersectBezierPoints[cnt][i], .5f);
                        Gizmos.DrawLine(origin, IntersectBezierPoints[cnt][i]);
                        origin = IntersectBezierPoints[cnt][i];
                    }
                }
                Gizmos.DrawLine(origin, IntersectionPoints[cnt].transform.position); //Draw la last line between the next waypoint and the last Bezier point
            }
        }
    }

    public override string ToString()
    {
        string str = _thistransform.name;
        if (Intersection != Enums.Intersection.None)
            str = string.Format("{0} | {1}", str, Intersection.ToString());
        if (State != Enums.WaypointState.None)
            str = string.Format("{0} | {1}", str, State.ToString());
        if (DrawBezier)
            str = string.Format("{0} => Bezier ({1})", str, BezierType.ToString());
        str = string.Format("{0} | Cars: {1}", str, _carCount);
        return str;
    }

    public string GetCars()
    {
        string str = "Cars: ";
        foreach(CarAI car in _currentAI)
        {
            str = string.Format("{0} | {1}", str, car.name);
            if (car.Wait)
                str = string.Format("{0} (Waiting)", str);
        }
        return str;
    }

    public GizmoData GetSerializableData()
    {
        GizmoData data = new GizmoData();
        data.ID = _id;
        data.SplitID = SplitID;
        data.Total = Total;
        data.Name = _thistransform.name;
        data.DrawBezier = DrawBezier;
        data.BezierPrecision = BezierPrecision;
        data.BezierType = BezierType;
        data.State = State;
        data.SplitState = SplitState;
        if (BezierPoints != null)
        {
            if (_thistransform.FindChild("_P1") != null)
                data.P1 = _thistransform.FindChild("_P1").transform.position.ToString();
            if (data.BezierType == Enums.Bezier.Cubic)
                data.P2 = _thistransform.FindChild("_P2").transform.position.ToString();
            data.BezierPointString = new string[BezierPoints.Length];
            for (int cnt = 0; cnt < BezierPoints.Length; cnt++)
            {
                data.BezierPointString[cnt] = BezierPoints[cnt].ToString().Replace(" ", "");
            }
        }

        data.IntersectionType = Intersection;
        if (IntersectBezierPoints != null)
        {
            if(IntersectBezierPoints[0] != null)
            {
                if (_thistransform.FindChild("_P1(LEFT)"))
                    data.P1L = _thistransform.FindChild("_P1(LEFT)").transform.position.ToString();

                data.IntersectBezierPointsString01 = new string[IntersectBezierPoints[0].Length];
                for(int cnt = 0; cnt < IntersectBezierPoints[0].Length; cnt++)
                {
                    data.IntersectBezierPointsString01[cnt] = IntersectBezierPoints[0][cnt].ToString().Replace(" ", "");
                }
            }
            if (IntersectBezierPoints[1] != null)
            {
                if (_thistransform.FindChild("_P1(RIGHT)") != null)
                    data.P1R = _thistransform.FindChild("_P1(RIGHT)").transform.position.ToString();

                data.IntersectBezierPointsString02 = new string[IntersectBezierPoints[1].Length];
                for(int cnt = 0; cnt < IntersectBezierPoints[1].Length; cnt++)
                {
                    data.IntersectBezierPointsString02[cnt] = IntersectBezierPoints[1][cnt].ToString().Replace(" ", "");
                }
            }
            if (IntersectBezierPoints[2] != null)
            {
                if (_thistransform.FindChild("_P1(TOP)") != null)
                    data.P1T = _thistransform.FindChild("_P1(TOP)").transform.position.ToString();
                data.IntersectBezierPointsString03 = new string[IntersectBezierPoints[2].Length];
                for(int cnt = 0; cnt < IntersectBezierPoints[2].Length; cnt++)
                {
                    data.IntersectBezierPointsString03[cnt] = IntersectBezierPoints[2][cnt].ToString().Replace(" ", "");
                }
            }
            if (IntersectBezierPoints[3] != null)
            {
                if (_thistransform.FindChild("_P1(DOWN)") != null)
                    data.P1D = _thistransform.FindChild("_P1(DOWN)").transform.position.ToString();
                data.IntersectBezierPointsString04 = new string[IntersectBezierPoints[3].Length];
                for(int cnt = 0; cnt < IntersectBezierPoints[3].Length; cnt++)
                {
                    data.IntersectBezierPointsString04[cnt] = IntersectBezierPoints[3][cnt].ToString().Replace(" ", "");
                }
            }
        }

        return data;
    }

    public void LoadData(GizmoData data)
    {
        _onLoad = true;
        _id = data.ID;
        SplitID = data.SplitID;
        Total = data.Total;
        if (_thistransform.name != data.Name)
            _thistransform.name = data.Name;
        DrawBezier = data.DrawBezier;
        BezierPrecision = data.BezierPrecision;
        BezierType = data.BezierType;
        State = data.State;
        SplitState = data.SplitState;

        if (data.BezierPointString != null && data.BezierPointString.Length > 0)
        {
            if (!_thistransform.FindChild("_P1"))
            {
                GameObject P1 = new GameObject("_P1");
                P1.transform.SetParent(_thistransform);
                P1.AddComponent<BasicGizmo>();
                P1.transform.position = Tools.ParseStringToVector3(data.P1);
            }
            else
            {
                _thistransform.FindChild("_P1").transform.position = Tools.ParseStringToVector3(data.P1);
            }
            if (BezierType == Enums.Bezier.Cubic)
            {
                if (!_thistransform.FindChild("_P2"))
                {
                    GameObject P2 = new GameObject("_P2");
                    P2.transform.SetParent(_thistransform);
                    P2.AddComponent<BasicGizmo>();
                    P2.transform.position = Tools.ParseStringToVector3(data.P2);
                }
                else
                {
                    _thistransform.FindChild("_P2").transform.position = Tools.ParseStringToVector3(data.P2);
                }
            }
            BezierPoints = new Vector3[data.BezierPointString.Length];
            for (int cnt = 0; cnt < data.BezierPointString.Length; cnt++)
            {
                BezierPoints[cnt] = Tools.ParseStringToVector3(data.BezierPointString[cnt]);
            }
        }

        Intersection = data.IntersectionType;

        if (data.IntersectBezierPointsString01 != null || data.IntersectBezierPointsString02 != null || data.IntersectBezierPointsString03 != null || data.IntersectBezierPointsString04 != null)
        {
            IntersectBezierPoints = new Vector3[4][];
            if (data.IntersectBezierPointsString01 != null)
            {
                if (!transform.FindChild("_P1(LEFT)"))
                {
                    GameObject P1 = new GameObject("_P1(LEFT)");
                    P1.transform.SetParent(_thistransform);
                    P1.AddComponent<BasicGizmo>();
                    P1.transform.position = Tools.ParseStringToVector3(data.P1L);
                }
                else
                {
                    _thistransform.FindChild("_P1(LEFT)").transform.position = Tools.ParseStringToVector3(data.P1L);
                }
                IntersectBezierPoints[0] = new Vector3[data.IntersectBezierPointsString01.Length];
                for (int cnt = 0; cnt < data.IntersectBezierPointsString01.Length; cnt++)
                {
                    IntersectBezierPoints[0][cnt] = Tools.ParseStringToVector3(data.IntersectBezierPointsString01[cnt]);
                }
            }
            if (data.IntersectBezierPointsString02 != null)
            {
                if (!_thistransform.FindChild("_P1(RIGHT)"))
                {
                    GameObject P1 = new GameObject("_P1(RIGHT)");
                    P1.transform.SetParent(_thistransform);
                    P1.AddComponent<BasicGizmo>();
                    P1.transform.position = Tools.ParseStringToVector3(data.P1R);
                }
                else
                {
                    _thistransform.FindChild("_P1(RIGHT)").transform.position = Tools.ParseStringToVector3(data.P1R);
                }
                IntersectBezierPoints[1] = new Vector3[data.IntersectBezierPointsString02.Length];
                for (int cnt = 0; cnt < data.IntersectBezierPointsString02.Length; cnt++)
                {
                    IntersectBezierPoints[1][cnt] = Tools.ParseStringToVector3(data.IntersectBezierPointsString02[cnt]);
                }
            }
            if (data.IntersectBezierPointsString03 != null)
            {
                if (!_thistransform.FindChild("_P1(TOP)"))
                {
                    GameObject P1 = new GameObject("_P1(TOP)");
                    P1.transform.SetParent(_thistransform);
                    P1.AddComponent<BasicGizmo>();
                    P1.transform.position = Tools.ParseStringToVector3(data.P1T);
                }
                else
                {
                    _thistransform.FindChild("_P1(TOP)").transform.position = Tools.ParseStringToVector3(data.P1T);
                }
                IntersectBezierPoints[2] = new Vector3[data.IntersectBezierPointsString03.Length];
                for (int cnt = 0; cnt < data.IntersectBezierPointsString03.Length; cnt++)
                {
                    IntersectBezierPoints[2][cnt] = Tools.ParseStringToVector3(data.IntersectBezierPointsString03[cnt]);
                }
            }
            if (data.IntersectBezierPointsString04 != null)
            {
                if (!_thistransform.FindChild("_P1(DOWN)"))
                {
                    GameObject P1 = new GameObject("_P1(DOWN)");
                    P1.transform.SetParent(_thistransform);
                    P1.AddComponent<BasicGizmo>();
                    P1.transform.position = Tools.ParseStringToVector3(data.P1D);
                }
                else
                {
                    _thistransform.FindChild("_P1(DOWN)").transform.position = Tools.ParseStringToVector3(data.P1D);
                }
                IntersectBezierPoints[3] = new Vector3[data.IntersectBezierPointsString04.Length];
                for (int cnt = 0; cnt < data.IntersectBezierPointsString04.Length; cnt++)
                {
                    IntersectBezierPoints[3][cnt] = Tools.ParseStringToVector3(data.IntersectBezierPointsString04[cnt]);
                }
            }
        }
        _onLoad = false;
    }
}

