﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AIController : MonoBehaviour
{
    //Cached components
    private Transform _transform;
    private AIComponent[] _carAIComponent;
    public AIComponent[] CarList
    {
        get
        {
            return _carAIComponent;
        }
    }

    [Range(0, 100)]
    public int Cars = 0;

    public string[] CarsObjects;
    public float[] CarSpeed;
    public float[] CarStopDuration;
    [Range(0, 200)]
    public int Pedestrians = 0;

    private AIComponent[] _pedestrianComponent;
    public AIComponent[] PedestrianList
    {
        get
        {
            return _pedestrianComponent;
        }
    }

    private bool _started = false;

    void Awake()
    {
        _transform = transform;
    }

    public void StartAI()
    {
        _carAIComponent = new AIComponent[Cars];
        _pedestrianComponent = new AIComponent[Pedestrians];
        string str = "";
        GameObject go;
        int id = 0;
        //Cars : 

        //Set the waypoint's car array size
        foreach(Waypoint[] wpArray in ObjectManager.Waypoints)
        {
            foreach(Waypoint wp in wpArray)
                wp.InitCarList(Cars);
        }

        for (int cnt = 0; cnt < Cars; cnt++)
        {
            id = Random.Range(0, CarsObjects.Length);
            str = string.Format("Cars/{0}", CarsObjects[id]);
            go = (GameObject)GameObject.Instantiate(Resources.Load(str));
            go.name = go.name + "_" + cnt;
            go.transform.SetParent(_transform.FindChild("Cars"));

            _carAIComponent[cnt] = go.GetComponent<AIComponent>();
            _carAIComponent[cnt].MaxSpeed = CarSpeed[id];
            _carAIComponent[cnt].StopDuration = CarStopDuration[id];
        }

        //Pedestrians : 
        /*for (int cnt = 0; cnt < Cars; cnt++)
        {
            str = string.Format("Pedestrians/{0}", PedestriansObjects[Random.Range(0, PedestriansObjects.Length)].name);
            go = (GameObject)GameObject.Instantiate(Resources.Load(str));
            go.transform.SetParent(transform.FindChild("Pedestrians"));
        }*/
        _started = true;
    }
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        if (_started)
        {
            for (int cnt = 0; cnt < Cars; cnt++)
            {
                //Debug.Log("OnSpawn event: " + _carAIComponent[cnt].gameObject.name + " " + _carAIComponent[cnt].State);
                if (_carAIComponent[cnt].State == Enums.AIState.Stopped)
                {
                    _carAIComponent[cnt].State = Enums.AIState.OnSpawn;
                    _carAIComponent[cnt].gameObject.SetActive(false);

                    _carAIComponent[cnt].Spawn();

                    _carAIComponent[cnt].gameObject.SetActive(true);
                }
            }
        }
	}


    public void DestroyExistingAI()
    {
        if (_carAIComponent != null)
        {
            foreach (AIComponent ai in _carAIComponent)
            {
                GameObject.DestroyObject(ai.gameObject);
            }
        }
        if (_pedestrianComponent != null)
        {
            foreach (AIComponent ai in _pedestrianComponent)
            {
                GameObject.DestroyObject(ai.gameObject);
            }
        }
    }
}
