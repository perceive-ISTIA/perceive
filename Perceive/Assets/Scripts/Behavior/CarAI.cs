﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System;

public class CarAI : BaseAI
{
    #region Private members

    private const int LEFT = 0;
    private const int RIGHT = 1;
    private const int TOP = 2;
    private const int DOWN = 3;

    private GameObject _obj;
    private Transform _objTransform;
    private Transform _front;
    private Transform _back;
    private Road[] _roads;
    private int _bezierIndex = -1;
    private int _intersectBezierIndex = -1;
    private int _intersectionID = 0;
    private int _targetPointID = 0;
    private int _currentRoad = 0;
    private int _nextRoad = -1;
    private int _nextRoadStartPointID = 0;
    private bool _isClosest = false;
    private bool _waitBack = false;
    private Waypoint _oldWP;
    private Waypoint _tmpWP = null;
    private bool _isSplit = false;

    //Cache
    private Transform _wpTr;
    private Transform _wpNextTr;

    #endregion

    #region Properties
    private int _dirSide = -1;
    /// <summary>
    /// Get the side of the next destination
    /// </summary>
    public int DirSide
    {
        get
        {
            return _dirSide;
        }
    }

    private bool _wait = false;
    public bool Wait
    {
        get
        {
            return _wait;
        }
        set
        {
            _wait = value;
        }
    }

    private Enums.AIState _state;
    public Enums.AIState AIState
    {
        get
        {
            return _state;
        }
        set
        {
            _state = value;
        }
    }

    public string name
    {
        get
        {
            return _obj.transform.name;
        }
    }

    public Transform transform
    {
        get
        {
            return _objTransform;
        }
    }
    #endregion

    #region Constructor
    /// <summary>
    /// Constructor: Initialize values and choose a random location
    /// </summary>
    /// <param name="obj">Car gameobject</param>
    /// <param name="roads">Array of roads</param>
    /// <param name="front">Front car gameobject (used for intersections)</param>
    /// <param name="back">Back car gameobject (used for intersections)</param>
    public CarAI(GameObject obj, Road[] roads, GameObject front, GameObject back)
    {
        _obj = obj;
        _objTransform = _obj.transform;
        _front = front.transform;
        _back = back.transform;
        _roads = roads;

        Spawn();
        ObjectManager.Waypoints[_currentRoad][_targetPointID].AddCarAI(this);

        _objTransform.position = _roads[_currentRoad].Points[_targetPointID].transform.position;
        if (_targetPointID + 1 < _roads[_currentRoad].Points.Count)
            _objTransform.rotation = Quaternion.LookRotation(_roads[_currentRoad].Points[_targetPointID + 1].transform.position - _objTransform.position);
        else
            _state = Enums.AIState.Stopped;

    }
    #endregion

    public void Move()
    {
        throw new NotImplementedException();
    }

    public void Spawn()
    {
        _state = Enums.AIState.OnSpawn;

        _bezierIndex = -1;
        _intersectBezierIndex = -1;
        _intersectionID = 0;
        _targetPointID = 0;
        _currentRoad = 0;
        _nextRoad = -1;
        _nextRoadStartPointID = 0;
        _isClosest = false;
        _waitBack = false;
        _tmpWP = null;
        _isSplit = false;
        _wait = false;

        int[] roadIDs = new int[_roads.Length];
        List<int> waypointIDs = new List<int>();
        //Get the roads IDs
        for (int cnt = 0; cnt < _roads.Length; cnt++)
        {
            roadIDs[cnt] = cnt;
        }

        //Get a random road
        _currentRoad = UnityEngine.Random.Range(0, _roads.Length);
        for (int cnt = 0; cnt < _roads[_currentRoad].Points.Count; cnt++)
        {
            //If the selected waypoint is not an intersection
            if (ObjectManager.Waypoints[_currentRoad][cnt].Intersection == Enums.Intersection.None)
                waypointIDs.Add(cnt); //Add waypoint
        }

        bool ok = false;
        bool roadFull = false;
        do
        {
            if (roadFull)
            {
                //roadIDs.Remove(_currentRoad);
                //Remove the last occurence
                int[] temp = new int[roadIDs.Length - 1];
                int x = 0;
                for (int i = 0; i < roadIDs.Length; i ++ )
                {
                    if(roadIDs[i] != _currentRoad)
                    {
                        temp[x] = roadIDs[i];
                        x++;
                    }
                }
                roadIDs = new int[temp.Length];
                temp.CopyTo(roadIDs, 0);

                _currentRoad = roadIDs[UnityEngine.Random.Range(0, roadIDs.Length)];
                waypointIDs.Clear();
                waypointIDs = new List<int>();
                for (int cnt = 0; cnt < _roads[_currentRoad].Points.Count; cnt++)
                {
                    if (ObjectManager.Waypoints[_currentRoad][cnt].Intersection == Enums.Intersection.None)
                        waypointIDs.Add(cnt);
                }
                roadFull = false;
            }
            //Get a waypoint
            _targetPointID = waypointIDs[UnityEngine.Random.Range(0, waypointIDs.Count)];
            if (ObjectManager.Waypoints[_currentRoad][_targetPointID].CarCount == 0) //If the waypoint is full
            {
                if (!(_targetPointID + 1 < _roads[_currentRoad].Points.Count)) //Check if the next waypoint exists
                    ok = true;
                else
                {
                    if (ObjectManager.Waypoints[_currentRoad][_targetPointID + 1].CarCount == 0) //Check if the next waypoint is empty
                        ok = true;
                    else
                    {
                        //Looping through the existing AI
                        bool t = false;
                        foreach (CarAI ai in ObjectManager.Waypoints[_currentRoad][_targetPointID + 1].CarAIList)
                        {
                            if (ai == null)
                                break;
                            if (transform != null && ai.transform != null && ai.transform.position == transform.position) //Same position
                            {
                                t = true;
                                break;
                            }
                        }
                        if (t)
                        {
                            waypointIDs.Remove(_targetPointID);
                            if (waypointIDs.Count == 0)
                                roadFull = true;
                        }
                        else
                            ok = true;
                    }
                }
            }
            else
            {
                waypointIDs.Remove(_targetPointID);
                if (waypointIDs.Count == 0)
                    roadFull = true;
            }
        } while (!ok);
        ObjectManager.Waypoints[_currentRoad][_targetPointID].AddCarAI(this);


        _objTransform.position = _roads[_currentRoad].Points[_targetPointID].transform.position;
        if (_targetPointID + 1 < _roads[_currentRoad].Points.Count)
        {
            _objTransform.rotation = Quaternion.LookRotation(_roads[_currentRoad].Points[_targetPointID + 1].transform.position - _objTransform.position);
            _state = Enums.AIState.Moving;
        }
        else
            _state = Enums.AIState.Stopped;
    }

    public Vector3 PickDestination()
    {
        #region Get waypoints
        Waypoint wpNext;
        // + 1
        if (_targetPointID + 1 < _roads[_currentRoad].Points.Count)
        {
            if (!_isSplit)
                wpNext = ObjectManager.Waypoints[_currentRoad][_targetPointID + 1];
            else
            {
                if (_roads[_currentRoad].SplitPoints[_targetPointID + 1] == null)
                    wpNext = ObjectManager.Waypoints[_currentRoad][_targetPointID + 1];
                else
                    wpNext = ObjectManager.SplitPoints[_currentRoad][_targetPointID + 1];
            }
            _wpNextTr = wpNext.transform;
        }
        else
        {
            wpNext = null;
        }
        Waypoint wp;
        if (!_isSplit)
            wp = ObjectManager.Waypoints[_currentRoad][_targetPointID];
        else
        {
            if (_roads[_currentRoad].SplitPoints[_targetPointID] != null)
                wp = ObjectManager.SplitPoints[_currentRoad][_targetPointID];
            else
                wp = ObjectManager.Waypoints[_currentRoad][_targetPointID];
        }
        _wpTr = wp.transform;

        #endregion




        if (wp.Intersection == Enums.Intersection.None)
        {
            if (_bezierIndex == -1) //If there is not a Bezier process
            {
                if ((_wpTr.position - _objTransform.position).magnitude != 0f)
                    _objTransform.rotation = Quaternion.Slerp(Quaternion.LookRotation(_wpTr.position - _objTransform.position), _objTransform.rotation, Time.deltaTime * 2.0f);
                return _wpTr.position;
            }
            else //Bezier process
            {
                if ((wp.BezierPoints[_bezierIndex] - _objTransform.position).magnitude != 0f)
                    _objTransform.rotation = Quaternion.Slerp(Quaternion.LookRotation(wp.BezierPoints[_bezierIndex] - _objTransform.position), _objTransform.rotation, Time.deltaTime * 2.0f);
                return wp.BezierPoints[_bezierIndex];
            }
        }
        else
        {
            //Switching Intersections
            switch (wp.Intersection)
            {
                #region Lights
                case Enums.Intersection.Lights:
                    if (wp.DrawBezier && _bezierIndex >= 0)
                    {
                        if ((wp.BezierPoints[_bezierIndex] - _objTransform.position).magnitude != 0f)
                            _objTransform.rotation = Quaternion.Slerp(Quaternion.LookRotation(wp.BezierPoints[_bezierIndex] - _objTransform.position), _objTransform.rotation, Time.deltaTime * 2.0f);
                        return wp.BezierPoints[_bezierIndex];
                    }
                    if (_intersectBezierIndex == -1) //If the target is the light waypoint but the Bezier process is not in progress
                    {
                        if (_waitBack)
                        {
                            if (wp.DrawBezier)
                            {
                                //Detect the closest Bezier point
                                float dist = 90.0f;
                                for (int cnt = 0; cnt < wp.BezierPoints.Length; cnt++)
                                {
                                    if (Vector3.Distance(_front.position, wp.BezierPoints[cnt]) < dist)
                                    {
                                        dist = Vector3.Distance(_front.position, wp.BezierPoints[cnt]);
                                    }
                                    else
                                    {
                                        _bezierIndex = cnt - 1;
                                        return wp.BezierPoints[cnt - 1];
                                    }
                                }
                            }
                            else
                            {
                                if ((_wpNextTr.position - _objTransform.position).magnitude != 0f)
                                    _objTransform.rotation = Quaternion.Slerp(Quaternion.LookRotation(_wpNextTr.position - _objTransform.position), _objTransform.rotation, Time.deltaTime * 2.0f);
                                return _wpNextTr.position;
                            }
                        }
                        if ((_wpTr.position - _objTransform.position).magnitude != 0f)
                            _objTransform.rotation = Quaternion.Slerp(Quaternion.LookRotation(_wpTr.position - _objTransform.position), _objTransform.rotation, Time.deltaTime * 2.0f);
                        if (!_wait)
                            return _wpTr.position;
                        else
                            return _objTransform.position;
                    }
                    else //Intersection Bezier in progress
                    {
                        if (_waitBack)
                        {
                            if ((_wpTr.position - _objTransform.position).magnitude != 0f)
                                _objTransform.rotation = Quaternion.Slerp(Quaternion.LookRotation(_wpTr.position - _objTransform.position), _objTransform.rotation, Time.deltaTime * 2.0f);
                            return _wpTr.position;
                        }
                        if ((wp.IntersectBezierPoints[_intersectionID][_intersectBezierIndex] - _objTransform.position).magnitude != 0f)
                            _objTransform.rotation = Quaternion.Slerp(Quaternion.LookRotation(wp.IntersectBezierPoints[_intersectionID][_intersectBezierIndex] - _objTransform.position), _objTransform.rotation, Time.deltaTime * 2.0f);
                        return wp.IntersectBezierPoints[_intersectionID][_intersectBezierIndex];
                    }
                #endregion
                #region Stop
                case Enums.Intersection.Stop:
                    if (_intersectBezierIndex == -1) //If the target is the light waypoint but the Bezier process is not in progress
                    {
                        if (_waitBack)
                        {
                            if ((_wpNextTr.position - _objTransform.position).magnitude != 0f)
                                _objTransform.rotation = Quaternion.Slerp(Quaternion.LookRotation(_wpNextTr.position - _objTransform.position), _objTransform.rotation, Time.deltaTime * 2.0f);
                            return _wpNextTr.position;
                        }
                        if ((_wpTr.position - _objTransform.position).magnitude != 0f)
                            _objTransform.rotation = Quaternion.Slerp(Quaternion.LookRotation(_wpTr.position - _objTransform.position), _objTransform.rotation, Time.deltaTime * 2.0f);
                        if (!_wait)
                            return _wpTr.position;
                        else
                            return _objTransform.position;
                    }
                    else //Intersection Bezier in progress
                    {
                        if ((wp.IntersectBezierPoints[_intersectionID][_intersectBezierIndex] - _objTransform.position).magnitude != 0f)
                            _objTransform.rotation = Quaternion.Slerp(Quaternion.LookRotation(wp.IntersectBezierPoints[_intersectionID][_intersectBezierIndex] - _objTransform.position), _objTransform.rotation, Time.deltaTime * 2.0f);
                        return wp.IntersectBezierPoints[_intersectionID][_intersectBezierIndex];
                    }
                #endregion
                #region Single crossing
                case Enums.Intersection.SimpleCrossing:
                    if (_intersectBezierIndex == -1) //If the target is the light waypoint but the Bezier process is not in progress
                    {
                        if (_waitBack)
                        {
                            if ((_wpNextTr.position - _objTransform.position).magnitude != 0f)
                                _objTransform.rotation = Quaternion.Slerp(Quaternion.LookRotation(_wpNextTr.position - _objTransform.position), _objTransform.rotation, Time.deltaTime * 2.0f);
                            return _wpNextTr.position;
                        }
                        if ((_wpTr.position - _objTransform.position).magnitude != 0f)
                            _objTransform.rotation = Quaternion.Slerp(Quaternion.LookRotation(_wpTr.position - _objTransform.position), _objTransform.rotation, Time.deltaTime * 2.0f);
                        if (!_wait)
                            return _wpTr.position;
                        else
                            return _objTransform.position;
                    }
                    else //Intersection Bezier in progress
                    {
                        if ((wp.IntersectBezierPoints[_intersectionID][_intersectBezierIndex] - _objTransform.position).magnitude != 0f)
                            _objTransform.rotation = Quaternion.Slerp(Quaternion.LookRotation(wp.IntersectBezierPoints[_intersectionID][_intersectBezierIndex] - _objTransform.position), _objTransform.rotation, Time.deltaTime * 2.0f);
                        return wp.IntersectBezierPoints[_intersectionID][_intersectBezierIndex];
                    }
                #endregion
                default:
                    _objTransform.rotation = Quaternion.Slerp(Quaternion.LookRotation(_wpTr.position - _objTransform.position), _objTransform.rotation, Time.deltaTime * 2.0f);
                    return _wpTr.position;
            }
        }
    }



    public void CheckDestination()
    {
        Waypoint wp;
        if (!_isSplit)
        {
            wp = ObjectManager.Waypoints[_currentRoad][_targetPointID];//Get the waypoint script
        }
        else
        {
            if (_roads[_currentRoad].SplitPoints[_targetPointID] != null)
                wp = ObjectManager.SplitPoints[_currentRoad][_targetPointID];//Get the waypoint script
            else
                wp = ObjectManager.Waypoints[_currentRoad][_targetPointID];
        }
        _wpTr = wp.transform;
        bool found = false;
        List<GameObject> tempDest = new List<GameObject>();
        List<int> tempID = new List<int>();



        switch (wp.Intersection)
        {
            #region Lights
            case Enums.Intersection.Lights:
                if (_intersectBezierIndex == -1)
                {
                    for (int cnt = 0; cnt < 4; cnt++)
                    {
                        if (wp.IntersectionPoints[cnt] != null)
                        {
                            tempDest.Add(wp.IntersectionPoints[cnt]);
                            tempID.Add(cnt);
                            found = true;
                        }
                    }
                }
                else
                {
                    if (!_waitBack)
                    {
                        found = true;
                        tempDest = null;
                        tempID = null;
                    }
                }
                #region NoDirection
                if (!found)
                {
                    //Wait for the tr.pos
                    if (Vector3.Distance(_objTransform.position, _wpTr.position) < 0.5f && _waitBack)
                    {
                        _intersectBezierIndex = -1; //Reset the Bezier index
                        //_targetPointID++; //Switch to the next point
                        return;
                    }
                    if (Vector3.Distance(_back.position, _wpTr.position) < 1.5f && _waitBack)
                    {
                        //If not Bezier
                        if (!wp.DrawBezier)
                        {
                            if (_targetPointID + 1 < _roads[_currentRoad].Points.Count)
                            {
                                _targetPointID++;
                                _oldWP.RemoveCarAI(this);
                                if (!_isSplit)
                                    ObjectManager.Waypoints[_currentRoad][_targetPointID].AddCarAI(this);
                                else
                                {
                                    if (_roads[_currentRoad].SplitPoints[_targetPointID] != null)
                                    {
                                        ObjectManager.SplitPoints[_currentRoad][_targetPointID].AddCarAI(this);
                                    }
                                    else
                                    {
                                        //_isSplit = false;
                                        ObjectManager.Waypoints[_currentRoad][_targetPointID].AddCarAI(this);
                                    }
                                }
                            }
                            else
                                _state = Enums.AIState.Stopped;
                        }
                        else
                        {
                            _oldWP.RemoveCarAI(this);
                            wp.AddCarAI(this);
                        }
                        _waitBack = false;
                        return;
                    }
                    #region Bezier
                    if (wp.DrawBezier)
                    {
                        //Bezier in progress
                        if (_bezierIndex >= 0)
                        {
                            if (Vector3.Distance(_objTransform.position, wp.BezierPoints[_bezierIndex]) < 0.5f)
                            {
                                //If the current point is not the end of the array
                                if (_bezierIndex + 1 < wp.BezierPoints.Length)
                                {
                                    _bezierIndex++;
                                }
                                else
                                {
                                    //End of Bezier
                                    _bezierIndex = -1; //Reset the Bezier index
                                    _targetPointID++; //Switch to the next point

                                    wp.RemoveCarAI(this);
                                    if (!_isSplit)
                                    {
                                        ObjectManager.Waypoints[_currentRoad][_targetPointID].AddCarAI(this);
                                    }
                                    else
                                    {
                                        if (_roads[_currentRoad].SplitPoints[_targetPointID] != null)
                                        {
                                            ObjectManager.SplitPoints[_currentRoad][_targetPointID].AddCarAI(this);
                                        }
                                        else
                                        {
                                            //_isSplit = false;
                                            ObjectManager.Waypoints[_currentRoad][_targetPointID].AddCarAI(this);
                                        }
                                    }
                                }
                            }
                            return;
                        }
                    }
                    #endregion
                    if (!_waitBack)
                    {
                        if (Vector3.Distance(_objTransform.position, _wpTr.position) < 0.5f)
                        {
                            if (_targetPointID + 1 < _roads[_currentRoad].Points.Count)
                            {
                                _targetPointID++;
                                wp.RemoveCarAI(this);
                                //Debug.Log(_currentRoad + "   " + _targetPointID);
                                if (!_isSplit)
                                    ObjectManager.Waypoints[_currentRoad][_targetPointID].AddCarAI(this);
                                else
                                {
                                    if (_roads[_currentRoad].SplitPoints[_targetPointID] != null)
                                        ObjectManager.SplitPoints[_currentRoad][_targetPointID].AddCarAI(this);
                                    else
                                        ObjectManager.Waypoints[_currentRoad][_targetPointID].AddCarAI(this);
                                }
                            }
                            else
                                _state = Enums.AIState.Stopped;
                        }
                        return;
                    }
                    return;
                }
                #endregion
                else
                {
                    //Debug.Log(_nextRoad + "   " + _intersectBezierIndex);
                    #region Get the next destination
                    //Get the next destination a first:
                    if (!_wait && _intersectBezierIndex == -1 && _nextRoad == -1)
                    {
                        //Choosing a destination:
                        int rnd = UnityEngine.Random.Range(0, tempDest.Count);

                        _intersectionID = tempID[rnd]; //Set teh ID of the intersection
                        _dirSide = _intersectionID;
                        _nextRoad = tempDest[rnd].transform.parent.GetComponent<Road>().ID; //Set the next road index
                        _nextRoadStartPointID = int.Parse(tempDest[rnd].transform.name.Replace("_waypoint", ""));
                    }
                    #endregion


                    if (Vector3.Distance(/*_obj*/_front.position, _wpTr.position) < 1.5f && _intersectBezierIndex == -1)
                    {
                        //Do we have to stop or not ?
                        TrafficLightController tr = wp.IntersectionLightController;
                        switch (tr.GetState())
                        {
                            case Enums.LightState.Yellow:
                            case Enums.LightState.Red:
                                _state = Enums.AIState.IntersecLights;
                                _wait = true;
                                break;
                            case Enums.LightState.Green:
                                _wait = false;
                                break;
                        }
                        if (!_wait && CheckSafeCrossing(wp))
                            _intersectBezierIndex = 0;
                        return;
                    }

                    if (_wait)
                    {
                        TrafficLightController tr = wp.IntersectionLightController;
                        if (tr.GetState() == Enums.LightState.Green)
                            _wait = false;
                    }
                    if (_intersectBezierIndex >= 0 && !_wait && CheckSafeCrossing(wp) && !_waitBack)
                    {
                        if (Vector3.Distance(_objTransform.position, wp.IntersectBezierPoints[_intersectionID][_intersectBezierIndex]) < 0.5f)
                        {
                            //If the current point is not the end of the array
                            if (_intersectBezierIndex + 1 < wp.IntersectBezierPoints[_intersectionID].Length)
                            {
                                _intersectBezierIndex++;
                            }
                            else
                            {
                                //End of Bezier
                                //_intersectBezierIndex = -1; //Reset the Bezier index
                                _targetPointID = _nextRoadStartPointID; //Switch to the next point
                                _currentRoad = _nextRoad;
                                _nextRoad = -1;
                                _waitBack = true;
                                //Update the carcount
                                _oldWP = wp;
                            }
                        }
                        return;
                    }
                }
                break;
            #endregion
            #region Right
            case Enums.Intersection.Right:
                break;
            #endregion
            #region Stop
            case Enums.Intersection.Stop:
                _state = Enums.AIState.IntersecStop;
                if (_intersectBezierIndex == -1)
                {
                    for (int cnt = 0; cnt < 4; cnt++)
                    {
                        if (wp.IntersectionPoints[cnt] != null)
                        {
                            tempDest.Add(wp.IntersectionPoints[cnt]);
                            tempID.Add(cnt);
                            found = true;
                        }
                    }
                }
                else
                {
                    found = true;
                    tempDest = null;
                    tempID = null;
                }
                if (!found)
                {
                    //Debug.Log("not found");
                    if (Vector3.Distance(_back.position, _wpTr.position) < 1.5f && _waitBack)
                    {
                        if (_targetPointID + 1 < _roads[_currentRoad].Points.Count)
                        {
                            _targetPointID++;

                            _oldWP.RemoveCarAI(this);
                            if (!_isSplit)
                            {
                                ObjectManager.Waypoints[_currentRoad][_targetPointID].AddCarAI(this);
                            }
                            else
                            {
                                if (_roads[_currentRoad].SplitPoints[_targetPointID] != null)
                                    ObjectManager.SplitPoints[_currentRoad][_targetPointID].AddCarAI(this);
                                else
                                    ObjectManager.Waypoints[_currentRoad][_targetPointID].AddCarAI(this);
                            }

                            _waitBack = false;
                        }
                        else
                            _state = Enums.AIState.Stopped;
                        return;
                    }
                    if (!_waitBack)
                    {
                        if (Vector3.Distance(_objTransform.position, _wpTr.position) < 0.5f)
                        {
                            if (_targetPointID + 1 < _roads[_currentRoad].Points.Count)
                            {
                                _targetPointID++;

                                wp.RemoveCarAI(this);
                                if (!_isSplit)
                                {
                                    ObjectManager.Waypoints[_currentRoad][_targetPointID].AddCarAI(this);
                                }
                                else
                                {
                                    if (_roads[_currentRoad].SplitPoints[_targetPointID] != null)
                                    {
                                        ObjectManager.SplitPoints[_currentRoad][_targetPointID].AddCarAI(this);
                                    }
                                    else
                                    {
                                        //_isSplit = false;
                                        ObjectManager.Waypoints[_currentRoad][_targetPointID].AddCarAI(this);
                                    }
                                }
                            }
                            else
                                _state = Enums.AIState.Stopped;
                        }
                        return;
                    }
                    return;
                }
                #region Initial Stop
                if (Vector3.Distance(/*_obj*/_front.position, _wpTr.position) < 1.5f && _intersectBezierIndex == -1)
                {
                    if (!_wait && _nextRoad == -1)
                    {
                        //Choosing a destination:
                        int rnd = UnityEngine.Random.Range(0, tempDest.Count);

                        _intersectionID = tempID[rnd]; //Set teh ID of the intersection
                        _nextRoad = tempDest[rnd].transform.parent.GetComponent<Road>().ID; //Set the next road index
                        _nextRoadStartPointID = int.Parse(tempDest[rnd].transform.name.Replace("_waypoint", ""));

                        //Check if the target is the closest point or not
                        //1: Check if the target is the closest point
                        float targetDistance = Vector3.Distance(_wpTr.position, _roads[_nextRoad].Points[_nextRoadStartPointID].transform.position);
                        _isClosest = true;
                        foreach (GameObject obj in tempDest)
                        {
                            if (obj != _roads[_nextRoad].Points[_nextRoadStartPointID]) //Check if the obj is not the next point
                            {
                                if (Vector3.Distance(obj.transform.position, _wpTr.position) < targetDistance)
                                {
                                    _isClosest = false;
                                    break;
                                }
                            }
                        }
                        _wait = true;
                    }
                    if (CheckSafeCrossing(wp) && !_wait)
                    {
                        _intersectBezierIndex = 0;
                    }
                    return;
                }
                #endregion
                //If there is an intersect Bezier in progress
                #region Intersection Bezier
                if (_intersectBezierIndex >= 0 && CheckSafeCrossing(wp) && !_wait)
                {
                    //Debug.Log(_intersectionID + "|" + wp.IntersectBezierPoints.Length + "|" + _intersectBezierIndex);
                    if (Vector3.Distance(_objTransform.position, wp.IntersectBezierPoints[_intersectionID][_intersectBezierIndex]) < 0.5f)
                    {
                        //If the current point is not the end of the array
                        if (_intersectBezierIndex + 1 < wp.IntersectBezierPoints[_intersectionID].Length)
                        {
                            _intersectBezierIndex++;
                        }
                        else
                        {
                            //End of Bezier
                            _intersectBezierIndex = -1; //Reset the Bezier index
                            _targetPointID = _nextRoadStartPointID; //Switch to the next point
                            _currentRoad = _nextRoad;
                            _nextRoad = -1;
                            _waitBack = true;
                            //Update the carcount
                            _oldWP = wp;
                        }
                    }
                    return;
                }
                #endregion
                break;
            #endregion
            #region Split
            case Enums.Intersection.Split2:
                /*If the current waypoint is a Split and if there is no Split in progress => start
                 * Else, there is a split in progress so this point is the end of the split
                */
                if (Vector3.Distance(_objTransform.position, _wpTr.position) < 0.5f && _bezierIndex == -1)
                {
                    if (!_isSplit)
                    {
                        //Choose a way :
                        if (UnityEngine.Random.Range(0, 2) == 1) //Normal
                        {
                            _targetPointID++;
                            _isSplit = true;
                        }
                        else
                        {
                            _targetPointID++;
                            _isSplit = false;
                        }
                    }
                    else
                    {
                        _targetPointID++;
                        _isSplit = false;
                    }
                }
                break;
            #endregion
            #region Simple crossing
            case Enums.Intersection.SimpleCrossing:
                //Get the next destination :
                if (_intersectBezierIndex == -1)
                {
                    for (int cnt = 0; cnt < 4; cnt++)
                    {
                        if (wp.IntersectionPoints[cnt] != null)
                        {
                            tempDest.Add(wp.IntersectionPoints[cnt]);
                            tempID.Add(cnt);
                            found = true;
                        }
                    }
                }
                if (found)
                {
                    #region Get the next destination
                    if (_intersectBezierIndex == -1 && _nextRoad == -1)
                    {
                        //If the current point is not the end of the road (=> Choose between turn or continue)
                        if (wp.State != Enums.WaypointState.End)
                        {
                            //Get the random destination:
                            int rnd = UnityEngine.Random.Range(-1, tempDest.Count); // [-1,DestCount[ (with -1 = don't change the road)
                            if (rnd != -1)
                            {
                                _intersectionID = tempID[rnd]; //Set teh ID of the intersection
                                _dirSide = _intersectionID;
                                _nextRoad = tempDest[rnd].transform.parent.GetComponent<Road>().ID; //Set the next road index
                                _nextRoadStartPointID = int.Parse(tempDest[rnd].transform.name.Replace("_waypoint", ""));
                                if (_state != Enums.AIState.IntersecSimpleCrossing)
                                    _state = Enums.AIState.IntersecSimpleCrossing;
                            }
                            else
                            {
                                _nextRoad = _currentRoad;
                                _nextRoadStartPointID = _targetPointID + 1;
                            }
                        }
                        else //Have to turn
                        {
                            int rnd = UnityEngine.Random.Range(0, tempDest.Count); // [-1,DestCount[ (with -1 = don't change the road)
                            _intersectionID = tempID[rnd]; //Set teh ID of the intersection
                            _dirSide = _intersectionID;
                            _nextRoad = tempDest[rnd].transform.parent.GetComponent<Road>().ID; //Set the next road index
                            _nextRoadStartPointID = int.Parse(tempDest[rnd].transform.name.Replace("_waypoint", ""));
                        }
                    }
                    #endregion

                    #region Initial Stop
                    //Check if the interction is reached
                    if (Vector3.Distance(_front.position, _wpTr.position) < 1.5f && _intersectBezierIndex == -1)
                    {
                        if (_nextRoad != _currentRoad)
                        {
                            if (CheckSafeCrossing(wp) && !_wait)
                                _intersectBezierIndex = 0;
                            else
                                return;
                        }
                        else
                        {
                            _nextRoad = -1;
                            _nextRoadStartPointID = -1;
                            _targetPointID++;
                            wp.RemoveCarAI(this);

                            if (!_isSplit)
                                ObjectManager.Waypoints[_currentRoad][_targetPointID].AddCarAI(this);
                            else
                            {
                                if (_roads[_currentRoad].SplitPoints[_targetPointID] != null)
                                    ObjectManager.SplitPoints[_currentRoad][_targetPointID].AddCarAI(this);
                                else
                                    ObjectManager.Waypoints[_currentRoad][_targetPointID].AddCarAI(this);
                            }
                        }
                        return;
                    }
                    #endregion
                }
                else
                {
                    //Check if the next destination is reached (with the _back transform)
                    if (Vector3.Distance(_back.position, _wpTr.position) < 1.5f && _waitBack)
                    {
                        //If not Bezier
                        if (!wp.DrawBezier)
                        {
                            if (_targetPointID + 1 < _roads[_currentRoad].Points.Count)
                            {
                                _state = Enums.AIState.Moving;
                                _targetPointID++;
                                _oldWP.RemoveCarAI(this);
                                if (!_isSplit)
                                    ObjectManager.Waypoints[_currentRoad][_targetPointID].AddCarAI(this);
                                else
                                {
                                    if (_roads[_currentRoad].SplitPoints[_targetPointID] != null)
                                        ObjectManager.SplitPoints[_currentRoad][_targetPointID].AddCarAI(this);
                                    else
                                        ObjectManager.Waypoints[_currentRoad][_targetPointID].AddCarAI(this);
                                }
                            }
                            else
                                _state = Enums.AIState.Stopped;
                        }
                        else
                        {
                            _state = Enums.AIState.Moving;
                            _oldWP.RemoveCarAI(this);
                            wp.AddCarAI(this);
                        }
                        _waitBack = false;
                        return;
                    }

                    #region Bezier
                    if (wp.DrawBezier)
                    {
                        //Bezier in progress
                        if (_bezierIndex >= 0)
                        {
                            if (Vector3.Distance(_objTransform.position, wp.BezierPoints[_bezierIndex]) < 0.5f)
                            {
                                //If the current point is not the end of the array
                                if (_bezierIndex + 1 < wp.BezierPoints.Length)
                                    _bezierIndex++;
                                else
                                {
                                    //End of Bezier
                                    _bezierIndex = -1; //Reset the Bezier index
                                    _targetPointID++; //Switch to the next point

                                    wp.RemoveCarAI(this);
                                    if (!_isSplit)
                                        ObjectManager.Waypoints[_currentRoad][_targetPointID].AddCarAI(this);
                                    else
                                    {
                                        if (_roads[_currentRoad].SplitPoints[_targetPointID] != null)
                                            ObjectManager.SplitPoints[_currentRoad][_targetPointID].AddCarAI(this);
                                        else
                                            ObjectManager.Waypoints[_currentRoad][_targetPointID].AddCarAI(this);
                                    }
                                }
                            }
                            return;
                        }
                    }
                    #endregion
                }

                #region Intersection Bezier
                if (_intersectBezierIndex >= 0 && CheckSafeCrossing(wp))
                {
                    if (Vector3.Distance(_objTransform.position, wp.IntersectBezierPoints[_intersectionID][_intersectBezierIndex]) < 0.5f)
                    {
                        //If the current point is not the end of the array
                        if (_intersectBezierIndex + 1 < wp.IntersectBezierPoints[_intersectionID].Length)
                            _intersectBezierIndex++;
                        else
                        {
                            //End of Bezier
                            _intersectBezierIndex = -1; //Reset the Bezier index
                            _targetPointID = _nextRoadStartPointID; //Switch to the next point
                            _currentRoad = _nextRoad;
                            _nextRoad = -1;
                            _waitBack = true;
                            //Update the carcount
                            _oldWP = wp;
                        }
                    }
                    return;
                }
                #endregion
                break;
            #endregion
            #region None
            case Enums.Intersection.None:
                //Check if the target is reached (no bezier in progress)
                if (Vector3.Distance(_objTransform.position, _wpTr.position) < 0.5f && _bezierIndex == -1)
                {
                    if (wp.DrawBezier && wp.BezierPoints.Length > 0)
                    {
                        _bezierIndex = 0;
                    }
                    else
                    {
                        if (!_wait)
                        {
                            if (_targetPointID + 1 < wp.Total)
                            {
                                _targetPointID++;
                                wp.RemoveCarAI(this);
                                if (!_isSplit)
                                    ObjectManager.Waypoints[_currentRoad][_targetPointID].AddCarAI(this);
                                else
                                {
                                    if (_roads[_currentRoad].SplitPoints[_targetPointID] != null)
                                        ObjectManager.SplitPoints[_currentRoad][_targetPointID].AddCarAI(this);
                                    else
                                    {
                                        //_isSplit = false;
                                        ObjectManager.Waypoints[_currentRoad][_targetPointID].AddCarAI(this);
                                    }
                                }
                            }
                            else
                                _state = Enums.AIState.Stopped;
                        }
                    }
                    return;
                }
                //Bezier in progress
                if (_bezierIndex >= 0)
                {
                    if (Vector3.Distance(_objTransform.position, wp.BezierPoints[_bezierIndex]) < 0.5f)
                    {
                        //If the current point is not the end of the array
                        if (_bezierIndex + 1 < wp.BezierPoints.Length)
                            _bezierIndex++;
                        else
                        {
                            //End of Bezier
                            _bezierIndex = -1; //Reset the Bezier index
                            _targetPointID++; //Switch to the next point

                            wp.RemoveCarAI(this);
                            if (!_isSplit)
                                ObjectManager.Waypoints[_currentRoad][_targetPointID].AddCarAI(this);
                            else
                            {
                                if (_roads[_currentRoad].SplitPoints[_targetPointID] != null)
                                    ObjectManager.SplitPoints[_currentRoad][_targetPointID].AddCarAI(this);
                                else
                                {
                                    //_isSplit = false;
                                    ObjectManager.Waypoints[_currentRoad][_targetPointID].AddCarAI(this);
                                }
                            }
                        }
                    }
                    return;
                }
                break;
            #endregion
        }
    }

    public Vector3 Direction()
    {
        throw new System.NotImplementedException();
    }

    /// <summary>
    /// Return true if the crossing is safe, false if not
    /// </summary>
    /// <param name="wp"></param>
    /// <returns></returns>
    private bool CheckSafeCrossing(Waypoint wp)
    {
        switch (wp.Intersection)
        {
            #region Lights
            case Enums.Intersection.Lights:
                if (wp.IntersectionPoints[2] != null) //If the destination is left and there is a front road
                {
                    //Check if there is a car in the front line :
                    if (wp.IntersectionPoints[2].transform.parent.GetComponent<Road>().OtherSide != null)
                    {
                        Road otherSide = wp.IntersectionPoints[2].transform.parent.GetComponent<Road>().OtherSide.GetComponent<Road>(); //Get the other side
                        Waypoint facingWP = null;
                        //Get the correct waypoint
                        float dist = 1000f;
                        foreach (GameObject otherwp in otherSide.Points)
                        {
                            if (otherwp.GetComponent<Waypoint>().Intersection == Enums.Intersection.Lights)
                            {
                                if (Vector3.Distance(otherwp.transform.position, wp.IntersectionPoints[2].transform.position) < dist)
                                {
                                    dist = Vector3.Distance(otherwp.transform.position, wp.IntersectionPoints[2].transform.position);
                                    facingWP = otherwp.GetComponent<Waypoint>();
                                }
                            }
                        }

                        if (facingWP != null && facingWP.CarCount > 0)
                        {
                            //Browing existing AI in the selected waypoint
                            foreach (CarAI car in facingWP.CarAIList)
                            {
                                if (car == null)
                                    return true;
                                if (car.DirSide == 2 && _dirSide == 0) //If Target(Top) and This(Left)
                                {
                                    _state = Enums.AIState.IntersecLights;
                                    _wait = true;
                                    return false;
                                }
                                if (car.DirSide == 1 && _dirSide == 0) //If Target(Right) and This(Left)
                                {
                                    _state = Enums.AIState.IntersecLights;
                                    _wait = true;
                                    return false;
                                }
                            }
                        }
                    }
                    else
                        return true;
                }
                return true;
            #endregion
            #region Stop
            case Enums.Intersection.Stop:
                //Check if the crossing is safe
                if (_isClosest)
                {
                    //2.1: If the target point is the closest, we don't have to llok ath both sides of the road to decide if the crossing is safe
                    if (ObjectManager.Waypoints[_nextRoad][_nextRoadStartPointID].CarCount != 0)
                    {
                        _state = Enums.AIState.IntersecStop;
                        _wait = true;
                        return false;
                    }
                    else
                        return true;
                }
                else
                {
                    //2.2: If the point is not the closest, we have to check both sides of the road to decide if the crossing is safe
                    uint r1Count = 0;
                    uint r2Count = 0;
                    if (wp.IntersectionPoints[0] != null)
                        r1Count = wp.IntersectionPoints[0].GetComponent<Waypoint>().CarCount; // Left
                    if (wp.IntersectionPoints[1] != null)
                        r2Count = wp.IntersectionPoints[1].GetComponent<Waypoint>().CarCount; //Right

                    if (r1Count != 0 || r2Count != 0)
                    {
                        _state = Enums.AIState.IntersecStop;
                        _wait = true;
                        return false;
                    }
                    else
                        return true;
                }
            #endregion
            #region Simple crossing
            case Enums.Intersection.SimpleCrossing:
                //Check if there is another side, else, no need of verification, we can turn
                if (wp.transform.parent.GetComponent<Road>().OtherSide != null)
                {
                    if (_intersectionID == LEFT) //If the intersection is left => Check the other side
                    {
                        //Checking if the 
                        if (_tmpWP == null)
                        {
                            GameObject otherSide = wp.transform.parent.GetComponent<Road>().OtherSide;
                            Waypoint tmpWP;
                            foreach (Transform tr in otherSide.transform)
                            {
                                tmpWP = tr.GetComponent<Waypoint>();
                                if (tmpWP.Intersection == Enums.Intersection.SimpleCrossing)
                                {
                                    //Debug.Log("Right name: " + tmpWP.transform.name);
                                    //If the current tr has the selected intersection
                                    if (tmpWP.IntersectionPoints[RIGHT] != null && tmpWP.IntersectionPoints[RIGHT] == wp.IntersectionPoints[LEFT])
                                    {
                                        _tmpWP = tmpWP;
                                        if (tmpWP.CarCount > 0)
                                        {
                                            _wait = true;
                                            return false;
                                        }
                                        else
                                            return true;
                                    }
                                }
                            }
                        }
                        else
                            if (_tmpWP.CarCount > 0)
                            {
                                _wait = true;
                                return false;
                            }
                            else
                                return true;
                        return true;
                    }
                    else
                        return true;
                }
                else
                {
                    Debug.Log("return true");
                    return true;
                }
            #endregion
            default:
                _wait = true;
                return false;
        }
    }

    public void WaitSeconds(float seconds)
    {
        throw new System.NotImplementedException();
    }

    public void WaitFormMove()
    {
        throw new System.NotImplementedException();
    }


    IEnumerator BaseAI.WaitSeconds(float seconds)
    {
        throw new System.NotImplementedException();
    }
}
