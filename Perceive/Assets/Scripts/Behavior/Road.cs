﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System.IO;
using System.Xml.Serialization;
using System.Linq;

//[ExecuteInEditMode]
public class Road : MonoBehaviour
{
    public int ID;
    private int _pointsCount = -1;
    public List<GameObject> Points = new List<GameObject>();
    public List<GameObject> SplitPoints = new List<GameObject>();
    public GameObject OtherSide;

    private Transform _thisTransform;

    void Awake()
    {
        _thisTransform = transform;
    }

    void Start()
    {
        if (_thisTransform.tag != "Road")
            _thisTransform.tag = "Road";
        _pointsCount = -1;
        Load();
    }

    public void InitWaypoints()
    {
        if (_thisTransform == null)
            _thisTransform = transform;
        //Calculate Total:
        int i = 0;
        foreach (Transform tr in _thisTransform)
        {
            if (!tr.name.Contains("Split"))
                i++;
        }

        foreach (Transform tr in _thisTransform)
        {
            tr.GetComponent<Waypoint>().Total = i;
            tr.GetComponent<Waypoint>().InitWaypoint();
        }
    }

    public void Refresh()
    {
        foreach (Transform tr in _thisTransform)
        {
            tr.GetComponent<Waypoint>().Refresh();
        }
    }

    public void Load()
    {
        //Load data:
        RoadFile file = LoadFileToData();
        if (file.Data.Length == _thisTransform.childCount)
        {
            foreach (Transform child in _thisTransform)
            {
                for (int cnt = 0; cnt < file.Data.Length; cnt++)
                {
                    if (file.Data[cnt].Name == child.transform.name)
                    {
                        child.GetComponent<Waypoint>().LoadData(file.Data[cnt]);
                        break;
                    }
                }
            }
        }
        else
            Debug.LogError("Wrong road file !");
    }

    public void UpdateList()
    {
        
        if (Points.Count != _pointsCount)
        {
            _pointsCount = 0;
            Points = new List<GameObject>();
            SplitPoints = new List<GameObject>();
            foreach (Transform tr in _thisTransform)
            {
                //Get ID:
                if (tr.name.Contains("Split"))
                    SplitPoints.Add(tr.gameObject);
                else
                {
                    _pointsCount++;
                    Points.Add(tr.gameObject);
                    bool found = false;
                    foreach (Transform spTr in _thisTransform)
                    {
                        if(spTr.name.Contains(string.Format("_waypointSplit{0}_", tr.name.Replace("_waypoint", ""))))
                        {
                            found = true;
                            break;
                        }
                    }
                    if(!found)
                        SplitPoints.Add(null);
                }
            }
        }
    }


    public void SaveDataToFile()
    {
        if (!Directory.Exists(Application.dataPath + "\\Roads"))
            Directory.CreateDirectory(Application.dataPath + "\\Roads");
        RoadFile file = new RoadFile();
        file.Data = new GizmoData[_thisTransform.childCount];
        for(int cnt = 0; cnt < file.Data.Length; cnt++)
        {
            file.Data[cnt] = _thisTransform.GetChild(cnt).GetComponent<Waypoint>().GetSerializableData();
        }
        XmlSerializer xml = new XmlSerializer(typeof(RoadFile));
        using (StreamWriter stream = new StreamWriter(string.Format("{0}\\Roads\\{1}.xml", Application.dataPath, _thisTransform.name), false, System.Text.Encoding.GetEncoding("UTF-8")))
        {
            xml.Serialize(stream, file);
            stream.Close();
        }
    }

    public void SaveDataToLocal()
    {
        RoadFile file = new RoadFile();
        file.Data = new GizmoData[_thisTransform.childCount];
        for (int cnt = 0; cnt < file.Data.Length; cnt++)
        {
            file.Data[cnt] = _thisTransform.GetChild(cnt).GetComponent<Waypoint>().GetSerializableData();
        }
        XmlSerializer xml = new XmlSerializer(typeof(RoadFile));
        using (StreamWriter stream = new StreamWriter(string.Format("\\Roads\\{0}.xml", _thisTransform.name), false, System.Text.Encoding.GetEncoding("UTF-8")))
        {
            xml.Serialize(stream, file);
            stream.Close();
        }
    }

    private RoadFile LoadFileToData()
    {
        RoadFile file = new RoadFile();
        XmlSerializer serializer = new XmlSerializer(typeof(RoadFile));
        using (StreamReader stream = new StreamReader(string.Format("{0}\\Roads\\{1}.xml", Application.dataPath, _thisTransform.name), System.Text.Encoding.GetEncoding("UTF-8")))
        {
            file = serializer.Deserialize(stream) as RoadFile;
            stream.Close();
        }
        return file;
    }

    public Waypoint GetBeginWp()
    {
        var wp = this.GetComponentsInChildren<Waypoint>().Where(wayp => wayp.State == Enums.WaypointState.Begin).ToList();
        if (wp.Count > 0)
            return wp[0];
        return null;
    }

    public Waypoint GetEndWp()
    {
        var wp = this.GetComponentsInChildren<Waypoint>().Where(wayp => wayp.State == Enums.WaypointState.End).ToList();
        Debug.Log(wp.Count);
        if (wp.Count > 0)
            return wp[0];
        return null;
    }
}
