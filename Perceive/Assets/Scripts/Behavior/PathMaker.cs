﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Collections;
using System.Collections.Generic;

#if UNITY_EDITOR
public class WaypointInitMenu : EditorWindow
{
    [MenuItem("Tools/AI/Init waypoints")]
    static void Init()
    {
        foreach(GameObject tr in GameObject.FindGameObjectsWithTag("Road"))
        {
            Debug.Log(tr.name);
            tr.GetComponent<Road>().InitWaypoints();
        }
    }
    [MenuItem("Tools/AI/Load All")]
    static void LoadAll()
    {
        foreach (GameObject tr in GameObject.FindGameObjectsWithTag("Road"))
        {
            tr.GetComponent<Road>().Refresh();
            tr.GetComponent<Road>().Load();
            SceneView.RepaintAll();
        }
        Debug.Log("All data have been loaded!");
    }

    [MenuItem("Tools/AI/Save All")]
    static void saveAll()
    {
        foreach (GameObject tr in GameObject.FindGameObjectsWithTag("Road"))
        {
            tr.GetComponent<Road>().UpdateList();
            tr.GetComponent<Road>().SaveDataToFile();
        }
        Debug.Log("All data have been saved!");
    }
}
#endif

#if UNITY_EDITOR
[CustomEditor(typeof(Road))]
public class PathMaker : Editor
{
    public Road _target;


    private Vector2 _scroll;
    private bool _onSplit = false;
    private int _splitID = -1;
    private int _childCount = 0;

    void OnEnable()
    {
        _target = (Road)target;
    }

    public override void OnInspectorGUI()
    {
        GUILayout.BeginVertical();
        GUILayout.Label("Road maker");

        _target.ID = EditorGUILayout.IntField("Road ID", _target.ID);

        _target.OtherSide = (GameObject)EditorGUILayout.ObjectField("Other side road", _target.OtherSide, typeof(GameObject));

        EditorGUILayout.LabelField(string.Format("Total waypoints: {0}", _target.Points.Count), EditorStyles.boldLabel);
        _scroll = EditorGUILayout.BeginScrollView(_scroll, GUILayout.Height(100));
        bool updateList = false;
        foreach(GameObject go in _target.Points)
        {
            if (go == null)
                updateList = true;
            else
                EditorGUILayout.LabelField(go.GetComponent<Waypoint>().ToString(), EditorStyles.miniBoldLabel);
        }
        EditorGUILayout.EndScrollView();

        if(updateList)
        {
            _target.UpdateList();
        }

        if(GUILayout.Button(string.Format("Save data to File \"{0}\"", _target.transform.name)))
        {
            _target.UpdateList();
            _target.SaveDataToFile();
        }
        if (GUILayout.Button("Save data to local fields"))
        {
            _target.SaveDataToLocal();
        }
        if (GUILayout.Button("Load data from file"))
        {
            _target.Refresh();
            _target.Load();
            SceneView.RepaintAll();
        }

        GUILayout.EndVertical();
    }

    public void CheckChilds(Transform baseTr)
    {
        if(_childCount != baseTr.GetComponent<Road>().Points.Count)
        {
            baseTr.GetComponent<Road>().Points = new List<GameObject>();
            baseTr.GetComponent<Road>().SplitPoints = new List<GameObject>();
            _childCount = 0;
            _splitID = -1;
            _onSplit = false;
            bool currentBegin = false;
            foreach(Transform tr in baseTr)
            {
                if (!tr.name.Contains("Split"))
                {
                    currentBegin = false;
                    if (tr.GetComponent<Waypoint>().Intersection == Enums.Intersection.Split2 && _splitID == -1)
                    {
                        _onSplit = true;
                        _splitID = 0;
                        currentBegin = true;
                    }
                    if (tr.GetComponent<Waypoint>().Intersection == Enums.Intersection.Split2 && _onSplit && !currentBegin)
                    {
                        _onSplit = false;
                        Debug.Log("reset");
                        _splitID = -1;
                    }

                    baseTr.GetComponent<Road>().Points.Add(tr.gameObject);
                    baseTr.GetComponent<Road>().SplitPoints.Add(null);
                    _childCount++;
                }
                else
                {
                    baseTr.GetComponent<Road>().SplitPoints.Add(tr.gameObject);
                    if (_onSplit)
                        _splitID++;
                    else
                        _splitID = -1;
                }
            }
        }
    }

    void OnSceneGUI()
    {
        if (Event.current.type == EventType.MouseDown && Selection.gameObjects.Length == 1 && Selection.gameObjects[0].transform == _target.transform && Event.current.shift)
        {
            Ray ray = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 1000))
            {
                Transform baseTr = Selection.gameObjects[0].transform;
                CheckChilds(baseTr);
                GameObject splitObj = null;
                if(_onSplit)
                {
                    Debug.Log("Split add");
                    splitObj = new GameObject();
                    splitObj.transform.SetParent(baseTr);
                    splitObj.transform.position = hit.point + new Vector3(0f, 1f, 0f);
                    splitObj.AddComponent<Waypoint>();
                    if(_splitID == 0)
                        splitObj.GetComponent<Waypoint>().SplitState = Enums.SplitState.Begin;
                    else
                        splitObj.GetComponent<Waypoint>().SplitState = Enums.SplitState.Split;
                    splitObj.GetComponent<Waypoint>().State = Enums.WaypointState.None;
                    splitObj.GetComponent<Waypoint>().ID = baseTr.GetComponent<Road>().Points.Count;
                    splitObj.GetComponent<Waypoint>().SplitID = _splitID;
                    _splitID++;
                }
                GameObject obj = new GameObject();
                obj.transform.SetParent(baseTr);
                obj.transform.position = hit.point + new Vector3(0f, 1f, 0f);
                Waypoint wp = obj.AddComponent<Waypoint>();
                if (_splitID >= 0)
                    wp.SplitID = _splitID - 1;
                wp.ID = baseTr.GetComponent<Road>().Points.Count;

                //Set the waypoint state (begining or end of the road
                if (baseTr.childCount == 1)
                    wp.State = Enums.WaypointState.Begin;
                else
                    wp.State = Enums.WaypointState.End;
                baseTr.GetComponent<Road>().Points.Add(obj);
                baseTr.GetComponent<Road>().SplitPoints.Add(splitObj);
                wp.Total = baseTr.GetComponent<Road>().Points.Count;
                if(splitObj != null)
                    splitObj.GetComponent<Waypoint>().Total = baseTr.GetComponent<Road>().Points.Count;
                if (baseTr.childCount > 1)
                {
                    foreach (Transform tr in baseTr)
                    {
                        if (!tr.name.Contains("Split"))
                        {
                            if (int.Parse(tr.name.Replace("_waypoint", "")) == baseTr.GetComponent<Road>().Points.Count - 2 && int.Parse(tr.name.Replace("_waypoint", "")) != 0)
                                tr.GetComponent<Waypoint>().State = Enums.WaypointState.None;
                            tr.GetComponent<Waypoint>().Total = baseTr.GetComponent<Road>().Points.Count;
                        }
                        else
                            tr.GetComponent<Waypoint>().Total = baseTr.GetComponent<Road>().Points.Count;
                    }
                }
            }
        }
    }
}
#endif