﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class AIComponent : MonoBehaviour
{
    private BaseAI _ai;
    public Enums.AIState State
    {
        get
        {
            return _ai.AIState;
        }
        set
        {
            _ai.AIState = value;
        }
    }
    public Enums.AIType Type;
    [Range(0f, 200f)]
    public float MaxSpeed = 50f;
    private float _currentSpeed;
    private Transform _thisTransform;
    public float Speed
    {
        get
        {
            return _currentSpeed;
        }
        set
        {
            _currentSpeed = value;
        }
    }
    [Range(0.5f, 10f)]
    public float StopDuration = 2.0f;

    private float _speed;
    private bool _waitTime = false; //Timer wait

    private int collisionMask = 0;

    private List<float> _accelerationCurve;
    private float _tick = 0.1f;
    private float _startTime;


    void Awake()
    {
        _thisTransform = transform;
    }

    void Start()
    {
        _speed = MaxSpeed;
        _accelerationCurve = Tools.CalculateSpeedCurve(_speed);

        switch (Type)
        {
            case Enums.AIType.Pedestrian:
                _ai = new PedestrianAI(gameObject);
                break;
            case Enums.AIType.Vehicle:
                _ai = new CarAI(gameObject, ObjectManager.Roads, _thisTransform.FindChild("Front").gameObject, _thisTransform.FindChild("Back").gameObject);
                break;
        }
        _startTime = Time.time;
    }

    public void Spawn()
    {
        _ai.Spawn();
        _waitTime = false;
    }

    void FixedUpdate()
    {
        if (_speed != MaxSpeed)
        {
            _speed = MaxSpeed;
            _accelerationCurve = Tools.CalculateSpeedCurve(_speed);
        }
        if (_ai.AIState != Enums.AIState.Stopped && !_waitTime && _ai.AIState != Enums.AIState.OnSpawn)
        {
            if (_ai.Wait)
            {
                if (_ai.AIState == Enums.AIState.IntersecSimpleCrossing)
                    StartCoroutine(WaitAI(0.5f));
                else
                    StartCoroutine(WaitAI(1.0f));
            }
            else
            {
                _ai.CheckDestination();
                _currentSpeed = GetSpeed(Time.time - _startTime);
                _thisTransform.position = Vector3.MoveTowards(_thisTransform.position, _ai.PickDestination(), _currentSpeed);
            }
        }
    }

    private float GetSpeed(float time)
    {
        if (collisionMask != 0)
            return 0.0f;
        if (time > _tick * (_accelerationCurve.Count - 1))
            return _accelerationCurve[_accelerationCurve.Count - 1];
        return _accelerationCurve[(int)(time / _tick)];
    }


    private IEnumerator WaitAI(float seconds)
    {
        _waitTime = true;
        //Debug.Log("Wait");
        yield return new WaitForSeconds(seconds);
        _waitTime = false;
        _startTime = Time.time;
        _ai.Wait = false;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.transform.name == "BackCollider")
        {
            collisionMask = collisionMask | 0x01;

            //Check if the collision is not a collision due tu the pop:
            if (other.transform.parent.position == _thisTransform.position)
            {
                //Change the spot: 
                switch (Type)
                {
                    case Enums.AIType.Vehicle:
                        _ai = new CarAI(gameObject, ObjectManager.Roads,_thisTransform.FindChild("Front").gameObject, _thisTransform.FindChild("Back").gameObject); //Reset the _ai
                        collisionMask = 0;
                        break;
                    case Enums.AIType.Pedestrian:
                        break;
                }
            }
        }
    }
    void OnTriggerExit(Collider other)
    {
        if (other.transform.name == "BackCollider" && (collisionMask | 0x01) == 1)
        {
            collisionMask = collisionMask & 0x0E; //Reset the last bit
            _startTime = Time.time;
        }
    }

}
