﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SceneLoader : MonoBehaviour
{
    public Slider Progress;
    public string[] Scenes;

    private bool _once = false;
	// Use this for initialization
	void Start ()
    {
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (!_once)
        {
            StartCoroutine(LoadLevel("mainScene"));
            _once = true;
        }
	}


    IEnumerator LoadLevel(string sceneName)
    {
        AsyncOperation async = Application.LoadLevelAdditiveAsync(sceneName);
        async.allowSceneActivation = false;
        while (async.progress < 0.9f)
        {
            Progress.value = async.progress * 100;
            Debug.Log(async.progress);
            yield return (0);
        }
        Debug.Log("Loading complete");
        Progress.value = Progress.maxValue;
        async.allowSceneActivation = true;

    }

}
